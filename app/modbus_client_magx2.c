//******************************************************************************
//  Modbus klient MagX2
//*****************************************************************************/

#include <avr/pgmspace.h>
#include "modbus_client_magx2.h"
#include "modbus_reader.h"
#include "sys/global_status.h"

// prototyp ulohy
typedef void (*TModbusClientTask)(void);

// stavy uloh
typedef enum
{
    MODBUS_CLIENT_TASK_STATE_READ_DATA,         // cteni dat
    MODBUS_CLIENT_TASK_STATE_READ_DATA_WAIT,    // cekani na dokonceni cteni dat

} TModbusClientTaskState;

// Stav
static struct
{
    // stav otevreni
    BOOL isOpen;

    // uloha
    TModbusClientTask task;

    // stav ulohy
    TModbusClientTaskState taskState;

    // ctena data
    TModbusClientMagX2ReadData readData;

    // vysledek transakce
    BOOL result;

} state;

// tabulka cteni Modbus readerem
// - adresa registru
// - pocet registru
// - datovy typ
// - datove uloziste
static const TModbusReaderRecord modbusReaderDataRecords[] =
{
    { 150 - 1, 2, MODBUS_READER_DATA_UINT32, &state.readData.flow },
    { 152 - 1, 2, MODBUS_READER_DATA_UINT32, &state.readData.total },
    { 154 - 1, 2, MODBUS_READER_DATA_UINT32, &state.readData.aux },
    { 156 - 1, 2, MODBUS_READER_DATA_UINT32, &state.readData.pos },
    { 158 - 1, 2, MODBUS_READER_DATA_UINT32, &state.readData.neg },
    { 160 - 1, 2, MODBUS_READER_DATA_UINT32, &state.readData.temp },
    { 162 - 1, 2, MODBUS_READER_DATA_UINT32, &state.readData.extTemp },
    { 164 - 1, 2, MODBUS_READER_DATA_UINT32, &state.readData.extPress }
};

// ulohy
// uloha - cteni dat
static void Task_ReadData(void)
{
    switch (state.taskState)
    {
    case MODBUS_CLIENT_TASK_STATE_READ_DATA:
        ModbusReader_Open();
        ModbusReader_Read((TModbusReaderRecord*)modbusReaderDataRecords, sizeof(modbusReaderDataRecords) / sizeof(TModbusReaderRecord));
        state.taskState = MODBUS_CLIENT_TASK_STATE_READ_DATA_WAIT;
        break;

    case MODBUS_CLIENT_TASK_STATE_READ_DATA_WAIT:
        if (!ModbusReader_Pending())
        {
            state.result = ModbusReader_GetResult();
            state.task = NULL;
            ModbusReader_Close();
        }
        break;

    default:
        break;
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void ModbusClientMagX2_Init(void)
{
    state.isOpen = FALSE;
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void ModbusClientMagX2_Task(void)
{
    if ((state.isOpen) && (state.task != NULL))
        state.task();
}

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL ModbusClientMagX2_Open(void)
{
    ModbusClientMagX2_Close();

    state.isOpen = TRUE;
    state.task = NULL;
    return state.isOpen;
}

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void ModbusClientMagX2_Close(void)
{
    if (!state.isOpen)
        return;

    ModbusReader_Close();
    state.isOpen = FALSE;
}

//------------------------------------------------------------------------------
//  Zacne vycitat data.
//  Volat ModbusClientMagX2_Pending pro zjisteni stavu trvani transakce,
//  vysledek pak pomoci ModbusClientMagX2_GetResult a ModbusClientMagX2_GetReadData.
//------------------------------------------------------------------------------
void ModbusClientMagX2_ReadData(void)
{
    if (!state.isOpen)
    {
        return;
    }

    state.taskState = MODBUS_CLIENT_TASK_STATE_READ_DATA;
    state.task = Task_ReadData;
}

//------------------------------------------------------------------------------
//  Vrati vyctena data.
//------------------------------------------------------------------------------
const TModbusClientMagX2ReadData* ModbusClientMagX2_GetReadData(void)
{
    return &state.readData;
}

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL ModbusClientMagX2_Pending(void)
{
    if (!state.isOpen)
        return FALSE;

    return (state.task != NULL) ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Vraci vysledek posledni transakce (TRUE == OK).
//------------------------------------------------------------------------------
BOOL ModbusClientMagX2_GetResult(void)
{
    return state.result;
}
