//******************************************************************************
//  Modbus master
//*****************************************************************************/

#include "modbus_master.h"
#include "half_duplex_line.h"
#include "sys/global_status.h"

#define MODBUS_MASTER_BUFFER_SIZE   MODBUS_SER_PDU_SIZE_MAX

// prototyp ulohy
typedef void (*TModbusMasterTask)(void);

// stavy uloh
typedef enum
{
    MODBUS_MASTER_TASK_STATE_START,   // start ulohy
    MODBUS_MASTER_TASK_STATE_WAIT,    // cekani na dokonceni ulohy

} TModbusMasterTaskState;

// vysledky uloh
typedef enum
{
    MODBUS_MASTER_TASK_RESULT_OK,       // bez chyb
    MODBUS_MASTER_TASK_RESULT_ERROR,    // chyba

} TModbusMasterTaskResult;

// Stav
static struct
{
    // stav otevreni
    BOOL isOpen;

    // ID linky
    THalfDuplexLineId lineId;

    // Modbus adresa
    TModbusAddress modbusAddress;

    // uloha
    TModbusMasterTask task;

    // stav ulohy
    TModbusMasterTaskState taskState;

    // vysledek ulohy
    TModbusMasterTaskResult taskResult;

    // Modbus funkcni kod
    TModbusCode modbusCode;

    // adresa registru
    uint16_t regAddr;

    // pocet registru
    uint8_t regCount;

    // bufer
    uint8_t buffer[MODBUS_MASTER_BUFFER_SIZE];

} state;

// Je odpoved od slave platna?
static BOOL IsResponseValid(void)
{
    return state.isOpen &&
        (state.task == NULL) &&
        (state.modbusCode != MODBUS_FUNC_NONE) &&
        (state.taskResult == MODBUS_MASTER_TASK_RESULT_OK);
}

// Je mozne cist i.ty uint16_t registr z odpovedi slave?
static BOOL IsResponseUint16Valid(uint8_t i)
{
    if (state.modbusCode != MODBUS_FUNC_READ_HOLDING_REGISTERS)
        return FALSE;

    return (i < state.regCount) ? TRUE : FALSE;
}

// Je mozne cist i.ty uint32_t registr z odpovedi slave?
static BOOL IsResponseUint32Valid(uint8_t i)
{
    if (state.modbusCode != MODBUS_FUNC_READ_HOLDING_REGISTERS)
        return FALSE;

    return (i < (state.regCount / 2)) ? TRUE : FALSE;
}

// ulohy
// uloha - Vycte ze slave registry (Read Holding Registers).
static void Task_ReadHoldingRegisters(void)
{
    uint16_t count;

    switch (state.taskState)
    {
    case MODBUS_MASTER_TASK_STATE_START:
        count = Modbus_GetFrame_ReadHoldingRegisters(state.modbusAddress, state.regAddr, state.regCount, state.buffer, MODBUS_MASTER_BUFFER_SIZE);
        if (count != 0)
        {
            HalfDuplexLine_SendAndReceive(state.lineId, state.buffer, count, MODBUS_MASTER_BUFFER_SIZE);
            state.taskState = MODBUS_MASTER_TASK_STATE_WAIT;
        }
        else
        {
            state.taskResult = MODBUS_MASTER_TASK_RESULT_ERROR;
            state.task = NULL;
        }
        break;

    case MODBUS_MASTER_TASK_STATE_WAIT:
        if (!HalfDuplexLine_Pending(state.lineId))
        {
            count = HalfDuplexLine_ReceivedBytes(state.lineId);
            state.taskResult = Modbus_IsValidResponseFrame_ReadHoldingRegisters(state.buffer, count, state.modbusAddress, state.regCount) ?
                MODBUS_MASTER_TASK_RESULT_OK : MODBUS_MASTER_TASK_RESULT_ERROR;

            state.task = NULL;
        }
        break;

    default:
        break;
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void ModbusMaster_Init(void)
{
    state.isOpen = FALSE;
    state.modbusAddress = MODBUS_ADDRESS_UNKNOWN;
    state.modbusCode = MODBUS_FUNC_NONE;
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void ModbusMaster_Task(void)
{
    if ((state.isOpen) && (state.task != NULL))
        state.task();
}

//------------------------------------------------------------------------------
//  Nastavi Modbus adresu, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL ModbusMaster_SetAddress(TModbusAddress addr)
{
    if (!Modbus_IsAddressValid(addr))
        return FALSE;

    state.modbusAddress = addr;
    return TRUE;
}

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL ModbusMaster_Open(THalfDuplexLineParameters* lineParams)
{
    ModbusMaster_Close();

    state.lineId = HalfDuplexLine_Open(lineParams);
    if (state.lineId == HALF_DUPLEX_LINE_ID_NONE)
        return FALSE;

    state.isOpen = TRUE;
    state.task = NULL;
    return TRUE;
}

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void ModbusMaster_Close(void)
{
    if (!state.isOpen)
        return;

    HalfDuplexLine_Close(state.lineId);
    state.isOpen = FALSE;
}

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL ModbusMaster_Pending(void)
{
    if (!state.isOpen)
        return FALSE;

    return (state.task != NULL) ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Vycte ze slave registry (Read Holding Registers).
//  Konec transakce se testuje pomoci funkce ModbusMaster_Pending.
//------------------------------------------------------------------------------
void ModbusMaster_ReadHoldingRegisters(uint16_t regAddr, uint8_t regCount)
{
    if (!state.isOpen)
        return;

    state.modbusCode = MODBUS_FUNC_READ_HOLDING_REGISTERS;
    state.regAddr = regAddr;
    state.regCount = regCount;
    state.taskState = MODBUS_MASTER_TASK_STATE_START;
    state.task = Task_ReadHoldingRegisters;
}

//------------------------------------------------------------------------------
//  Vycte i.ty uint16_t registr z odpovedi slave. Po uspechu vraci TRUE.
//------------------------------------------------------------------------------
BOOL ModbusMaster_GetResponseUint16(uint8_t i, uint16_t* reg)
{
    if (!IsResponseValid())
        return FALSE;

    if (!IsResponseUint16Valid(i))
        return FALSE;

    *reg = Modbus_ParseRegUint16(&state.buffer[MODBUS_SER_PDU_READ_REG_OFF], i);
    return TRUE;
}

//------------------------------------------------------------------------------
//  Vycte i.ty uint32_t registr z odpovedi slave. Po uspechu vraci TRUE.
//------------------------------------------------------------------------------
BOOL ModbusMaster_GetResponseUint32(uint8_t i, uint32_t* reg)
{
    if (!IsResponseValid())
    {
        return FALSE;
    }

    if (!IsResponseUint32Valid(i))
    {
        return FALSE;
    }

    *reg = Modbus_ParseRegUint32(&state.buffer[MODBUS_SER_PDU_READ_REG_OFF], i);
    return TRUE;
}
