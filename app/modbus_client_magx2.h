//******************************************************************************
//  Modbus klient MagX2
//*****************************************************************************/

#ifndef MODBUS_CLIENT_MAGX2_H_INCLUDED
#define MODBUS_CLIENT_MAGX2_H_INCLUDED

#include "sys/types.h"

// ctena data
typedef struct ModbusClientMagX2ReadData
{
    // prutok
    float flow;

    // totalizer
    float total;

    // totalizer aux
    float aux;

    // totalizer +
    float pos;

    // totalizer -
    float neg;

    // teplota
    float temp;

    // externi teplota
    float extTemp;

    // externi tlak
    float extPress;

} TModbusClientMagX2ReadData;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void ModbusClientMagX2_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void ModbusClientMagX2_Task(void);

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL ModbusClientMagX2_Open(void);

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void ModbusClientMagX2_Close(void);

//------------------------------------------------------------------------------
//  Zacne vycitat data.
//  Volat ModbusClientMagX2_Pending pro zjisteni stavu trvani transakce,
//  vysledek pak pomoci ModbusClientMagX2_GetResult a ModbusClientMagX2_GetReadData.
//------------------------------------------------------------------------------
void ModbusClientMagX2_ReadData(void);

//------------------------------------------------------------------------------
//  Vrati vyctena data.
//------------------------------------------------------------------------------
const TModbusClientMagX2ReadData* ModbusClientMagX2_GetReadData(void);

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL ModbusClientMagX2_Pending(void);

//------------------------------------------------------------------------------
//  Vraci vysledek posledni transakce (TRUE == OK).
//------------------------------------------------------------------------------
BOOL ModbusClientMagX2_GetResult(void);

#endif // MODBUS_CLIENT_MAGX2_H_INCLUDED
