//******************************************************************************
//  Profibus slave
//*****************************************************************************/

#ifndef PROFIBUS_SLAVE_H_INCLUDED
#define PROFIBUS_SLAVE_H_INCLUDED

#include "sys/types.h"

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void ProfibusSlave_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void ProfibusSlave_Task(void);

#endif // PROFIBUS_SLAVE_H_INCLUDED
