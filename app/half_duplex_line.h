//******************************************************************************
//  Poloduplexni linka
//*****************************************************************************/

#ifndef HALF_DUPLEX_LINE_H_INCLUDED
#define HALF_DUPLEX_LINE_H_INCLUDED

#include "sys/types.h"
#include "sys/uart.h"
#include "sys/io.h"

// pocet linek
#define HALF_DUPLEX_LINES   2

// identifikator neprirazene/zadne linky
#define HALF_DUPLEX_LINE_ID_NONE   HALF_DUPLEX_LINES

// identifikator linky
typedef uint8_t THalfDuplexLineId;

// Parametry linky
typedef struct HalfDuplexLineParameters
{
    // UART ID
    TUartId uartId;

    // Parametry UARTu
    TUartParameters uartParams;

    // Vystup ovladani smeru prenosu na lince
    // (pokud neni treba, pak IO_ID_None)
    TIOId dirOutput;

    // Uroven vystupu dirOutput pro vysilani
    // (pokud je dirOutput == IO_ID_None, pak nema vyznam)
    TBit dirOutputTxLevel;

    // Max. prodleva odezvy [ms] (cekani na 1.byte ramce odpovedi),
    // pokud je 0 a provadi se uloha HalfDuplexLine_Receive, pak se nebere v potaz
    uint16_t responseTimeout;

    // Doba klidu za koncem prijateho ramce [ms]
    uint16_t frameEndDelay;

    // Prodleva pred vysilanim [ms],
    // ma vyznam pouze v uloze HalfDuplexLine_Send
    uint16_t delayBeforeTx;

    // Prodleva po vysilani [ms],
    // ma vyznam pouze v uloze HalfDuplexLine_Send
    uint16_t delayAfterTx;

} THalfDuplexLineParameters;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void HalfDuplexLine_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void HalfDuplexLine_Task(void);

//------------------------------------------------------------------------------
//  Otevre linku, vraci jeji ID (nebo HALF_DUPLEX_LINE_ID_NONE,
//  pokud nenalezne volnou linku).
//------------------------------------------------------------------------------
THalfDuplexLineId HalfDuplexLine_Open(THalfDuplexLineParameters* params);

//------------------------------------------------------------------------------
//  Zavre linku.
//------------------------------------------------------------------------------
void HalfDuplexLine_Close(THalfDuplexLineId id);

//------------------------------------------------------------------------------
//  Odesle data na linku a prijme odpoved (neblokujici volani, konec transakce
//  se testuje pomoci funkce HalfDuplexLine_Pending),
//  volajici zajisti dostupnost dat po dobu vysilani a dostatek mista v buferu
//  pro odpoved.
//------------------------------------------------------------------------------
void HalfDuplexLine_SendAndReceive(THalfDuplexLineId id, uint8_t* buffer, uint16_t sendCount, uint16_t bufferSize);

//------------------------------------------------------------------------------
//  Odesle data na linku (neblokujici volani, konec transakce
//  se testuje pomoci funkce HalfDuplexLine_Pending),
//  volajici zajisti dostupnost dat po dobu vysilani.
//------------------------------------------------------------------------------
void HalfDuplexLine_Send(THalfDuplexLineId id, uint8_t* buffer, uint16_t sendCount);

//------------------------------------------------------------------------------
//  Prijme data z linky (neblokujici volani, konec transakce
//  se testuje pomoci funkce HalfDuplexLine_Pending),
//  volajici zajisti dostatek mista v buferu.
//------------------------------------------------------------------------------
void HalfDuplexLine_Receive(THalfDuplexLineId id, uint8_t* buffer, uint16_t bufferSize);

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce na lince (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL HalfDuplexLine_Pending(THalfDuplexLineId id);

//------------------------------------------------------------------------------
//  Vraci pocet prijatych bytu v buferu z linky.
//------------------------------------------------------------------------------
uint16_t HalfDuplexLine_ReceivedBytes(THalfDuplexLineId id);

#endif // HALF_DUPLEX_LINE_H_INCLUDED
