//******************************************************************************
//  Modbus master
//*****************************************************************************/

#ifndef MODBUS_MASTER_H_INCLUDED
#define MODBUS_MASTER_H_INCLUDED

#include "sys/types.h"
#include "app/half_duplex_line.h"
#include "app/modbus_protocol.h"

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void ModbusMaster_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void ModbusMaster_Task(void);

//------------------------------------------------------------------------------
//  Nastavi Modbus adresu, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL ModbusMaster_SetAddress(TModbusAddress addr);

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL ModbusMaster_Open(THalfDuplexLineParameters* lineParams);

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void ModbusMaster_Close(void);

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL ModbusMaster_Pending(void);

//------------------------------------------------------------------------------
//  Vycte ze slave registry (Read Holding Registers).
//  Konec transakce se testuje pomoci funkce ModbusMaster_Pending.
//------------------------------------------------------------------------------
void ModbusMaster_ReadHoldingRegisters(uint16_t regAddr, uint8_t regCount);

//------------------------------------------------------------------------------
//  Vycte i.ty uint16_t registr z odpovedi slave. Po uspechu vraci TRUE.
//------------------------------------------------------------------------------
BOOL ModbusMaster_GetResponseUint16(uint8_t i, uint16_t* reg);

//------------------------------------------------------------------------------
//  Vycte i.ty uint32_t registr z odpovedi slave. Po uspechu vraci TRUE.
//------------------------------------------------------------------------------
BOOL ModbusMaster_GetResponseUint32(uint8_t i, uint32_t* reg);

#endif // MODBUS_MASTER_H_INCLUDED
