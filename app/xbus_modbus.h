//******************************************************************************
//  Vymena dat mezi externi sbernici a Modbus
//*****************************************************************************/

#ifndef XBUS_MODBUS_H_INCLUDED
#define XBUS_MODBUS_H_INCLUDED

#include "sys/types.h"
#include "modbus_client_magx2.h"

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void XbusModbus_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void XbusModbus_Task(void);

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL XbusModbus_Open(void);

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void XbusModbus_Close(void);

//------------------------------------------------------------------------------
//  Vrati vyctena data nebo NULL.
//------------------------------------------------------------------------------
const TModbusClientMagX2ReadData* XbusModbus_GetReadData(void);

#endif // XBUS_MODBUS_H_INCLUDED
