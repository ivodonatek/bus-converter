//******************************************************************************
//  Vymena dat mezi externi sbernici a Modbus
//*****************************************************************************/

#include "sys/sys_config.h"

#include <string.h>
#include "xbus_modbus.h"
#include "modbus_client_magx2.h"
#include "sys/global_status.h"

// prototyp ulohy
typedef void (*TXbusModbusTask)(void);

// stavy uloh
typedef enum
{
    XBUS_MODBUS_TASK_STATE_READ_MODBUS_DATA,         // cteni Modbus dat
    XBUS_MODBUS_TASK_STATE_READ_MODBUS_DATA_WAIT,    // cekani na dokonceni cteni Modbus dat

} TXbusModbusTaskState;

// Stav
static struct
{
    // stav otevreni
    BOOL isOpen;

    // uloha
    TXbusModbusTask task;

    // stav ulohy
    TXbusModbusTaskState taskState;

    // vyctena Modbus data
    TModbusClientMagX2ReadData modbusReadData;

    // platnost vyctenych Modbus dat
    BOOL isModbusReadDataValid;

} state;

// ulohy
// uloha - vymena dat
static void Task_DataExchange(void)
{
    switch (state.taskState)
    {
    case XBUS_MODBUS_TASK_STATE_READ_MODBUS_DATA:
        ModbusClientMagX2_ReadData();
        state.taskState = XBUS_MODBUS_TASK_STATE_READ_MODBUS_DATA_WAIT;
        break;

    case XBUS_MODBUS_TASK_STATE_READ_MODBUS_DATA_WAIT:
        if (!ModbusClientMagX2_Pending())
        {
            if (ModbusClientMagX2_GetResult())
            {
                 const TModbusClientMagX2ReadData* pReadData = ModbusClientMagX2_GetReadData();
                 memcpy((void *)&(state.modbusReadData), (void *)pReadData, sizeof(TModbusClientMagX2ReadData));
                 state.isModbusReadDataValid = TRUE;
            }

            state.taskState = XBUS_MODBUS_TASK_STATE_READ_MODBUS_DATA;
        }
        break;

    default:
        break;
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void XbusModbus_Init(void)
{
    state.isOpen = FALSE;
    state.isModbusReadDataValid = FALSE;
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void XbusModbus_Task(void)
{
    if ((state.isOpen) && (state.task != NULL))
        state.task();
}

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL XbusModbus_Open(void)
{
    XbusModbus_Close();

    state.isOpen = ModbusClientMagX2_Open();
    state.taskState = XBUS_MODBUS_TASK_STATE_READ_MODBUS_DATA;
    state.task = Task_DataExchange;
    return state.isOpen;
}

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void XbusModbus_Close(void)
{
    if (!state.isOpen)
        return;

    ModbusClientMagX2_Close();
    state.isOpen = FALSE;
}

//------------------------------------------------------------------------------
//  Vrati vyctena data nebo NULL.
//------------------------------------------------------------------------------
const TModbusClientMagX2ReadData* XbusModbus_GetReadData(void)
{
    return (state.isModbusReadDataValid) ? &state.modbusReadData : NULL;
}
