//******************************************************************************
//  Vycteni skupiny registru.
//*****************************************************************************/

#include "board/board_config.h"
#include "sys/setting.h"
#include "sys/global_status.h"
#include "sys/systick.h"
#include "modbus_reader.h"
#include "modbus_master.h"

// Max. prodleva odezvy [ms] (cekani na 1.byte ramce odpovedi)
#define MODBUS_READER_RESPONSE_TIMEOUT  500

// prototyp ulohy
typedef void (*TModbusReaderTask)(void);

// stavy uloh
typedef enum
{
    MODBUS_READER_TASK_STATE_READ,   // cteni dalsiho zaznamu
    MODBUS_READER_TASK_STATE_WAIT,   // cekani na dokonceni cteni zaznamu
    MODBUS_READER_TASK_STATE_DELAY   // prodleva mezi vycitanymi zaznamy

} TModbusReaderTaskState;

// Stav
static struct
{
    // stav otevreni
    BOOL isOpen;

    // uloha
    TModbusReaderTask task;

    // stav ulohy
    TModbusReaderTaskState taskState;

    // cteci zaznamy
    TModbusReaderRecord* records;

    // pocet zaznamu
    uint8_t recordCount;

    // pocet vyctenych zaznamu
    uint8_t recordRead;

    // timer
    uint32_t timer;

    // delay [ms]
    uint16_t delay;

    // vysledek vycitani
    BOOL result;

} state;

// naplni parametry linky
static void FillLineParameters(THalfDuplexLineParameters* lineParams)
{
    lineParams->uartId = MODBUS_UART_ID;
    lineParams->uartParams.baudRate = Setting_GetModbusUartBaudRate();
    lineParams->uartParams.dataBits = Setting_GetModbusUartDataBits();
    lineParams->uartParams.stopBits = Setting_GetModbusUartStopBits();
    lineParams->uartParams.parity = Setting_GetModbusUartParity();
    lineParams->dirOutput = IO_ID_None;
    lineParams->dirOutputTxLevel = 0;
    lineParams->responseTimeout = MODBUS_READER_RESPONSE_TIMEOUT;
    lineParams->frameEndDelay = Modbus_GetInterFrameDelay(lineParams->uartParams.baudRate,
        Uart_GetFrameBits(lineParams->uartParams.dataBits, lineParams->uartParams.parity,
        lineParams->uartParams.stopBits));
    lineParams->delayBeforeTx = 0;
    lineParams->delayAfterTx = 0;
}

// vrati odkaz na aktualni zaznam
static TModbusReaderRecord* GetRecord(void)
{
    return &state.records[state.recordRead];
}

// vycte dalsi zaznam
static void ReadRecord(void)
{
    TModbusReaderRecord* pRecord = GetRecord();
    ModbusMaster_ReadHoldingRegisters(pRecord->regAddr, pRecord->regCount);
}

// ulozi vyctene registry do uloziste zaznamu, vraci TRUE po uspechu
static BOOL StoreRecord(void)
{
    TModbusReaderRecord* pRecord = GetRecord();

    switch (pRecord->dataType)
    {
    case MODBUS_READER_DATA_UINT16:
        if (!ModbusMaster_GetResponseUint16(0, (uint16_t*)pRecord->dataStorage))
        {
            return FALSE;
        }
        break;

    case MODBUS_READER_DATA_UINT32:
        if (!ModbusMaster_GetResponseUint32(0, (uint32_t*)pRecord->dataStorage))
        {
            return FALSE;
        }
        break;

    default:
        return FALSE;
    }

    return TRUE;
}

// ulohy
// uloha - cteni
static void Task_Read(void)
{
    switch (state.taskState)
    {
    case MODBUS_READER_TASK_STATE_READ:
        if (state.recordRead < state.recordCount)
        {
            ReadRecord();
            state.taskState = MODBUS_READER_TASK_STATE_WAIT;
        }
        else
        {
            state.result = TRUE;
            state.task = NULL;
        }
        break;

    case MODBUS_READER_TASK_STATE_WAIT:
        if (!ModbusMaster_Pending())
        {
            if (StoreRecord())
            {
                state.recordRead++;
                state.timer = SysTick_GetSysMilliSeconds();
                state.taskState = MODBUS_READER_TASK_STATE_DELAY;
            }
            else
            {
                state.result = FALSE;
                state.task = NULL;
            }
        }
        break;

    case MODBUS_READER_TASK_STATE_DELAY:
        if (SysTick_IsSysMilliSecondTimeout(state.timer, state.delay))
        {
            state.taskState = MODBUS_READER_TASK_STATE_READ;
        }
        break;

    default:
        break;
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void ModbusReader_Init(void)
{
    state.isOpen = FALSE;
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void ModbusReader_Task(void)
{
    if ((state.isOpen) && (state.task != NULL))
        state.task();
}

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL ModbusReader_Open(void)
{
    ModbusReader_Close();

    THalfDuplexLineParameters lineParams;
    FillLineParameters(&lineParams);
    state.delay = lineParams.frameEndDelay;

    state.isOpen = ModbusMaster_Open(&lineParams);
    ModbusMaster_SetAddress(Setting_GetModbusSlaveAddress());
    state.task = NULL;
    return state.isOpen;
}

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void ModbusReader_Close(void)
{
    if (!state.isOpen)
        return;

    ModbusMaster_Close();
    state.isOpen = FALSE;
}

//------------------------------------------------------------------------------
//  Zacne vycitat skupinu registru.
//  Volat ModbusReader_Pending pro zjisteni stavu trvani transakce,
//  vysledek pak pomoci ModbusReader_GetResult.
//------------------------------------------------------------------------------
void ModbusReader_Read(TModbusReaderRecord* records, uint8_t recordCount)
{
    if (!state.isOpen)
        return;

    state.records = records;
    state.recordCount = recordCount;
    state.recordRead = 0;
    state.taskState = MODBUS_READER_TASK_STATE_READ;
    state.task = Task_Read;
}

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL ModbusReader_Pending(void)
{
    if (!state.isOpen)
        return FALSE;

    return (state.task != NULL) ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Vraci vysledek posledni transakce (TRUE == OK).
//------------------------------------------------------------------------------
BOOL ModbusReader_GetResult(void)
{
    return state.result;
}
