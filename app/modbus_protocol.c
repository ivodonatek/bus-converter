//******************************************************************************
//  Modbus protokol
//*****************************************************************************/

#include <avr/pgmspace.h>
#include "modbus_protocol.h"
#include "sys/global_status.h"

static const PROGMEM uint8_t aucCRCHi[] = {
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40
};

static const PROGMEM uint8_t aucCRCLo[] = {
    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7,
    0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,
    0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9,
    0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
    0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
    0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,
    0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D,
    0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
    0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF,
    0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
    0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1,
    0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
    0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB,
    0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
    0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
    0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
    0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97,
    0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,
    0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89,
    0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83,
    0x41, 0x81, 0x80, 0x40
};

//------------------------------------------------------------------------------
//  Spocita CRC ramce
//------------------------------------------------------------------------------
static uint16_t Modbus_CalculateCRC(uint8_t* frame, uint8_t length)
{
    uint8_t ucCRCHi = 0xFF;
    uint8_t ucCRCLo = 0xFF;
    int iIndex;

    while (length--)
    {
        iIndex = ucCRCLo ^ *( frame++ );
        ucCRCLo = ucCRCHi ^ pgm_read_byte(&aucCRCHi[iIndex]);
        ucCRCHi = pgm_read_byte(&aucCRCLo[iIndex]);
    }
    return ( uint16_t )( ucCRCHi << 8 | ucCRCLo );
}

//------------------------------------------------------------------------------
//  Kontrola povolene adresy
//------------------------------------------------------------------------------
BOOL Modbus_IsAddressValid(TModbusAddress addr)
{
    return (addr <= MODBUS_ADDRESS_MAX) ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Kontrola povolene slave adresy
//------------------------------------------------------------------------------
BOOL Modbus_IsSlaveAddressValid(TModbusAddress addr)
{
    return ((MODBUS_ADDRESS_MIN <= addr) && (addr <= MODBUS_ADDRESS_MAX)) ?
        TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Vrati adresu v ramci.
//------------------------------------------------------------------------------
TModbusAddress Modbus_GetFrameAddress(uint8_t* frame)
{
    return frame[MODBUS_SER_PDU_ADDR_OFF];
}

//------------------------------------------------------------------------------
//  Vrati funkcni kod v ramci.
//------------------------------------------------------------------------------
TModbusCode Modbus_GetFrameCode(uint8_t* frame)
{
    return frame[MODBUS_SER_PDU_FUNC_OFF];
}

//------------------------------------------------------------------------------
//  Zkontroluje Modbus ramec a vrati TRUE pokud je vporadku.
//------------------------------------------------------------------------------
BOOL Modbus_IsValidFrame(uint8_t* frame, uint16_t frameLgt)
{
    return ((frameLgt >= MODBUS_SER_PDU_SIZE_MIN) && (Modbus_CalculateCRC(frame, frameLgt) == 0)) ?
        TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Zkontroluje Modbus ramec odpovedi na pozadavek Read Holding Registers
//  a vrati TRUE pokud je vporadku.
//------------------------------------------------------------------------------
BOOL Modbus_IsValidResponseFrame_ReadHoldingRegisters(uint8_t* frame, uint16_t frameLgt,
    TModbusAddress addr, uint8_t regCount)
{
    if (!Modbus_IsValidFrame(frame, frameLgt))
    {
        return FALSE;
    }

    if (addr != Modbus_GetFrameAddress(frame))
    {
        return FALSE;
    }

    if ((regCount * 2) != (frame[MODBUS_SER_PDU_DATA_OFF]))
    {
        return FALSE;
    }

    if ((frameLgt - MODBUS_SER_PDU_SIZE_MIN - 1) != (regCount * 2))
    {
        return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------
//  Vrati klidovy interval mezi ramci [ms].
//  bits ... start,data,parity,stop
//------------------------------------------------------------------------------
uint16_t Modbus_GetInterFrameDelay(uint32_t baudRate, uint8_t bits)
{
    if (baudRate > 19200)
    {
        return 2;
    }
    else if (baudRate > 0)
    {
        float delay = bits * 3.5 * 1000.0 / baudRate;
        return 1 + (uint16_t)delay;
    }
    else
    {
        return 0;
    }
}

//------------------------------------------------------------------------------
//  Sestavi ramec Read Holding Registers.
//  Vrati pocet bytu ramce (>0) nebo 0 po chybe.
//------------------------------------------------------------------------------
uint16_t Modbus_GetFrame_ReadHoldingRegisters(TModbusAddress slaveAddr,
    uint16_t regAddr, uint8_t regCount, uint8_t* frame, uint16_t frameBufLgt)
{
    if (!Modbus_IsSlaveAddressValid(slaveAddr))
        return 0;

    if (regCount > MODBUS_READ_HOLDING_REGISTERS_COUNT_MAX)
        return 0;

    if (frameBufLgt < 8)
        return 0;

    frame[0] = slaveAddr;
    frame[1] = MODBUS_FUNC_READ_HOLDING_REGISTERS;

    frame[2] = (uint8_t)(regAddr >> 8);
    frame[3] = (uint8_t)(regAddr);

    frame[4] = 0;
    frame[5] = regCount;

    uint16_t crc = Modbus_CalculateCRC(frame, 6);
    frame[6] = (uint8_t)(crc);
    frame[7] = (uint8_t)(crc >> 8);

    return 8;
}

//------------------------------------------------------------------------------
//  Parsing typu uint16_t.
//------------------------------------------------------------------------------
uint16_t Modbus_ParseUint16(uint8_t* data)
{
    uint16_t result;

    result = (uint16_t)data[0] << 8;
    result += (uint16_t)data[1];

    return result;
}

//------------------------------------------------------------------------------
//  Parsing i-teho registru typu uint16_t.
//------------------------------------------------------------------------------
uint16_t Modbus_ParseRegUint16(uint8_t* data, uint8_t i)
{
    return Modbus_ParseUint16(&data[i * sizeof(uint16_t)]);
}

//------------------------------------------------------------------------------
//  Parsing typu uint32_t.
//------------------------------------------------------------------------------
uint32_t Modbus_ParseUint32(uint8_t* data)
{
    uint32_t result;

    result = (uint32_t)data[0] << 8;
    result += (uint32_t)data[1];
    result += (uint32_t)data[2] << 24;
    result += (uint32_t)data[3] << 16;

    return result;
}

//------------------------------------------------------------------------------
//  Parsing i-teho registru typu uint32_t.
//------------------------------------------------------------------------------
uint32_t Modbus_ParseRegUint32(uint8_t* data, uint8_t i)
{
    return Modbus_ParseUint32(&data[i * sizeof(uint32_t)]);
}
