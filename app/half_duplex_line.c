//******************************************************************************
//  Poloduplexni linka
//*****************************************************************************/

#include <stdio.h>
#include "machine/api.h"
#include "half_duplex_line.h"
#include "sys/systick.h"
#include "sys/log.h"
#include "sys/global_status.h"

// smer komunikace na lince
typedef enum
{
    LINE_COMM_DIR_RX, // prijem
    LINE_COMM_DIR_TX  // vysilani

} TLineCommDir;

// stavy uloh linky
typedef enum
{
    LINE_TASK_STATE_RX,             // prijem
    LINE_TASK_STATE_RX_WAIT_BEGIN,  // cekani na prijem zacatku ramce
    LINE_TASK_STATE_RX_WAIT_END,    // cekani na prijem celeho ramce
    LINE_TASK_STATE_TX,             // vysilani
    LINE_TASK_STATE_TX_DLY_BEFORE,  // prodleva pred vysilanim
    LINE_TASK_STATE_TX_WAIT,        // cekani na dovysilani
    LINE_TASK_STATE_TX_DLY_AFTER,   // prodleva po vysilani

} TLineTaskState;

typedef struct LineState TLineState;

// prototyp ulohy/transakce
typedef void (*TLineTask)(TLineState* pLineState);

// Stav linky
struct LineState
{
    // stav otevreni
    BOOL isOpen;

    // UART ID
    TUartId uartId;

    // Vystup ovladani smeru prenosu na lince
    // (pokud neni treba, pak IO_ID_None)
    TIOId dirOutput;

    // Uroven vystupu dirOutput pro vysilani
    // (pokud je dirOutput == IO_ID_None, pak nema vyznam)
    TBit dirOutputTxLevel;

    // Max. prodleva odezvy [ms] (cekani na 1.byte ramce odpovedi),
    // pokud je 0 a provadi se uloha LineTask_Receive, pak se nebere v potaz
    uint16_t responseTimeout;

    // Doba klidu za koncem prijateho ramce [ms]
    uint16_t frameEndDelay;

    // Prodleva pred vysilanim [ms],
    // ma vyznam pouze v uloze LineTask_Send
    uint16_t delayBeforeTx;

    // Prodleva po vysilani [ms],
    // ma vyznam pouze v uloze LineTask_Send
    uint16_t delayAfterTx;

    // uloha/transakce na lince
    TLineTask lineTask;

    // stav ulohy
    TLineTaskState lineTaskState;

    // cas [ms]
    uint32_t time;

    // bufer dat pro vysilani/prijem
    uint8_t* buffer;

    // pocet bytu dat v buferu
    uint16_t bufferDataCount;

    // velikost buferu
    uint16_t bufferSize;

};

// stavy
static TLineState lineStates[HALF_DUPLEX_LINES];

// vrati odkaz na stav linky nebo NULL
static TLineState* GetLineState(THalfDuplexLineId id)
{
    if (id >= HALF_DUPLEX_LINES)
        return NULL;

    return &lineStates[id];
}

// vrati odkaz na stav linky nebo NULL (podle Uartu)
static TLineState* GetUartLineState(TUartId uartId)
{
    for (THalfDuplexLineId id = 0; id < HALF_DUPLEX_LINES; id++)
    {
        if (lineStates[id].uartId == uartId)
            return &lineStates[id];
    }

    return NULL;
}

// najde ID volne linky nebo HALF_DUPLEX_LINE_ID_NONE (vse obsazeno)
static THalfDuplexLineId FindFreeLineId(void)
{
    for (THalfDuplexLineId id = 0; id < HALF_DUPLEX_LINES; id++)
    {
        if (!lineStates[id].isOpen)
            return id;
    }

    return HALF_DUPLEX_LINE_ID_NONE;
}

// nastavi uroven vystupu ovladani smeru prenosu na lince
static void SetLineDirOutputLevel(TLineState *pLineState, TLineCommDir dir)
{
    /*if (pLineState->dirOutput == IO_ID_None)
        return;

    TBit level = (dir == LINE_COMM_DIR_TX) ? pLineState->dirOutputTxLevel :
        pLineState->dirOutputTxLevel ? 0 : 1;

    IO_SetOutputLevel(pLineState->dirOutput, level);*/
}

// ulohy linky
// uloha - Odesle data na linku a prijme odpoved
static void LineTask_SendAndReceive(TLineState* pLineState);

static void LineTask_SendAndReceive_Callback(TUartId id)
{
    TLineState *pLineState = GetUartLineState(id);
    if (pLineState == NULL)
        return;

    if (pLineState->lineTask == LineTask_SendAndReceive)
    {
        SetLineDirOutputLevel(pLineState, LINE_COMM_DIR_RX);
        Uart_Receive(pLineState->uartId, pLineState->buffer, pLineState->bufferSize);
        pLineState->time = SysTick_GetSysMilliSeconds();
        pLineState->lineTaskState = LINE_TASK_STATE_RX_WAIT_BEGIN;
    }
}

static void LineTask_SendAndReceive(TLineState* pLineState)
{
    uint16_t receivedBytes;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        switch (pLineState->lineTaskState)
        {
        case LINE_TASK_STATE_TX:
            SetLineDirOutputLevel(pLineState, LINE_COMM_DIR_TX);
            Uart_SendWithCallback(pLineState->uartId, pLineState->buffer, pLineState->bufferDataCount, LineTask_SendAndReceive_Callback);
            pLineState->lineTaskState = LINE_TASK_STATE_TX_WAIT;
            break;

        case LINE_TASK_STATE_TX_WAIT:
            break;

        case LINE_TASK_STATE_RX_WAIT_BEGIN:
            pLineState->bufferDataCount = Uart_ReceivedBytes(pLineState->uartId);
            if (pLineState->bufferDataCount == 0)
            {
                if (SysTick_IsSysMilliSecondTimeout(pLineState->time, pLineState->responseTimeout))
                {
                    Uart_StopReceiving(pLineState->uartId);
                    pLineState->lineTask = NULL;
                }
            }
            else
            {
                pLineState->time = SysTick_GetSysMilliSeconds();
                pLineState->lineTaskState = LINE_TASK_STATE_RX_WAIT_END;
            }
            break;

        case LINE_TASK_STATE_RX_WAIT_END:
            receivedBytes = Uart_ReceivedBytes(pLineState->uartId);
            if (pLineState->bufferDataCount == receivedBytes)
            {
                if (SysTick_IsSysMilliSecondTimeout(pLineState->time, pLineState->frameEndDelay))
                {
                    Uart_StopReceiving(pLineState->uartId);
                    pLineState->lineTask = NULL;
                }
            }
            else
            {
                pLineState->bufferDataCount = receivedBytes;
                pLineState->time = SysTick_GetSysMilliSeconds();
                if (receivedBytes == pLineState->bufferSize)
                {
                    Uart_StopReceiving(pLineState->uartId);
                    pLineState->lineTask = NULL;
                }
            }
            break;

        default:
            break;
        }
    }
}

// uloha - Odesle data na linku
static void LineTask_Send(TLineState* pLineState)
{
    switch (pLineState->lineTaskState)
    {
    case LINE_TASK_STATE_TX:
        SetLineDirOutputLevel(pLineState, LINE_COMM_DIR_RX);
        pLineState->time = SysTick_GetSysMilliSeconds();
        pLineState->lineTaskState = LINE_TASK_STATE_TX_DLY_BEFORE;
        break;

    case LINE_TASK_STATE_TX_DLY_BEFORE:
        if (SysTick_IsSysMilliSecondTimeout(pLineState->time, pLineState->delayBeforeTx))
        {
            SetLineDirOutputLevel(pLineState, LINE_COMM_DIR_TX);
            Uart_Send(pLineState->uartId, pLineState->buffer, pLineState->bufferDataCount);
            pLineState->lineTaskState = LINE_TASK_STATE_TX_WAIT;
        }
        break;

    case LINE_TASK_STATE_TX_WAIT:
        if (!Uart_Sending(pLineState->uartId))
        {
            SetLineDirOutputLevel(pLineState, LINE_COMM_DIR_RX);
            pLineState->time = SysTick_GetSysMilliSeconds();
            pLineState->lineTaskState = LINE_TASK_STATE_TX_DLY_AFTER;
        }
        break;

    case LINE_TASK_STATE_TX_DLY_AFTER:
        if (SysTick_IsSysMilliSecondTimeout(pLineState->time, pLineState->delayAfterTx))
        {
            pLineState->lineTask = NULL;
        }
        break;

    default:
        break;
    }
}

// uloha - Prijem dat z linky
static void LineTask_Receive(TLineState* pLineState)
{
    uint16_t receivedBytes;

    switch (pLineState->lineTaskState)
    {
    case LINE_TASK_STATE_RX:
        SetLineDirOutputLevel(pLineState, LINE_COMM_DIR_RX);
        Uart_Receive(pLineState->uartId, pLineState->buffer, pLineState->bufferSize);
        pLineState->time = SysTick_GetSysMilliSeconds();
        pLineState->lineTaskState = LINE_TASK_STATE_RX_WAIT_BEGIN;
        break;

    case LINE_TASK_STATE_RX_WAIT_BEGIN:
        pLineState->bufferDataCount = Uart_ReceivedBytes(pLineState->uartId);
        if (pLineState->bufferDataCount == 0)
        {
            if (pLineState->responseTimeout != 0)
            {
                if (SysTick_IsSysMilliSecondTimeout(pLineState->time, pLineState->responseTimeout))
                {
                    Uart_StopReceiving(pLineState->uartId);
                    pLineState->lineTask = NULL;
                }
            }
        }
        else
        {
            pLineState->time = SysTick_GetSysMilliSeconds();
            pLineState->lineTaskState = LINE_TASK_STATE_RX_WAIT_END;
        }
        break;

    case LINE_TASK_STATE_RX_WAIT_END:
        receivedBytes = Uart_ReceivedBytes(pLineState->uartId);
        if (pLineState->bufferDataCount == receivedBytes)
        {
            if (SysTick_IsSysMilliSecondTimeout(pLineState->time, pLineState->frameEndDelay))
            {
                Uart_StopReceiving(pLineState->uartId);
                pLineState->lineTask = NULL;
            }
        }
        else
        {
            pLineState->bufferDataCount = receivedBytes;
            pLineState->time = SysTick_GetSysMilliSeconds();
            if (receivedBytes == pLineState->bufferSize)
            {
                Uart_StopReceiving(pLineState->uartId);
                pLineState->lineTask = NULL;
            }
        }
        break;

    default:
        break;
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void HalfDuplexLine_Init(void)
{
    for (THalfDuplexLineId id = 0; id < HALF_DUPLEX_LINES; id++)
    {
        lineStates[id].isOpen = FALSE;
    }
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void HalfDuplexLine_Task(void)
{
    TLineState *pLineState;

    for (THalfDuplexLineId id = 0; id < HALF_DUPLEX_LINES; id++)
    {
        pLineState = &lineStates[id];

        if ((pLineState->isOpen) && (pLineState->lineTask != NULL))
        {
            TLineTask lineTask = pLineState->lineTask;
            lineTask(pLineState);
        }
    }
}

//------------------------------------------------------------------------------
//  Otevre linku, vraci jeji ID (nebo HALF_DUPLEX_LINE_ID_NONE,
//  pokud nenalezne volnou linku).
//------------------------------------------------------------------------------
THalfDuplexLineId HalfDuplexLine_Open(THalfDuplexLineParameters* params)
{
    THalfDuplexLineId id = FindFreeLineId();
    if (id == HALF_DUPLEX_LINE_ID_NONE)
        return HALF_DUPLEX_LINE_ID_NONE;

    TLineState *pLineState = GetLineState(id);
    /*if (pLineState == NULL) // tady je test navic
        return HALF_DUPLEX_LINE_ID_NONE;*/

    pLineState->isOpen = TRUE;
    pLineState->lineTask = NULL;
    pLineState->uartId = params->uartId;
    pLineState->dirOutput = params->dirOutput;
    pLineState->dirOutputTxLevel = params->dirOutputTxLevel;
    pLineState->responseTimeout = params->responseTimeout;
    pLineState->frameEndDelay = params->frameEndDelay;
    pLineState->delayBeforeTx = params->delayBeforeTx;
    pLineState->delayAfterTx = params->delayAfterTx;

    SetLineDirOutputLevel(pLineState, LINE_COMM_DIR_RX);
    Uart_Open(params->uartId, &params->uartParams);

    return id;
}

//------------------------------------------------------------------------------
//  Zavre linku.
//------------------------------------------------------------------------------
void HalfDuplexLine_Close(THalfDuplexLineId id)
{
    TLineState *pLineState = GetLineState(id);
    if (pLineState == NULL)
        return;

    if (!pLineState->isOpen)
        return;

    SetLineDirOutputLevel(pLineState, LINE_COMM_DIR_RX);
    Uart_Close(pLineState->uartId);
    pLineState->isOpen = FALSE;
}

//------------------------------------------------------------------------------
//  Odesle data na linku a prijme odpoved (neblokujici volani, konec transakce
//  se testuje pomoci funkce HalfDuplexLine_Pending),
//  volajici zajisti dostupnost dat po dobu vysilani a dostatek mista v buferu
//  pro odpoved.
//------------------------------------------------------------------------------
void HalfDuplexLine_SendAndReceive(THalfDuplexLineId id, uint8_t* buffer, uint16_t sendCount, uint16_t bufferSize)
{
    TLineState *pLineState = GetLineState(id);
    if (pLineState == NULL)
    {
        return;
    }

    if (!pLineState->isOpen)
    {
        return;
    }

    pLineState->buffer = buffer;
    pLineState->bufferDataCount = sendCount;
    pLineState->bufferSize = bufferSize;
    pLineState->lineTaskState = LINE_TASK_STATE_TX;
    pLineState->lineTask = LineTask_SendAndReceive;
}

//------------------------------------------------------------------------------
//  Odesle data na linku (neblokujici volani, konec transakce
//  se testuje pomoci funkce HalfDuplexLine_Pending),
//  volajici zajisti dostupnost dat po dobu vysilani.
//------------------------------------------------------------------------------
void HalfDuplexLine_Send(THalfDuplexLineId id, uint8_t* buffer, uint16_t sendCount)
{
    TLineState *pLineState = GetLineState(id);
    if (pLineState == NULL)
        return;

    if (!pLineState->isOpen)
        return;

    pLineState->buffer = buffer;
    pLineState->bufferDataCount = sendCount;
    pLineState->lineTaskState = LINE_TASK_STATE_TX;
    pLineState->lineTask = LineTask_Send;
}

//------------------------------------------------------------------------------
//  Prijme data z linky (neblokujici volani, konec transakce
//  se testuje pomoci funkce HalfDuplexLine_Pending),
//  volajici zajisti dostatek mista v buferu.
//------------------------------------------------------------------------------
void HalfDuplexLine_Receive(THalfDuplexLineId id, uint8_t* buffer, uint16_t bufferSize)
{
    TLineState *pLineState = GetLineState(id);
    if (pLineState == NULL)
        return;

    if (!pLineState->isOpen)
        return;

    pLineState->buffer = buffer;
    pLineState->bufferSize = bufferSize;
    pLineState->lineTaskState = LINE_TASK_STATE_RX;
    pLineState->lineTask = LineTask_Receive;
}

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce na lince (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL HalfDuplexLine_Pending(THalfDuplexLineId id)
{
    TLineState *pLineState = GetLineState(id);
    if (pLineState == NULL)
        return FALSE;

    if (!pLineState->isOpen)
        return FALSE;

    return (pLineState->lineTask != NULL) ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Vraci pocet prijatych bytu v buferu z linky.
//------------------------------------------------------------------------------
uint16_t HalfDuplexLine_ReceivedBytes(THalfDuplexLineId id)
{
    TLineState *pLineState = GetLineState(id);
    if (pLineState == NULL)
        return 0;

    if (!pLineState->isOpen)
        return 0;

    return Uart_ReceivedBytes(pLineState->uartId);
}
