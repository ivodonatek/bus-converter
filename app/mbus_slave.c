//******************************************************************************
//  Mbus slave
//  - pouzita dokumentace na http://www.m-bus.com/mbusdoc/default.php
//*****************************************************************************/

#include "sys/sys_config.h"

#ifdef MBUS_HW_MODULE_TYPE

#include <string.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include "board/board_config.h"
#include "sys/setting.h"
#include "sys/log.h"
#include "sys/global_status.h"
#include "mbus_slave.h"
#include "half_duplex_line.h"
#include "mbus_protocol.h"
#include "modbus_client_magx2.h"
#include "xbus_modbus.h"

// EN 61107 manufacturer ID (three uppercase letters)
static const char MANUFACTURER_ID[] = "ARK";

// Fixed Data Header version of Variable Data Structure
#define VARIABLE_DATA_STRUCTURE_VERSION     1

// Timeout prijmu [ms] (After a time limit of (330 bit periods + 50 ms) the master
// interprets the lack of a reply as a fault and repeats the same telegram up to two times.)
#define RCV_TIMEOUT         45

#define MBUS_SLAVE_BUFFER_SIZE   2048  //delka prijimaciho/vysilaciho buferu

#define MBUS_END_OF_FRAME_CHARS     3 // pocet znaku pro vypocet klidu na konci prijateho ramce
#define MBUS_END_OF_FRAME_MIN_TIME  3 // minimalni cas klidu na konci prijateho ramce [ms]

// prototyp ulohy
typedef void (*TMbusSlaveTask)(void);

// stavy uloh
typedef enum
{
    MBUS_SLAVE_TASK_STATE_RX,        // prijem
    MBUS_SLAVE_TASK_STATE_RX_WAIT,   // cekani na dokonceni prijmu
    MBUS_SLAVE_TASK_STATE_TX,        // vysilani
    MBUS_SLAVE_TASK_STATE_TX_WAIT,   // cekani na dokonceni vysilani

} TMbusSlaveTaskState;

// Stav
static struct
{
    // stav otevreni
    BOOL isOpen;

    // ID linky
    THalfDuplexLineId lineId;

    // uloha
    //TMbusSlaveTask task;

    // stav ulohy
    TMbusSlaveTaskState taskState;

    // bufer
    uint8_t buffer[MBUS_SLAVE_BUFFER_SIZE];

    // pocet obsazenych bytu v buferu
    uint16_t bufferDataCount;

} state;

// MBus ramec
static mbus_frame frame;

// for Access No., The Access Number has unsigned binary coding, and is increased by one after each RSP_UD from the slave
static unsigned char rspUdCounter = 0;

// Variable Data Blocks record
static mbus_data_record mbusDataRecord;

// spocita dobu klidu za koncem prijateho ramce [ms]
static uint16_t GetFrameEndDelay(void)
{
    uint8_t bits = Uart_GetFrameBits(Setting_GetMbusUartDataBits(), Setting_GetMbusUartParity(),
        Setting_GetMbusUartStopBits());
    float frameTime = Uart_GetFrameTime(Setting_GetMbusUartBaudRate(), bits);
    float time = MBUS_END_OF_FRAME_CHARS * frameTime;
    return (time >= MBUS_END_OF_FRAME_MIN_TIME) ? (uint16_t)time : MBUS_END_OF_FRAME_MIN_TIME;
}

//------------------------------------------------------------------------------
//  Vrati prodlevu [ms] pred odeslanim dat na sbernici.
//  Viz. http://www.m-bus.com/mbusdoc/md5.php
//  � After the reception of a valid telegram the slave has to wait between
//  11 bit times and (330 bit times + 50ms) before answering (see also EN1434-3).
//------------------------------------------------------------------------------
static uint16_t GetDelayBeforeSendData(void)
{
    // 12bit pro jistotu, ale ne mensi nez 2ms
    uint16_t delay = (12 * 1000) / Setting_GetMbusUartBaudRate();
    if (delay < 2)
        delay = 2;

    return delay;
}

//------------------------------------------------------------------------------
//  Vrati prodlevu [ms] po odeslani dat na sbernici.
//------------------------------------------------------------------------------
static uint16_t GetDelayAfterSendData(void)
{
    // 2ms
    return 2;
}

// naplni parametry linky
static void FillLineParameters(THalfDuplexLineParameters* lineParams)
{
    lineParams->uartId = MBUS_UART_ID;
    lineParams->uartParams.baudRate = Setting_GetMbusUartBaudRate();
    lineParams->uartParams.dataBits = Setting_GetMbusUartDataBits();
    lineParams->uartParams.stopBits = Setting_GetMbusUartStopBits();
    lineParams->uartParams.parity = Setting_GetMbusUartParity();
    lineParams->dirOutput = IO_ID_None;
    lineParams->dirOutputTxLevel = 0;
    lineParams->responseTimeout = 0;
    lineParams->frameEndDelay = GetFrameEndDelay();
    lineParams->delayBeforeTx = GetDelayBeforeSendData();
    lineParams->delayAfterTx = GetDelayAfterSendData();
}

//------------------------------------------------------------------------------
//  Vraci Status pole hlavicky dat
//------------------------------------------------------------------------------
static unsigned char GetStatusField(void)
{
    return 0;
}

//------------------------------------------------------------------------------
//  Je prijaty (master->slave) ramec adresovan pro mne?
//------------------------------------------------------------------------------
static BOOL IsM2SFrameForMe(void)
{
    TMbusAddress frameAdr = frame.address;
    TMbusAddress mySlaveAdr = Setting_GetMbusSlaveAddress();

    if (mySlaveAdr == MBUS_ADDRESS_SLAVE_NONE)
        return FALSE;

    if ((MBUS_ADDRESS_SLAVE_MIN <= frameAdr) && (frameAdr <= MBUS_ADDRESS_SLAVE_MAX))
    {
        if (mySlaveAdr == frameAdr)
            return TRUE;
    }
    else
    {
        if (frameAdr == MBUS_ADDRESS_BROADCAST_REPLY)
            return TRUE;

        if (frameAdr == MBUS_ADDRESS_BROADCAST_NOREPLY)
            return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------
//  Vyplni hlavicku variabilnich dat.
//------------------------------------------------------------------------------
static void FillVariableHeader(mbus_data_variable_header *header)
{
    //Ident.Nr. Manufr. Version Medium Access No. Status  Signature
    //4 Byte    2 Byte  1 Byte  1 Byte   1 Byte   1 Byte  2 Byte

    mbus_data_bcd_encode(header->id_bcd, 4, Setting_GetIdentificationNumber());

    unsigned int manId = mbus_manufacturer_id((char*)MANUFACTURER_ID);
    header->manufacturer[0] = (unsigned char)manId;
    header->manufacturer[1] = (unsigned char)(manId >> 8);

    header->version = VARIABLE_DATA_STRUCTURE_VERSION;
    header->medium = MBUS_VARIABLE_DATA_MEDIUM_WATER;
    header->access_no = rspUdCounter;
    header->status = GetStatusField();

    header->signature[0] = 0;
    header->signature[1] = 0;
}

//------------------------------------------------------------------------------
//  Vraci velikost bloku dat v bytech uvnitr ramce.
//------------------------------------------------------------------------------
static size_t GetDataRecordSizeInFrame(const mbus_data_record *record)
{
    return 2 + record->drh.dib.ndife + record->drh.vib.nvife + record->data_len;
}

//------------------------------------------------------------------------------
//  Vlozi blok dat do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertDataRecordIntoFrame(const mbus_data_record *record)
{
    size_t recordSize = GetDataRecordSizeInFrame(record);
    if ((recordSize + frame.data_size) > MBUS_FRAME_DATA_LENGTH)
        return FALSE;

    // DIF
    frame.data[frame.data_size++] = record->drh.dib.dif;

    // DIFE
    int i;
    for (i = 0; i < record->drh.dib.ndife; i++)
        frame.data[frame.data_size++] = record->drh.dib.dife[i];

    // VIF
    frame.data[frame.data_size++] = record->drh.vib.vif;

    // VIFE
    for (i = 0; i < record->drh.vib.nvife; i++)
        frame.data[frame.data_size++] = record->drh.vib.vife[i];

    // Data
    for (i = 0; i < record->data_len; i++)
        frame.data[frame.data_size++] = record->data[i];

    return TRUE;
}

//------------------------------------------------------------------------------
//  Vlozi objemovy prutok [m3/h] do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertVolumeFlowIntoFrame(float value)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_VOLUME_FLOW_1M3H;
    memcpy(mbusDataRecord.data, &value, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi objem [m3] do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertVolumeIntoFrame(float value)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_VOLUME_1M3;
    memcpy(mbusDataRecord.data, &value, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi externi teplotu [C] do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertExtTempIntoFrame(float value)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_EXTERNAL_TEMPERATURE_1C;
    memcpy(mbusDataRecord.data, &value, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi tlak [bar] do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertPressureIntoFrame(float value)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_PRESSURE_1BAR;
    memcpy(mbusDataRecord.data, &value, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi bloky variabilnich dat do ramce. Pokud se nepodari, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertVariableDataBlocksIntoFrame(void)
{
    const TModbusClientMagX2ReadData* pReadData = XbusModbus_GetReadData();
    if (pReadData == NULL)
        return FALSE;

    if (!InsertVolumeFlowIntoFrame(pReadData->flow))
        return FALSE;

    if (!InsertVolumeIntoFrame(pReadData->total))
        return FALSE;

    if (!InsertVolumeIntoFrame(pReadData->aux))
        return FALSE;

    if (!InsertVolumeIntoFrame(pReadData->pos))
        return FALSE;

    if (!InsertVolumeIntoFrame(pReadData->neg))
        return FALSE;

    if (!InsertExtTempIntoFrame(pReadData->temp))
        return FALSE;

    if (!InsertExtTempIntoFrame(pReadData->extTemp))
        return FALSE;

    if (!InsertPressureIntoFrame(pReadData->extPress))
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------
//  Vytvoreni ramce ACK v buferu.
//------------------------------------------------------------------------------
static void CreateACK(void)
{
    state.buffer[0] = MBUS_FRAME_ACK_START;
    state.bufferDataCount = 1;
}

//------------------------------------------------------------------------------
//  Vytvoreni ramce UD2 (Class 2 Data) v buferu, volajici najde pripadny pripraveny
//  ramec k odeslani v state.buffer.
//------------------------------------------------------------------------------
static void CreateUD2(void)
{
    mbus_frame_init(&frame, MBUS_FRAME_TYPE_LONG);

    frame.control = MBUS_CONTROL_F_RSP_UD;
    frame.address = Setting_GetMbusSlaveAddress();
    frame.control_information = MBUS_CONTROL_INFO_RESP_VARIABLE;

    frame.data_size = 0;

    mbus_data_variable_header header;
    FillVariableHeader(&header);

    // pack variable data structure header
    frame.data[frame.data_size++] = header.id_bcd[0];
    frame.data[frame.data_size++] = header.id_bcd[1];
    frame.data[frame.data_size++] = header.id_bcd[2];
    frame.data[frame.data_size++] = header.id_bcd[3];
    frame.data[frame.data_size++] = header.manufacturer[0];
    frame.data[frame.data_size++] = header.manufacturer[1];
    frame.data[frame.data_size++] = header.version;
    frame.data[frame.data_size++] = header.medium;
    frame.data[frame.data_size++] = header.access_no;
    frame.data[frame.data_size++] = header.status;
    frame.data[frame.data_size++] = header.signature[0];
    frame.data[frame.data_size++] = header.signature[1];

    // pack variable data blocks
    BOOL insertResult = InsertVariableDataBlocksIntoFrame();

    int packResult = 0;
    if (insertResult)
        packResult = mbus_frame_pack(&frame, state.buffer, MBUS_SLAVE_BUFFER_SIZE);

    if (packResult > 0)
    {
        state.bufferDataCount = packResult;
        rspUdCounter++;
    }
    else
    {
        state.bufferDataCount = 0;
    }
}

//------------------------------------------------------------------------------
//  Zpracuje prijaty ramec - CONTROL, volajici najde pripadny pripraveny
//  ramec k odeslani v state.buffer.
//------------------------------------------------------------------------------
static void ProcessRcvFrameControl(void)
{
    unsigned char func = frame.control & MBUS_CONTROL_MASK_F;

    switch (func)
    {
    case MBUS_CONTROL_F_SND_NKE:
    case MBUS_CONTROL_F_SND_UD:
        {
            if (frame.address == MBUS_ADDRESS_BROADCAST_NOREPLY)
                state.bufferDataCount = 0;
            else
                CreateACK();
        }
        break;

    case MBUS_CONTROL_F_REQ_UD2:
    case MBUS_CONTROL_F_REQ_UD1:
        {
            if (frame.address == MBUS_ADDRESS_BROADCAST_NOREPLY)
                state.bufferDataCount = 0;
            else
                CreateUD2();
        }
        break;

    default:
        state.bufferDataCount = 0;
        break;
    }
}

//------------------------------------------------------------------------------
//  Zpracuje prijaty ramec, volajici najde pripadny pripraveny ramec k odeslani
//  v state.buffer.
//------------------------------------------------------------------------------
static void ProcessRcvFrame(void)
{
    switch (frame.type)
    {
    case MBUS_FRAME_TYPE_SHORT:
    case MBUS_FRAME_TYPE_CONTROL:
    case MBUS_FRAME_TYPE_LONG:
        if ((mbus_frame_direction(&frame) == MBUS_CONTROL_MASK_DIR_M2S) &&
            (IsM2SFrameForMe()))
        {
            ProcessRcvFrameControl();
        }
        else
        {
            state.bufferDataCount = 0;
        }
        break;

    default:
        state.bufferDataCount = 0;
        break;
    }
}

//------------------------------------------------------------------------------
//  Zpracuje prijata data, volajici najde pripadny pripraveny ramec k odeslani
//  v state.buffer.
//------------------------------------------------------------------------------
static void ProcessRcvData(void)
{
    memset((void *)&frame, 0, sizeof(mbus_frame));

    int parseResult = mbus_parse(&frame, state.buffer, state.bufferDataCount);

    if (parseResult == 0)
    {
        ProcessRcvFrame();
    }
    else // error
    {
        state.bufferDataCount = 0;
    }
}

// ulohy
// uloha - MBus slave
static void Task_MbusSlave(void)
{
    switch (state.taskState)
    {
    case MBUS_SLAVE_TASK_STATE_RX:
        HalfDuplexLine_Receive(state.lineId, state.buffer, MBUS_SLAVE_BUFFER_SIZE);
        state.taskState = MBUS_SLAVE_TASK_STATE_RX_WAIT;
        break;

    case MBUS_SLAVE_TASK_STATE_RX_WAIT:
        if (!HalfDuplexLine_Pending(state.lineId))
        {
            state.bufferDataCount = HalfDuplexLine_ReceivedBytes(state.lineId);
            ProcessRcvData();
            state.taskState = (state.bufferDataCount > 0) ? MBUS_SLAVE_TASK_STATE_TX : MBUS_SLAVE_TASK_STATE_RX;
        }
        break;

    case MBUS_SLAVE_TASK_STATE_TX:
        HalfDuplexLine_Send(state.lineId, state.buffer, state.bufferDataCount);
        state.taskState = MBUS_SLAVE_TASK_STATE_TX_WAIT;
        break;

    case MBUS_SLAVE_TASK_STATE_TX_WAIT:
        if (!HalfDuplexLine_Pending(state.lineId))
        {
            state.taskState = MBUS_SLAVE_TASK_STATE_RX;
        }
        break;

    default:
        break;
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void MbusSlave_Init(void)
{
    state.isOpen = FALSE;
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void MbusSlave_Task(void)
{
    if (state.isOpen)
        Task_MbusSlave();
}

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL MbusSlave_Open(void)
{
    MbusSlave_Close();

    THalfDuplexLineParameters lineParams;
    FillLineParameters(&lineParams);

    state.lineId = HalfDuplexLine_Open(&lineParams);
    if (state.lineId == HALF_DUPLEX_LINE_ID_NONE)
        return FALSE;

    state.isOpen = TRUE;
    state.taskState = MBUS_SLAVE_TASK_STATE_RX;
    return TRUE;
}

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void MbusSlave_Close(void)
{
    if (!state.isOpen)
        return;

    HalfDuplexLine_Close(state.lineId);
    state.isOpen = FALSE;
}

#endif // MBUS_HW_MODULE_TYPE
