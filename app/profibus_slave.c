//******************************************************************************
//  Profibus slave
//*****************************************************************************/

#include "sys/sys_config.h"

#ifdef PROFIBUS_HW_MODULE_TYPE

#include <string.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include "machine/api.h"
#include "board/board_config.h"
#include "sys/setting.h"
#include "sys/systick.h"
#include "sys/log.h"
#include "sys/global_status.h"
#include "profibus_slave.h"
#include "device/vpc3.h"
#include "modbus_client_magx2.h"
#include "xbus_modbus.h"

// stav ulohy
typedef enum
{
    PROFIBUS_TS_Nothing,
    PROFIBUS_TS_SetResetVPC3,
    PROFIBUS_TS_SetResetVPC3Delay,
    PROFIBUS_TS_ClrResetVPC3Delay,
    PROFIBUS_TS_MemoryTest,
    PROFIBUS_TS_MemoryTestWrite,
    PROFIBUS_TS_MemoryTestRead,
    PROFIBUS_TS_InitVPC3,
    PROFIBUS_TS_InitVPC3Clear,
    PROFIBUS_TS_InitVPC3Copy,
    PROFIBUS_TS_InitVPC3CopyCfg,
    PROFIBUS_TS_Main,
    PROFIBUS_TS_Poll,
    PROFIBUS_TS_PollPrm,
    PROFIBUS_TS_PollPrmCopy,
    PROFIBUS_TS_PollCfg,
    PROFIBUS_TS_PollCfgLoop,
    PROFIBUS_TS_PollCfgLoopCopy,
    PROFIBUS_TS_PollTimeout,
    PROFIBUS_TS_PollGoLeaveDataExchange,
    PROFIBUS_TS_PollDxOut,
    PROFIBUS_TS_PollNewGcCommand,
    PROFIBUS_TS_PollNewSsaData,
    PROFIBUS_TS_PollBaudrateDetect,
    PROFIBUS_TS_PollDxbOut,
    PROFIBUS_TS_PollDxbLinkError,
    PROFIBUS_TS_PollPollEnd,
    PROFIBUS_TS_PollIndication,
    PROFIBUS_TS_PollIntAck,
    PROFIBUS_TS_State,
    PROFIBUS_TS_DpState,
    PROFIBUS_TS_CheckEvIoOut,
    PROFIBUS_TS_ApplicationReady

} TProfibusSlaveTaskState;

// PROFIBUS-DP diagnostics
#define MAX_DIAG_RETRY 10

// -- defines for user alarm state
#define USER_AL_STATE_CLOSED        ((uint8_t)0x00)
#define USER_AL_STATE_OPEN          ((uint8_t)0x01)
// -- defines for diagnostics
#define USER_TYPE_RESET_DIAG        ((uint8_t)0xF8)
#define USER_TYPE_DPV0              ((uint8_t)0xFA)
#define USER_TYPE_PRM_OK            ((uint8_t)0xFB)
#define USER_TYPE_PRM_NOK           ((uint8_t)0xFC)
#define USER_TYPE_CFG_OK            ((uint8_t)0xFD)
#define USER_TYPE_CFG_NOK           ((uint8_t)0xFE)
#define USER_TYPE_APPL_RDY          ((uint8_t)0xFF)

/** Events for PROFIBUS Application */
typedef enum
{
   eDpApplEv_Init              = 0x0000,   /**< Init value. */
   eDpApplEv_ResetBasp         = 0x0001,   /**< Enable BASP signal. */
   eDpApplEv_SetBasp           = 0x0002,   /**< Disable BASP signal. */
   eDpApplEv_IoIn              = 0x0010,   /**< Read input data. */
   eDpApplEv_IoOut             = 0x0020,   /**< Write output data. */
   eDpApplEv_Freeze            = 0x0100,   /**< Write freeze command. */
   eDpApplEv_UnFreeze          = 0x0200,   /**< Write unfreeze command. */
   eDpApplEv_Sync              = 0x0400,   /**< Write sync command. */
   eDpApplEv_UnSync            = 0x0800,   /**< Write unsync command. */
   eDpApplEv_FwUpd             = 0x1000,   /**< Firmware update command. */
   eDpApplEv_PollEepromRW      = 0x2000,   /**< Poll EEPROM Read command. */
   eDpApplEv_PollServChnRW     = 0x4000    /**< Poll service channel. */
} eDpApplEv_Flags;

typedef struct
{
   eDpApplEv_Flags   eDpApplEvent;                 /**< User application structure. */

   CFG_STRUCT        sCfgData;                     /**< Default PROFIBUS configuration data. */

   uint8_t           abDpOutputData[DOUT_BUFSIZE]; /**< Buffer for PROFIBUS output data ( master --> slave ). */
   uint8_t           abDpInputData[DIN_BUFSIZE];   /**< Buffer for PROFIBUS input data ( slave to master ). */
} DP_APPL_STRUC;

// stav ulohy
static TProfibusSlaveTaskState taskState;

static VPC3_STRUC_PTR             pVpc3;               /**< Pointer to VPC3+ structure. */
//static VPC3_STRUC_PTR             pVpc3Channel1;       /**< Pointer to VPC3+ structure channel 1. */

static VPC3_SYSTEM_STRUC_PTR      pDpSystem;           /**< Pointer to global profibus system structure. */
static VPC3_SYSTEM_STRUC          sDpSystemChannel1;   /**< Global profibus system structure. */

static VPC3_STRUC_ERRCB           sVpc3Error;          /**< Error structure. */
static DP_APPL_STRUC              sDpAppl;             /**< User application structure. */

static VPC3_STRUC              sVpc3OnlyForInit;    /**< Structure is used for VPC3+ initialization in serial mode. */

//default configuration data for startup
//#define DpApplCfgDataLength ((uint8_t)0x01)     /**< Length of configuration data. */
#define DpApplCfgDataLength ((uint8_t)0x02)     /**< Length of configuration data. */
//static const uint8_t DpApplDefCfg[1] = { 0x31 };  /**< Default configuration data. */
static const uint8_t DpApplDefCfg[DpApplCfgDataLength] = { 0x1F, 0x1F };  /**< Default configuration data. */

//static uint8_t masterData[2]; //2 bytes of data for the PB master device

//------------------------------------------------------------------------------
//  Vlozi vstupni data Profibus slave typu float do buferu.
//
//  Float (IEEE754 Single precision 32-bit)
//  buffer[0] ... low byte
//  buffer[3] ... high byte
//------------------------------------------------------------------------------
static void InsertSlaveInputFloatIntoBuffer(uint8_t *buffer, float value)
{
    uint8_t i;
    uint8_t *pByte = (uint8_t *)&value;

    for (i = 0; i < sizeof(float); i++)
        buffer[i] = pByte[i];
}

//------------------------------------------------------------------------------
//  Vlozi vstupni data Profibus slave do buferu. Pokud se nepodari, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertSlaveInputDataIntoBuffer(uint8_t *buffer)
{
    uint8_t i;

    for (i = 0; i < DIN_BUFSIZE; i++)
        buffer[i] = 0;

    const TModbusClientMagX2ReadData* pReadData = XbusModbus_GetReadData();
    if (pReadData == NULL)
        return FALSE;

    InsertSlaveInputFloatIntoBuffer(&buffer[0], pReadData->flow);
    InsertSlaveInputFloatIntoBuffer(&buffer[4], pReadData->total);
    InsertSlaveInputFloatIntoBuffer(&buffer[8], pReadData->aux);
    InsertSlaveInputFloatIntoBuffer(&buffer[12], pReadData->pos);
    InsertSlaveInputFloatIntoBuffer(&buffer[16], pReadData->neg);
    InsertSlaveInputFloatIntoBuffer(&buffer[20], pReadData->temp);
    InsertSlaveInputFloatIntoBuffer(&buffer[24], pReadData->extTemp);
    InsertSlaveInputFloatIntoBuffer(&buffer[28], pReadData->extPress);

    return TRUE;
}

/*---------------------------------------------------------------------------*/
/* function: DpPrm_Init                                                      */
/*---------------------------------------------------------------------------*/
/**
 * @brief Initializes local structures.
 *
 * This function is called during startup and with each received parameter telegram from PROFIBUS-DP master.
 */
static void DpPrm_Init( void )
{
   pDpSystem->eDPV1 = DPV0_MODE;
}

/*!
  \brief Init profibus configuration.
*/
static void DpCfg_Init( void )
{
   //todo:
   //insert your real configuration data here
   sDpAppl.sCfgData.bLength = DpApplCfgDataLength; // length of configuration data
   memcpy( &sDpAppl.sCfgData.abData[0], &DpApplDefCfg[0], sDpAppl.sCfgData.bLength );
}

/*---------------------------------------------------------------------------*/
/* function: DpDiag_Init                                                     */
/*---------------------------------------------------------------------------*/
/**
 * @brief Initializes local structures.
 *
 * This function is called during startup and with each received parameter telegram from PROFIBUS-DP master.
 */
static void DpDiag_Init( void )
{
   //nothing todo in this example.
}

/*--------------------------------------------------------------------------*/
/* function: DpDiag_AlarmInit                                               */
/*--------------------------------------------------------------------------*/
/**
 * @brief Initializes user alarm structure.
 *
 * Set all values to zero.
 */
static void DpDiag_AlarmInit( void )
{
   //nothing todo in this example.
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_SetConstants                                               */
/*---------------------------------------------------------------------------*/
/*!
  \brief Initializes VPC3+ with all constant values.

  \param[in] bSlaveAddress - PROFIBUS slave address ( range: 1..126 )
  \param[in] wIdentNumber - PROFIBUS ident number

  \retval DP_OK - Initialization OK
  \retval DP_ADDRESS_ERROR - Error, DP Slave address
*/
static DP_ERROR_CODE VPC3_SetConstants( uint8_t bSlaveAddress, uint16_t wIdentNumber )
{
   DP_ERROR_CODE bError;

   bError = DP_OK;

   pDpSystem->bDinBufsize  = DIN_BUFSIZE;
   pDpSystem->bDoutBufsize = DOUT_BUFSIZE;
   pDpSystem->bPrmBufsize  = PRM_BUFSIZE;
   pDpSystem->bDiagBufsize = DIAG_BUFSIZE;
   pDpSystem->bCfgBufsize  = CFG_BUFSIZE;
   pDpSystem->bSsaBufsize  = SSA_BUFSIZE;

   pDpSystem->wAsicUserRam = ASIC_USER_RAM;

   /*-----------------------------------------------------------------------*/
   /* initialize  control logic                                             */
   /*-----------------------------------------------------------------------*/
   pVpc3->bIntReqReg_L VPC3_EXTENSION                = 0x00;
   pVpc3->bIntReqReg_H VPC3_EXTENSION                = 0x00;
   pVpc3->sReg.sWrite.bIntAck_L VPC3_EXTENSION       = 0xFF;
   pVpc3->sReg.sWrite.bIntAck_H VPC3_EXTENSION       = 0xFF;
   pVpc3->sCtrlPrm.sWrite.bModeReg1_R VPC3_EXTENSION = 0xFF;

   Vpc3Write( bVpc3RwIntReqReg_L, 0x00 );
   Vpc3Write( bVpc3RwIntReqReg_H, 0x00 );

   Vpc3Write( bVpc3WoIntAck_L,    0xFF );
   Vpc3Write( bVpc3WoIntAck_H,    0xFF );
   Vpc3Write( bVpc3WoModeReg1_R,  0xFF );

   /*-----------------------------------------------------------------------*/
   /* set modes of vpc3                                                     */
   /*-----------------------------------------------------------------------*/
   pVpc3->bModeReg0_H VPC3_EXTENSION = INIT_VPC3_MODE_REG_H;
   pVpc3->bModeReg0_L VPC3_EXTENSION = INIT_VPC3_MODE_REG_L;

   pVpc3->sCtrlPrm.sWrite.bModeReg2 VPC3_EXTENSION = INIT_VPC3_MODE_REG_2;
   pVpc3->sCtrlPrm.sWrite.bModeReg3 VPC3_EXTENSION = INIT_VPC3_MODE_REG_3;

   Vpc3Write( bVpc3RwModeReg0_H, INIT_VPC3_MODE_REG_H );
   Vpc3Write( bVpc3RwModeReg0_L, INIT_VPC3_MODE_REG_L );

   Vpc3Write( bVpc3WoModeReg2, INIT_VPC3_MODE_REG_2 );
   Vpc3Write( bVpc3WoModeReg3, INIT_VPC3_MODE_REG_3 );

   /*-----------------------------------------------------------------------*/
   /* set interrupt triggers                                                */
   /*-----------------------------------------------------------------------*/
   pVpc3->sReg.sWrite.bIntMask_H VPC3_EXTENSION = (uint8_t)(~(INIT_VPC3_IND_H));
   pVpc3->sReg.sWrite.bIntMask_L VPC3_EXTENSION = (uint8_t)(~(INIT_VPC3_IND_L));

   pDpSystem->bIntIndHigh = (uint8_t)(~(INIT_VPC3_IND_H));
   pDpSystem->bIntIndLow  = (uint8_t)(~(INIT_VPC3_IND_L));

   Vpc3Write( bVpc3WoIntMask_H, (uint8_t)(~(INIT_VPC3_IND_H)) );
   Vpc3Write( bVpc3WoIntMask_L, (uint8_t)(~(INIT_VPC3_IND_L)) );

   /*-----------------------------------------------------------------------*/
   /* set time-variables                                                    */
   /*-----------------------------------------------------------------------*/
   pVpc3->sCtrlPrm.sWrite.bWdBaudControlVal VPC3_EXTENSION = 0x10;
   pVpc3->sCtrlPrm.sWrite.bMinTsdrVal VPC3_EXTENSION       = 0x0B;

   Vpc3Write( bVpc3WoWdBaudControlVal, 0x10 );
   Vpc3Write( bVpc3WoMinTsdrVal, 0x0B );

   /*-----------------------------------------------------------------------*/
   /* set variables for synch-mode                                          */
   /*-----------------------------------------------------------------------*/
   #if DP_ISOCHRONOUS_MODE
      pVpc3->sCtrlPrm.sWrite.bSyncPwReg VPC3_EXTENSION = SYNCH_PULSEWIDTH;
      pVpc3->sCtrlPrm.sWrite.bGroupSelectReg VPC3_EXTENSION = 0x80;
      pVpc3->sCtrlPrm.sWrite.bControlCommandReg VPC3_EXTENSION  = 0x00;
      pVpc3->sCtrlPrm.sWrite.bGroupSelectMask VPC3_EXTENSION = 0x00;
      pVpc3->sCtrlPrm.sWrite.bControlCommandMask VPC3_EXTENSION  = 0x00;

      Vpc3Write( bVpc3WoSyncPwReg, SYNCH_PULSEWIDTH );
      Vpc3Write( bVpc3WoGroupSelectReg, 0x80 );
      Vpc3Write( bVpc3WoControlCommandReg, 0x00 );
      Vpc3Write( bVpc3WoGroupSelectMask, 0x00 );
      Vpc3Write( bVpc3WoControlCommandMask, 0x00 );
   #endif /* #if DP_ISOCHRONOUS_MODE */

   /*-----------------------------------------------------------------------*/
   /* set user watchdog (dataexchange telegram counter)                     */
   /*-----------------------------------------------------------------------*/
   pVpc3->abUserWdValue[1] VPC3_EXTENSION = (uint8_t)(USER_WD >> 8);
   pVpc3->abUserWdValue[0] VPC3_EXTENSION = (uint8_t)(USER_WD);

   /*-----------------------------------------------------------------------*/
   /* set pointer to FF                                                     */
   /*-----------------------------------------------------------------------*/
   pVpc3->bFdlSapListPtr   VPC3_EXTENSION = VPC3_SAP_CTRL_LIST_START;
   VPC3_SET_EMPTY_SAP_LIST();

   /*-----------------------------------------------------------------------*/
   /* ssa support                                                           */
   /*-----------------------------------------------------------------------*/
   pVpc3->bRealNoAddChange VPC3_EXTENSION = ( SSA_BUFSIZE == 0 ) ? 0xFF : 0x00;

   /*-----------------------------------------------------------------------*/
   /* set profibus ident number                                             */
   /*-----------------------------------------------------------------------*/
   pVpc3->bIdentHigh VPC3_EXTENSION = (uint8_t)(wIdentNumber >> 8);
   pVpc3->bIdentLow  VPC3_EXTENSION = (uint8_t)(wIdentNumber);

   /*-----------------------------------------------------------------------*/
   /* set and check slave address                                           */
   /*-----------------------------------------------------------------------*/
   if( bSlaveAddress < 127 && bSlaveAddress != 0 )
   {
      pVpc3->bTsAddr VPC3_EXTENSION = bSlaveAddress;
   }
   else
   {
      bError = DP_ADDRESS_ERROR;
   }

   return bError;
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_CalculateInpOutpLength                                     */
/*---------------------------------------------------------------------------*/
/*!
   \brief Calculation of input- and output data.

   \param[in] pToCfgData - address of configuration data
   \param[in] bCfgLength - length of configuration data

   \retval DP_OK - Initialization OK
   \retval DP_CFG_LEN_ERROR - Error with CFG length
   \retval DP_CALCULATE_IO_ERROR - Error with DIN or DOUT length
   \retval DP_CFG_FORMAT_ERROR - Error in special configuration format
*/
static DP_ERROR_CODE VPC3_CalculateInpOutpLength( MEM_UNSIGNED8_PTR pToCfgData, uint8_t bCfgLength )
{
   DP_ERROR_CODE bError;
   uint8_t       bSpecificDataLength;
   uint8_t       bTempOutputDataLength;
   uint8_t       bTempInputDataLength;
   uint8_t       bLength;
   uint8_t       bCount;

   bError = DP_OK;
   bTempOutputDataLength = 0;
   bTempInputDataLength  = 0;

   if( ( bCfgLength > 0 ) && ( bCfgLength <= CFG_BUFSIZE ) )
   {
      for( ; bCfgLength > 0; bCfgLength -= bCount )
      {
         bCount = 0;

         if( *pToCfgData & VPC3_CFG_IS_BYTE_FORMAT )
         {
            /* general identifier format */
            bCount++;
            /* pToCfgData points to "Configurationbyte", CFG_BF means "CFG_IS_BYTE_FORMAT" */
            bLength = (uint8_t)( ( *pToCfgData & VPC3_CFG_BF_LENGTH) + 1 );

            if( *pToCfgData & VPC3_CFG_BF_OUTP_EXIST )
            {
               bTempOutputDataLength += ( *pToCfgData & VPC3_CFG_LENGTH_IS_WORD_FORMAT ) ? ( 2 * bLength ) : bLength;
            } /* if( *pToCfgData & VPC3_CFG_BF_OUTP_EXIST ) */

            if( *pToCfgData & VPC3_CFG_BF_INP_EXIST )
            {
               bTempInputDataLength += ( *pToCfgData & VPC3_CFG_LENGTH_IS_WORD_FORMAT ) ? ( 2 * bLength ) : bLength;
            } /* if( *pToCfgData & VPC3_CFG_BF_INP_EXIST ) */

            pToCfgData++;
         } /* if( *pToCfgData & VPC3_CFG_IS_BYTE_FORMAT ) */
         else
         {
            /* pToCfgData points to the headerbyte of "special identifier format */
            /* CFG_SF means "CFG_IS_SPECIAL_FORMAT" */
            if( *pToCfgData & VPC3_CFG_SF_OUTP_EXIST )
            {
               bCount++;    /* next byte contains the length of outp_data */
               bLength = (uint8_t)( ( *( pToCfgData + bCount ) & VPC3_CFG_SF_LENGTH ) + 1 );

               bTempOutputDataLength += ( *( pToCfgData + bCount ) & VPC3_CFG_LENGTH_IS_WORD_FORMAT ) ? ( 2 * bLength ) : bLength;
            } /* if( *pToCfgData & VPC3_CFG_SF_OUTP_EXIST ) */

            if( *pToCfgData & VPC3_CFG_SF_INP_EXIST )
            {
               bCount++;  /* next byte contains the length of inp_data */
               bLength = (uint8_t)( ( *( pToCfgData + bCount ) & VPC3_CFG_SF_LENGTH ) + 1 );

               bTempInputDataLength += ( *( pToCfgData + bCount ) & VPC3_CFG_LENGTH_IS_WORD_FORMAT ) ? ( 2 * bLength ) : bLength;
            } /* if( *pToCfgData & VPC3_CFG_SF_INP_EXIST ) */

            bSpecificDataLength = (uint8_t)( *pToCfgData & VPC3_CFG_BF_LENGTH );

            if( bSpecificDataLength != 15 )
            {
               bCount = (uint8_t)( bCount + 1 + bSpecificDataLength );
               pToCfgData = pToCfgData + bCount;
            } /* if( bSpecificDataLength != 15 ) */
            else
            {
               /* specific data length = 15 not allowed */
               pDpSystem->bInputDataLength  = 0xFF;
               pDpSystem->bOutputDataLength = 0xFF;
               bError = DP_CFG_FORMAT_ERROR;
            } /* else of if( bSpecificDataLength != 15 ) */
         } /* else of if( *pToCfgData & VPC3_CFG_IS_BYTE_FORMAT ) */
      } /* for( ; bCfgLength > 0; bCfgLength -= bCount ) */

      if( ( bCfgLength != 0 ) || ( bTempInputDataLength > DIN_BUFSIZE ) || ( bTempOutputDataLength > DOUT_BUFSIZE ) )
      {
         pDpSystem->bInputDataLength  = 0xFF;
         pDpSystem->bOutputDataLength = 0xFF;

         bError = DP_CALCULATE_IO_ERROR;
      } /* if( ( bCfgLength != 0 ) || ( bTempInputDataLength > DIN_BUFSIZE ) || ( bTempOutputDataLength > DOUT_BUFSIZE ) ) */
      else
      {
         pDpSystem->bInputDataLength  = bTempInputDataLength;
         pDpSystem->bOutputDataLength = bTempOutputDataLength;
         bError = DP_OK;
      } /* else of if( ( bCfgLength != 0 ) || ( bTempInputDataLength > DIN_BUFSIZE ) || ( bTempOutputDataLength > DOUT_BUFSIZE ) ) */
   } /* if( ( bCfgLength > 0 ) && ( bCfgLength <= CFG_BUFSIZE ) ) */
   else
   {
      pDpSystem->bInputDataLength  = 0xFF;
      pDpSystem->bOutputDataLength = 0xFF;
      bError = DP_CFG_LEN_ERROR;
   } /* else of if( ( bCfgLength > 0 ) && ( bCfgLength <= CFG_BUFSIZE ) ) */

   return bError;
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_InitBufferStructure                                        */
/*---------------------------------------------------------------------------*/
/*!
  \brief Initializes VPC3+ buffer structure.

  \retval DP_OK - Initialization OK
  \retval DP_DOUT_LEN_ERROR - Error with Dout length
  \retval DP_DIN_LEN_ERROR - Error with Din length
  \retval DP_DIAG_LEN_ERROR - Error with diagnostics length
  \retval DP_PRM_LEN_ERROR - Error with parameter assignment data length
  \retval DP_SSA_LEN_ERROR - Error with address data length
  \retval DP_CFG_LEN_ERROR - Error with configuration data length
  \retval DP_LESS_MEM_ERROR - Error Overall, too much memory used
*/
static DP_ERROR_CODE VPC3_InitBufferStructure( void )
{
   DP_ERROR_CODE  bError;
   uint16_t       wOutBufferLength;    /**< calculated length of output buffer */
   uint16_t       wInBufferLength;     /**< calculated length of input buffer */
   uint16_t       wDiagBufferLength;   /**< calculated length of diagnostic buffer */
   uint16_t       wPrmBufferLength;    /**< calculated length of parameter buffer */
   uint16_t       wCfgBufferLength;    /**< calculated length of check-config buffer */
   uint16_t       wSsaBufferLength;    /**< calculated length of set-slave-address buffer */
   uint16_t       wAux2BufferLength;   /**< calculated length of aux buffer 2*/

   bError = DP_OK;

   /*-----------------------------------------------------------------------*/
   /* check buffer length                                                   */
   /*-----------------------------------------------------------------------*/
   if( pDpSystem->bDoutBufsize > 244 )
   {
      bError = DP_DOUT_LEN_ERROR;
   } /* if( pDpSystem->bDoutBufsize > 244 ) */

   else
   if( pDpSystem->bDinBufsize > 244 )
   {
      bError = DP_DIN_LEN_ERROR;
   } /* if( pDpSystem->bDinBufsize > 244 ) */

   else
   if( ( pDpSystem->bDiagBufsize < 6 ) || ( pDpSystem->bDiagBufsize > 244 ) )
   {
      bError = DP_DIAG_LEN_ERROR;
   } /* if( ( pDpSystem->bDiagBufsize < 6 ) || ( pDpSystem->bDiagBufsize > 244 ) */

   else
   if( ( pDpSystem->bPrmBufsize < 7 ) || ( pDpSystem->bPrmBufsize > 244 ) )
   {
      bError = DP_PRM_LEN_ERROR;
   } /* if( ( pDpSystem->bPrmBufsize < 7 ) || ( pDpSystem->bPrmBufsize > 244 ) ) */

   else
   if( ( pDpSystem->bCfgBufsize < 1 ) || ( pDpSystem->bCfgBufsize > 244 ) )
   {
      bError = DP_CFG_LEN_ERROR;
   } /* if( ( pDpSystem->bCfgBufsize < 1 ) || ( pDpSystem->bCfgBufsize > 244 ) ) */

   else
   if( pDpSystem->bSsaBufsize != 0 && ( ( pDpSystem->bSsaBufsize < 4 ) || ( pDpSystem->bSsaBufsize > 244 ) ) )
   {
      bError = DP_SSA_LEN_ERROR;
   } /* if( pDpSystem->bSsaBufsize != 0 && ( ( pDpSystem->bSsaBufsize < 4 ) || ( pDpSystem->bSsaBufsize > 244 ) ) ) */

   /* prm buffer */
   if( pVpc3->bModeReg0_H VPC3_EXTENSION & VPC3_SPEC_PRM_BUF_MODE )
   {
      /* Spec_Prm_Buf_Mode: Is no longer supported by the software, 4kByte memory is enough to handle Set-Prm-telegram via auxiliary buffer. */
      bError = DP_SPEC_PRM_NOT_SUPP_ERROR;
   } /* if( pVpc3->bModeReg0_H VPC3_EXTENSION & VPC3_SPEC_PRM_BUF_MODE ) */
   pVpc3->bLenSpecPrmBuf VPC3_EXTENSION = 0;

   if( bError == DP_OK )
   {
      /*-------------------------------------------------------------------*/
      /* length of buffers ok, check memory consumption                    */
      /*-------------------------------------------------------------------*/
      /* length of output buffer */
      wOutBufferLength =  ( ( pDpSystem->bDoutBufsize + SEG_OFFSET ) & SEG_ADDWORD );
      /* length of input buffer */
      wInBufferLength =   ( ( pDpSystem->bDinBufsize  + SEG_OFFSET ) & SEG_ADDWORD );
      /* length of diagnostic buffer */
      wDiagBufferLength = ( ( pDpSystem->bDiagBufsize + SEG_OFFSET ) & SEG_ADDWORD );
      /* length of prm buffer */
      wPrmBufferLength =  ( ( pDpSystem->bPrmBufsize  + SEG_OFFSET ) & SEG_ADDWORD );
      /* length of cfg buffer */
      wCfgBufferLength =  ( ( pDpSystem->bCfgBufsize  + SEG_OFFSET ) & SEG_ADDWORD );
      /* length of ssa buffer */
      wSsaBufferLength =  ( ( pDpSystem->bSsaBufsize  + SEG_OFFSET ) & SEG_ADDWORD );
      /* length of aux buffer 2 2*/
      wAux2BufferLength = ( wCfgBufferLength > wSsaBufferLength ) ? wCfgBufferLength : wSsaBufferLength;

      /*-------------------------------------------------------------------*/
      /* check memory consumption                                          */
      /*-------------------------------------------------------------------*/
      pDpSystem->wVpc3UsedDPV0BufferMemory = 0;
      /* add input and output buffer */
      pDpSystem->wVpc3UsedDPV0BufferMemory += ( wOutBufferLength + wInBufferLength ) * 3;
      /* add diagnostic buffer */
      pDpSystem->wVpc3UsedDPV0BufferMemory += wDiagBufferLength * 2;
      /* add prm buffer, add aux buffer 1 */
      pDpSystem->wVpc3UsedDPV0BufferMemory += wPrmBufferLength * 2;
      /* add Read Config fuffer, add Check Config buffer */
      pDpSystem->wVpc3UsedDPV0BufferMemory += wCfgBufferLength * 2;
      /* add SSA buffer */
      pDpSystem->wVpc3UsedDPV0BufferMemory += wSsaBufferLength;
      /* add aux buffer 2 */
      pDpSystem->wVpc3UsedDPV0BufferMemory += wAux2BufferLength;

      if( pDpSystem->wVpc3UsedDPV0BufferMemory > pDpSystem->wAsicUserRam )
      {
         /* Error: user needs too much memory */
         pDpSystem->wVpc3UsedDPV0BufferMemory = 0;
         bError = DP_LESS_MEM_ERROR;
      } /* if( pDpSystem->wVpc3UsedDPV0BufferMemory > pDpSystem->wAsicUserRam ) */
      else
      {
         /*---------------------------------------------------------------*/
         /* set buffer pointer                                            */
         /*---------------------------------------------------------------*/
         pVpc3->abDoutBufPtr[0] VPC3_EXTENSION  = VPC3_DP_BUF_START;
         pVpc3->abDoutBufPtr[1] VPC3_EXTENSION  = pVpc3->abDoutBufPtr[0] VPC3_EXTENSION  + ( wOutBufferLength  >> SEG_MULDIV );
         pVpc3->abDoutBufPtr[2] VPC3_EXTENSION  = pVpc3->abDoutBufPtr[1] VPC3_EXTENSION  + ( wOutBufferLength  >> SEG_MULDIV );

         pVpc3->abDinBufPtr[0] VPC3_EXTENSION   = pVpc3->abDoutBufPtr[2] VPC3_EXTENSION  + ( wOutBufferLength  >> SEG_MULDIV );
         pVpc3->abDinBufPtr[1] VPC3_EXTENSION   = pVpc3->abDinBufPtr[0]  VPC3_EXTENSION  + ( wInBufferLength   >> SEG_MULDIV );
         pVpc3->abDinBufPtr[2] VPC3_EXTENSION   = pVpc3->abDinBufPtr[1]  VPC3_EXTENSION  + ( wInBufferLength   >> SEG_MULDIV );

         pVpc3->abDiagBufPtr[0] VPC3_EXTENSION  = pVpc3->abDinBufPtr[2]  VPC3_EXTENSION  + ( wInBufferLength   >> SEG_MULDIV );
         pVpc3->abDiagBufPtr[1] VPC3_EXTENSION  = pVpc3->abDiagBufPtr[0] VPC3_EXTENSION  + ( wDiagBufferLength >> SEG_MULDIV );

         pVpc3->bCfgBufPtr VPC3_EXTENSION       = pVpc3->abDiagBufPtr[1] VPC3_EXTENSION  + ( wDiagBufferLength >> SEG_MULDIV );
         pVpc3->bReadCfgBufPtr VPC3_EXTENSION   = pVpc3->bCfgBufPtr      VPC3_EXTENSION  + ( wCfgBufferLength  >> SEG_MULDIV );

         pVpc3->bPrmBufPtr VPC3_EXTENSION       = pVpc3->bReadCfgBufPtr  VPC3_EXTENSION  + ( wCfgBufferLength  >> SEG_MULDIV );

         pVpc3->bAuxBufSel VPC3_EXTENSION       = 0x06; /* SetPrm via Aux1, ChkCfg and SSA via Aux2 */
         pVpc3->abAuxBufPtr[0] VPC3_EXTENSION   = pVpc3->bPrmBufPtr VPC3_EXTENSION       + ( wPrmBufferLength  >> SEG_MULDIV );
         pVpc3->abAuxBufPtr[1] VPC3_EXTENSION   = pVpc3->abAuxBufPtr[0] VPC3_EXTENSION   + ( wPrmBufferLength  >> SEG_MULDIV );

         pVpc3->bSsaBufPtr VPC3_EXTENSION = 0;
         if( wSsaBufferLength != 0 )
         {
             pVpc3->bSsaBufPtr VPC3_EXTENSION   = pVpc3->abAuxBufPtr[1] VPC3_EXTENSION   + ( wAux2BufferLength >> SEG_MULDIV );
         } /* if( wSsaBufferLength != 0 ) */

         /*---------------------------------------------------------------*/
         /* set buffer length                                             */
         /*---------------------------------------------------------------*/
         pVpc3->bLenDoutBuf VPC3_EXTENSION     = pDpSystem->bOutputDataLength;
         pVpc3->bLenDinBuf  VPC3_EXTENSION     = pDpSystem->bInputDataLength;

         pVpc3->abLenDiagBuf[0] VPC3_EXTENSION = 6;
         pVpc3->abLenDiagBuf[1] VPC3_EXTENSION = 6;

         pVpc3->bLenCfgData VPC3_EXTENSION     = pDpSystem->bCfgBufsize;
         pVpc3->bLenPrmData VPC3_EXTENSION     = pDpSystem->bPrmBufsize;
         pVpc3->bLenSsaBuf  VPC3_EXTENSION     = pDpSystem->bSsaBufsize;

         pVpc3->abLenCtrlBuf[0] VPC3_EXTENSION = pDpSystem->bPrmBufsize;
         pVpc3->abLenCtrlBuf[1] VPC3_EXTENSION = ( wAux2BufferLength >= 244 ) ? 244 : wAux2BufferLength;

         /* for faster access, store some pointer in local structure */
         pDpSystem->pDoutBuffer1 = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->abDoutBufPtr[0] VPC3_EXTENSION) << SEG_MULDIV));                                                                                                                                                                                                       ;
         pDpSystem->pDoutBuffer2 = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->abDoutBufPtr[1] VPC3_EXTENSION) << SEG_MULDIV));
         pDpSystem->pDoutBuffer3 = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->abDoutBufPtr[2] VPC3_EXTENSION) << SEG_MULDIV));

         pDpSystem->pDinBuffer1  = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->abDinBufPtr[0] VPC3_EXTENSION) << SEG_MULDIV));
         pDpSystem->pDinBuffer2  = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->abDinBufPtr[1] VPC3_EXTENSION) << SEG_MULDIV));
         pDpSystem->pDinBuffer3  = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->abDinBufPtr[2] VPC3_EXTENSION) << SEG_MULDIV));

         pDpSystem->pDiagBuffer1 = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->abDiagBufPtr[0] VPC3_EXTENSION) << SEG_MULDIV));
         pDpSystem->pDiagBuffer2 = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->abDiagBufPtr[1] VPC3_EXTENSION) << SEG_MULDIV));
      } /* else of if( pDpSystem->wVpc3UsedDPV0BufferMemory > pDpSystem->wAsicUserRam ) */
   } /* if( bError == DP_OK ) */

   return bError;
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_InitSubscriber                                             */
/*---------------------------------------------------------------------------*/
#if DP_SUBSCRIBER
static DP_ERROR_CODE VPC3_InitSubscriber( void )
{
   DP_ERROR_CODE       bError;
   VPC3_UNSIGNED8_PTR  pToVpc3;              /**< pointer to VPC3, �Controller formatted */
   uint16_t            wDxbBufferLength;     /**< segmented length of DxB-Buffer */
   uint8_t             bVpc3SegmentAddress;  /**< segment address in VPC3-ASIC */

   bError = DP_OK;

   /*-----------------------------------------------------------------------*/
   /* check buffer length                                                   */
   /*-----------------------------------------------------------------------*/
   if( DP_MAX_DATA_PER_LINK > 244 )
   {
      bError = SSC_MAX_DATA_PER_LINK;
   } /* if( DP_MAX_DATA_PER_LINK > 244 ) */
   else
   {
      /* pointer mc formatted */
      pToVpc3 = (VPC3_UNSIGNED8_PTR)( Vpc3AsicAddress + DP_ORG_LENGTH + SAP_LENGTH + pDpSystem->wVpc3UsedDPV0BufferMemory );
      /* pointer vpc3 formatted */
      bVpc3SegmentAddress = (uint8_t)( ( pDpSystem->wVpc3UsedDPV0BufferMemory + DP_ORG_LENGTH + SAP_LENGTH ) >> SEG_MULDIV );

      /* length dxb_out */
      wDxbBufferLength = (((DP_MAX_DATA_PER_LINK+2)+SEG_OFFSET) & SEG_ADDBYTE);
      pVpc3->bLenDxbOutBuf = (DP_MAX_DATA_PER_LINK+2);
      pDpSystem->wVpc3UsedDPV0BufferMemory += (3*wDxbBufferLength);
      /* length link status */
      pVpc3->bLenDxbStatusBuf = ((((DP_MAX_LINK_SUPPORTED*2)+4)+SEG_OFFSET) & SEG_ADDBYTE);
      pDpSystem->wVpc3UsedDPV0BufferMemory += pVpc3->bLenDxbStatusBuf;
      /* length link table */
      pVpc3->bLenDxbLinkBuf = (((DP_MAX_LINK_SUPPORTED*4)+SEG_OFFSET) & SEG_ADDBYTE);
      pDpSystem->wVpc3UsedDPV0BufferMemory += pVpc3->bLenDxbLinkBuf;

      /*-------------------------------------------------------------------*/
      /* check memory consumption                                          */
      /*-------------------------------------------------------------------*/
      if( pDpSystem->wVpc3UsedDPV0BufferMemory > ASIC_USER_RAM )
      {
         /* Error: user needs too much memory */
         pDpSystem->wVpc3UsedDPV0BufferMemory = 0;
         bError = DP_LESS_MEM_ERROR;
      } /* if( pDpSystem->wVpc3UsedDPV0BufferMemory > ASIC_USER_RAM ) */
      else
      {
         /*---------------------------------------------------------------*/
         /* set buffer pointer                                            */
         /*---------------------------------------------------------------*/
         pVpc3->bDxbLinkBufPtr   = bVpc3SegmentAddress;
         pVpc3->bDxbStatusBufPtr = pVpc3->bDxbLinkBufPtr   + ( pVpc3->bLenDxbLinkBuf >> SEG_MULDIV );
         pVpc3->bDxbOutBufPtr1   = pVpc3->bDxbStatusBufPtr + ( pVpc3->bLenDxbStatusBuf >> SEG_MULDIV );
         pVpc3->bDxbOutBufPtr2   = pVpc3->bDxbOutBufPtr1   + ( wDxbBufferLength >> SEG_MULDIV );
         pVpc3->bDxbOutBufPtr3   = pVpc3->bDxbOutBufPtr2   + ( wDxbBufferLength >> SEG_MULDIV );

         pDpSystem->pDxbOutBuffer1 = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->bDxbOutBufPtr1 VPC3_EXTENSION) << SEG_MULDIV)+((VPC3_ADR)Vpc3AsicAddress));
         pDpSystem->pDxbOutBuffer2 = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->bDxbOutBufPtr2 VPC3_EXTENSION) << SEG_MULDIV)+((VPC3_ADR)Vpc3AsicAddress));
         pDpSystem->pDxbOutBuffer3 = (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(pVpc3->bDxbOutBufPtr3 VPC3_EXTENSION) << SEG_MULDIV)+((VPC3_ADR)Vpc3AsicAddress));
      } /* else of if( pDpSystem->wVpc3UsedDPV0BufferMemory > ASIC_USER_RAM ) */
   } /* else of if( DP_MAX_DATA_PER_LINK > 244 ) */

   return bError;
} /* static DP_ERROR_CODE VPC3_InitSubscriber( void ) */
#endif /* #if DP_SUBSCRIBER */

/*---------------------------------------------------------------------------*/
/* function: VPC3_GetDiagBufPtr                                              */
/*---------------------------------------------------------------------------*/
/*!
  \brief Fetch buffer pointer of the next diagnostic buffer.

  \param

  \retval !0 - pointer to current diagnostic buffer.
  \retval 0 - no buffer available
*/
static VPC3_UNSIGNED8_PTR VPC3_GetDiagBufPtr( void )
{
   VPC3_UNSIGNED8_PTR pToDiagBuffer;               /* pointer to diagnosis buffer */
   uint8_t            bDiagBufferSm;

   bDiagBufferSm = VPC3_GET_DIAG_BUFFER_SM();

   if( ( bDiagBufferSm & 0x03 ) == 0x01 )       /* locate Diag Buffer */
   {
      pToDiagBuffer = pDpSystem->pDiagBuffer1;
   } /* if( ( bDiagBufferSm & 0x03 ) == 0x01 ) */
   else
   {
      if( ( bDiagBufferSm & 0x0C ) == 0x04 )
      {
         pToDiagBuffer = pDpSystem->pDiagBuffer2;
      } /* if( ( bDiagBufferSm & 0x0C ) == 0x04 ) */
      else
      {
         pToDiagBuffer = VPC3_NULL_PTR;
      } /* else of if( ( bDiagBufferSm & 0x0C ) == 0x04 ) */
   } /* else of if( ( bDiagBufferSm & 0x03 ) == 0x01 ) */

   return pToDiagBuffer;
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_Start                                                      */
/*---------------------------------------------------------------------------*/
/*!
  \brief Set VPC3+ to online mode.
  If the ASIC could be correctly initialized with VPC3_Initialization(), it still has to be started. Between initialization and
  start, the user can still initialize buffers in the ASIC.

  Reaction after this service:
  - VPC3+ generates DX_OUT event, all outputs will be cleared
  - VPC3+ generates BAUDRATE_DETECT event, if master connected
  - master sends FDL-Status.req --> slave answers with FDL-Status.resp ( RTS signal! )
*/
static void VPC3_Start( void )
{
   #if DP_MSAC_C2
      MSAC_C2_OpenChannel();
   #endif /* #if DP_MSAC_C2 */

   /*-----------------------------------------------------------------------*/
   /* start vpc3                                                            */
   /*-----------------------------------------------------------------------*/
   VPC3_Start__();

   /* Fetch the first diagnosis buffer */
   pDpSystem->pDiagBuffer = VPC3_GetDiagBufPtr();
}

/*---------------------------------------------------------------------------*/
/* function: DpDiag_IsrDiagBufferChanged                                     */
/*---------------------------------------------------------------------------*/
/**
 * @brief The function VPC3_Isr() or VPC3_Poll() calls this function if the VPC3+ has
 * exchanged the diagnostic buffers, and made the old buffer available again to the user.
 */
static void DpDiag_IsrDiagBufferChanged( void )
{
   // diagnosis buffer has been changed
   VPC3_ClrDpState( eDpStateDiagActive );
   // Fetch new diagnosis buffer
   pDpSystem->pDiagBuffer = VPC3_GetDiagBufPtr();
}

/*---------------------------------------------------------------------------*/
/* function: DpPrm_ChkDpv1StatusBytes                                        */
/*---------------------------------------------------------------------------*/
/**
 * @brief Check DP-V1 status bytes.
 *
 * Some rules are defined in IEC61158-6 ( summery RulesCheckingPrmData.pdf - <a href="RulesCheckingPrmData.pdf">click here ).</a>
 *
 * @attention In DP-V0 mode all bytes are fix to 0x00!
 *
 * @param[in]bDpv1Status1    DP-V1 status byte 1
 * @param[in]bDpv1Status2    DP-V1 status byte 2
 * @param[in]bDpv1Status3    DP-V1 status byte 3
 * @return DP_OK - The check of DP-V1 status bytes is OK.
 * @return DP_PRM_DPV1_STATUS - The check of DP-V1 status bytes isn't OK. @see DP_ERROR_CODE
 */
static DP_ERROR_CODE DpPrm_ChkDpv1StatusBytes( uint8_t bDpv1Status1, uint8_t bDpv1Status2, uint8_t bDpv1Status3 )
{
   DP_ERROR_CODE eRetValue;

   eRetValue = DP_OK;

   //DPV0-Mode
   if(    (( bDpv1Status1 & 0xF8 ) != 0x00 )
       || (( bDpv1Status2 & 0xFF ) != 0x00 )
       || (( bDpv1Status3 & 0xFF ) != 0x00 )
     )
   {
      eRetValue = DP_PRM_DPV1_STATUS;
   }//if(    (( bDpv1Status1 & 0xF8 ) != 0x00 ) ...

   return eRetValue;
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_CheckDiagBufferChanged                                     */
/*---------------------------------------------------------------------------*/
static void VPC3_CheckDiagBufferChanged( void )
{
    uint8_t bVpc3Event;

    if ( VPC3_GetDpState( eDpStateDiagActive ) )
    {
        bVpc3Event = Vpc3Read( bVpc3RwIntReqReg_H );

        if ( bVpc3Event & VPC3_INT_DIAG_BUFFER_CHANGED )
        {
            DpDiag_IsrDiagBufferChanged();
            Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_DIAG_BUFFER_CHANGED );
        }
    }
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_CheckDiagBufPtr                                            */
/*---------------------------------------------------------------------------*/
static VPC3_UNSIGNED8_PTR VPC3_CheckDiagBufPtr( void )
{
   if( pDpSystem->pDiagBuffer == VPC3_NULL_PTR )
   {
      pDpSystem->pDiagBuffer = VPC3_GetDiagBufPtr();
   }
   return pDpSystem->pDiagBuffer;
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_UpdateDiagnosis                                            */
/*---------------------------------------------------------------------------*/
/*!
  \brief Set diagnostic data to VPC3+.
  By calling this function, the user transfers the new, external diagnostics to VPC3+.
  As a return value, the user receives a pointer to the new diagnostics data buffer.
  The user has to make sure that the buffer size does not exceed the size of the
  diagnostic buffer that was set when the slave�s memory resources were set up:

  Diagnostic Buffer Length >= bUserDiagLen + DIAG_NORM_DIAG_SIZE

  \param[in] bDiagControl - add diagnostic bits
  \param[in] pbToUserDiagData - pointer to structure with alarm/diagnostic data
  \param[in] bUserDiagLength - length of the current user diagnostic

  Values for bUserDiagLength:
  - 0 - A previously set user diagnostic is deleted from the slave�s diagnostic buffer. Only 6 bytes
        standard diagnostic are sent in the diagnostic telegram. In this case, the user does not have to transfer a
        pointer to a diagnostic buffer.
  - 1..DIAG_BUFSIZE-6 - Length of the new user diagnostic data.

  Values for bDiagControl:
  - DIAG_RESET - Reset bit 'extended diagnostic, static diagnostic, diagnostic overflow'
  - EXT_DIAG_SET - Set bit 'extended diagnostic'.
  - STAT_DIAG_SET - Set bit 'static diagnostic'.

  \retval !0 - pointer to current diagnostic buffer.
  \retval 0 - no buffer available
*/
static VPC3_UNSIGNED8_PTR VPC3_UpdateDiagnosis( uint8_t bDiagControl, MEM_UNSIGNED8_PTR pbToUserDiagData, uint8_t bUserDiagLen )
{
   VPC3_UNSIGNED8_PTR pDiagBuffer; /* pointer to diagnosis buffer */
   uint8_t            bNewDiagBufferCmd;
   uint8_t            bDiagBufferSm;

   bDiagBufferSm = VPC3_GET_DIAG_BUFFER_SM();

   if( ( bDiagBufferSm & 0x03 ) == 0x01 )                      /* locate Diag Buffer */
   {
      /* copy to diagnosis buffer */
      if( bUserDiagLen > 0 )
      {
         Vpc3_CopyToVpc3( pDpSystem->pDiagBuffer1+DIAG_NORM_DIAG_SIZE, pbToUserDiagData, bUserDiagLen );
      } /* if( bUserDiagLen > 0 ) */

      Vpc3Write( bVpc3RwLenDiagBuf1, bUserDiagLen+DIAG_NORM_DIAG_SIZE );
      Vpc3Write( (VPC3_ADR)pDpSystem->pDiagBuffer1, bDiagControl );
   } /* if( ( bDiagBufferSm & 0x03 ) == 0x01 ) */
   else
   {
      if( ( bDiagBufferSm & 0x0C ) == 0x04 )
      {
         /* copy to diagnosis buffer */
         if( bUserDiagLen > 0 )
         {
            Vpc3_CopyToVpc3( pDpSystem->pDiagBuffer2+DIAG_NORM_DIAG_SIZE, pbToUserDiagData, bUserDiagLen );
         } /* if( bUserDiagLen > 0 ) */

         Vpc3Write( bVpc3RwLenDiagBuf2, bUserDiagLen+DIAG_NORM_DIAG_SIZE );
         Vpc3Write( (VPC3_ADR)pDpSystem->pDiagBuffer2, bDiagControl );
      } /* if( ( bDiagBufferSm & 0x0C ) == 0x04 ) */
   } /* else of if( ( bDiagBufferSm & 0x03 ) == 0x01 ) */

   bNewDiagBufferCmd = VPC3_GET_NEW_DIAG_BUFFER_CMD();

   switch( bNewDiagBufferCmd & 0x03 )
   {
      case 1:   /* use buffer 1 */
      {
         pDiagBuffer = pDpSystem->pDiagBuffer1;
         break;
      } /* case 1: */

      case 2:   /* use buffer 2 */
      {
         pDiagBuffer = pDpSystem->pDiagBuffer2;
         break;
      } /* case 2: */

      default:
      {
         /* no buffer available */
         pDiagBuffer = VPC3_NULL_PTR;
         break;
      } /* default: */
   } /* switch( bNewDiagBufferCmd & 0x03 ) */

   return pDiagBuffer;
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_SetDiagnosis                                               */
/*---------------------------------------------------------------------------*/
/*!
  \brief Set diagnostic data to VPC3+.
  By calling this function, the user provides diagnostic data to the slave. The diagnostic
  data is sent at the next possible time.
  The user has to make sure that the buffer size does not exceed the size of the
  diagnostic buffer that was set when the slave�s memory resources were set up:

  Diagnostic Buffer Length >= LengthDiag_User + LengthAlarm_User

  \attention This function is not suitable for setting alarms.

  The user can set up DP diagnostics; the following applies:
   - The 6 byte standard diagnostic (refer to EN 50170) is not part of the user diagnostic.
   - In DP standard operation, one ID-related, several channel-related, and one
   - device-related diagnostics may be utilized.
   - In DPV1 operation, one revision, one ID-related, several channel-related, and
     one alarm.
   - The user is responsible for the content of the diagnostic data.

  \param[in] pbToUserDiagData - pointer to structure with alarm/diagnostic data
  \param[in] bUserDiagLength - length of the current user diagnostic
  \param[in] bDiagControl - add diagnostic bits
  \param[in] bCheckDiagFlag - check VPC3+ diagnostic flag

  Values for bUserDiagLength:
  - 0 - A previously set user diagnostic is deleted from the slave�s diagnostic buffer. Only 6 bytes
        standard diagnostic are sent in the diagnostic telegram. In this case, the user does not have to transfer a
        pointer to a diagnostic buffer.
  - 1..DIAG_BUFSIZE-6 - Length of the new user diagnostic data.

  Values for bDiagControl:
  - DIAG_RESET - Reset bit 'extended diagnostic, static diagnostic, diagnostic overflow'
  - EXT_DIAG_SET - Set bit 'extended diagnostic'.
  - STAT_DIAG_SET - Set bit 'static diagnostic'.

  \retval DP_OK - Execution OK, diagnostic message is copied into VPC3+
  \retval DP_DIAG_OLD_DIAG_NOT_SEND_ERROR - Error, wait because last diagnostic message isn't send
  \retval DP_DIAG_BUFFER_LENGTH_ERROR - Error, diagnostic message is too long
  \retval DP_DIAG_NO_BUFFER_ERROR - Error, no diagnostic buffer available
  \retval DP_DIAG_CONTROL_BYTE_ERROR - Error of bDiagControl
  \retval DP_DIAG_BUFFER_ERROR - Error, diagnostic header is wrong
  \retval DP_DIAG_SEQUENCE_ERROR - Error, revision will be send in wrong state
  \retval DP_DIAG_NOT_POSSIBLE_ERROR - Error, unknown diagnostic header
*/
static DP_ERROR_CODE VPC3_SetDiagnosis( MEM_UNSIGNED8_PTR pbToUserDiagData, uint8_t bUserDiagLength, uint8_t bDiagControl, uint8_t bCheckDiagFlag )
{
   MEM_UNSIGNED8_PTR pbToDiagArray;
   DP_ERROR_CODE     bRetValue;
   uint8_t           bTmpUserDiagnosisLength;
   uint8_t           bTmpLength;
   uint8_t           bHeader;
   uint8_t           bDpState;

   bRetValue = DP_OK;

   /* check available diag buffer */
   if( VPC3_CheckDiagBufPtr() != VPC3_NULL_PTR )
   {
      bTmpUserDiagnosisLength = bUserDiagLength;
      pbToDiagArray = pbToUserDiagData;

      bDpState = VPC3_GET_DP_STATE();
      if( ( bDpState == DATA_EX ) && ( bCheckDiagFlag == VPC3_TRUE ) )
      {
         if( VPC3_GET_DIAG_FLAG() )
         {
            /* old diagnosis not send */
            bRetValue = DP_DIAG_OLD_DIAG_NOT_SEND_ERROR;
         } /* if( VPC3_GET_DIAG_FLAG() ) */
      } /* if( ( bDpState == DATA_EX ) && ( bCheckDiagFlag == VPC3_TRUE ) ) */

      /* check bUserDiagLength */
      if( bUserDiagLength > ( DIAG_BUFSIZE - 6 ) )
      {
         bRetValue = DP_DIAG_BUFFER_LENGTH_ERROR;
      } /* if( bUserDiagLength > ( DIAG_BUFSIZE - 6 ) ) */

      if( bRetValue == DP_OK )
      {
         /* check control byte */
         switch( bDiagControl )
         {
            case EXT_DIAG_SET:
            {
               if( bUserDiagLength == 0 )
               {
                   bRetValue = DP_DIAG_CONTROL_BYTE_ERROR;
               } /* if( bUserDiagLength == 0 ) */
               break;
            } /* case EXT_DIAG_SET: */

            default:
            {
               /* do nothing */
               break;
            } /* default: */
         } /* switch( bDiagControl ) */

         /* check user diag buffer contents */
         while( ( 0 < bTmpUserDiagnosisLength ) && ( DP_OK == bRetValue ) )
         {
            bHeader = pbToDiagArray[0];
            switch( DIAG_TYPE_MASK & bHeader )
            {
               case DIAG_TYPE_DEV:
               {
                  bTmpLength = (( ~DIAG_TYPE_MASK ) & bHeader );
                  if( STATUS_DIAG_HEAD_SIZE > bTmpLength )
                  {
                     bRetValue = DP_DIAG_BUFFER_ERROR;
                  } /* if( STATUS_DIAG_HEAD_SIZE > bTmpLength ) */
                  break;
               } /* case DIAG_TYPE_DEV: */

               case DIAG_TYPE_KEN:
               {
                  bTmpLength = (( ~DIAG_TYPE_MASK ) & bHeader );
                  if ( bTmpLength == 0 )
                  {
                     bRetValue = DP_DIAG_BUFFER_ERROR;
                  } /* if ( bTmpLength == 0 ) */
                  break;
               } /* case DIAG_TYPE_KEN: */

               case DIAG_TYPE_CHN:
               {
                  bTmpLength = DIAG_TYPE_CHN_SIZE;
                  break;
               } /* case DIAG_TYPE_CHN: */

               case DIAG_TYPE_REV:
               {
                  bTmpLength = DIAG_TYPE_REV_SIZE;
                  if( bDpState != DATA_EX )
                  {
                     /* only allowed in state DATA_EX */
                     bRetValue = DP_DIAG_SEQUENCE_ERROR;
                  } /* if( bDpState != DATA_EX ) */
                  break;
               } /* case DIAG_TYPE_REV: */

               default:
               {
                  /* not possible! */
                  bTmpLength = 0;
                  bRetValue = DP_DIAG_NOT_POSSIBLE_ERROR;
                  break;
               } /* default: */
            } /* switch( DIAG_TYPE_MASK & bHeader ) */

            bTmpUserDiagnosisLength -= bTmpLength;
            pbToDiagArray += bTmpLength;
         } /* while( ( 0 < bTmpUserDiagnosisLength ) && ( DP_OK == bRetValue ) ) */

         if( bRetValue == DP_OK )
         {
            pDpSystem->pDiagBuffer = VPC3_UpdateDiagnosis( bDiagControl, pbToUserDiagData, bUserDiagLength );
         } /* if( bRetValue == DP_OK ) */
      } /* if( bRetValue == DP_OK ) */
   } /* if( VPC3_CheckDiagBufPtr() != VPC3_NULL_PTR ) */
   else
   {
      /* Fetch new diagnosis buffer */
      pDpSystem->pDiagBuffer = VPC3_GetDiagBufPtr();
      /* wait for next free diag_buffer */
      bRetValue = DP_DIAG_NO_BUFFER_ERROR;
   } /* else of if( VPC3_CheckDiagBufPtr() != VPC3_NULL_PTR ) */

   return bRetValue;
}

/*---------------------------------------------------------------------------*/
/* function: DpDiag_Alarm ( is also called from alarm state machine !!!! )   */
/*---------------------------------------------------------------------------*/
/**
 * @brief Reset static diagnostic bit.
 *
 * @param[in]bAlarmType
 * @param[in]bSeqNr
 * @param[in]psAlarm
 * @param[in]bCheckDiagFlag
 *
 * @return VPC3_TRUE - the static diagnostic is resettet
 * @return VPC3_FALSE - the static diagnostic is not resettet yet, try again
 */
static DP_ERROR_CODE DpDiag_Alarm( uint8_t bAlarmType, uint8_t bSeqNr, ALARM_STATUS_PDU_PTR psAlarm, uint8_t bCheckDiagFlag )
{
   MEM_UNSIGNED8_PTR pbToDiagArray;
   DP_ERROR_CODE     bRetValue;
   uint8_t           bExtDiagFlag;
   uint8_t           bDiagLength;
   uint16_t          wDiagEvent;

   bRetValue = DP_NOK;

   psAlarm = psAlarm; //avoid warning
   wDiagEvent = ( (uint16_t)(bAlarmType << 8) | ((uint16_t)bSeqNr) );

   VPC3_CheckDiagBufferChanged();

   //don't send diagnostic twice!
   if( wDiagEvent != pDpSystem->wOldDiag )
   {
      if( !( VPC3_GetDpState( eDpStateDiagActive ) ))
      {
      	memset( &pDpSystem->abUserDiagnostic[0], 0x00, sizeof( pDpSystem->abUserDiagnostic ) );
      	pbToDiagArray = pDpSystem->abUserDiagnostic;

      	bExtDiagFlag = 0x00;
      	bDiagLength = 0x00;

      	switch( bAlarmType )
      	{
      	   case USER_TYPE_CFG_OK:
      	   {
      	      bExtDiagFlag = STAT_DIAG_SET;
      	      break;
      	   }//case USER_TYPE_CFG_OK:

      	   case USER_TYPE_APPL_RDY:
      	   case USER_TYPE_RESET_DIAG:
      	   case USER_TYPE_PRM_NOK:
      	   case USER_TYPE_PRM_OK:
      	   default:
      	   {
      	      bExtDiagFlag = 0x00;
      	      bDiagLength = 0x00;
      	      break;
      	   }//default:
      	}//switch( bAlarmType )

      	VPC3_SetDpState( eDpStateDiagActive );

      	bRetValue = VPC3_SetDiagnosis( pDpSystem->abUserDiagnostic, bDiagLength, bExtDiagFlag, bCheckDiagFlag );

      	if( bRetValue == DP_OK )
      	{
      	   pDpSystem->wOldDiag = wDiagEvent;
      	}//if( bRetValue == DP_OK )
      	else
      	{
      	   VPC3_ClrDpState( eDpStateDiagActive );
      	}//else of if( bRetValue == DP_OK )
   	}//if( !( VPC3_GetDpState( eDpStateDiagActive ) ))
      else
      {
         bRetValue = DP_DIAG_ACTIVE_DIAG;
      }//else of if( !( VPC3_GetDpState( eDpStateDiagActive ) ))
   }//if( wDiagEvent != pDpSystem->wOldDiag )
   else
   {
      bRetValue = DP_DIAG_SAME_DIAG;
   }//else if( wDiagEvent != pDpSystem->wOldDiag )

   return bRetValue;
}

/*--------------------------------------------------------------------------*/
/* function: DpDiag_ResetStatDiag                                           */
/*--------------------------------------------------------------------------*/
/**
 * @brief Reset static diagnostic bit.
 *
 * @return VPC3_TRUE - the static diagnostic bit is resettet
 * @return VPC3_FALSE - the static diagnostic bit is not resettet yet, try again
 */
static uint8_t DpDiag_ResetStatDiag( void )
{
   if( DpDiag_Alarm( USER_TYPE_APPL_RDY, 0, (ALARM_STATUS_PDU_PTR) VPC3_NULL_PTR, VPC3_FALSE ) == DP_OK )
   {
      return VPC3_TRUE;
   }
   return VPC3_FALSE;
}

/*--------------------------------------------------------------------------*/
/* function: DpDiag_SetPrmOk                                                */
/*--------------------------------------------------------------------------*/
/**
 * @brief Set User specific diagnostic, if SetPrm is OK.
 * In this demo, the last user specific diagnostic will be cleared!
 *
 * @return DP_OK - diagnostic is set
 * @return DP_DIAG_SAME_DIAG - no change necessary
 * @return other - diagnostic is not resettet yet, try again
 */
static DP_ERROR_CODE DpDiag_SetPrmOk( DP_ERROR_CODE ePrmError )
{
   DP_ERROR_CODE bRetValue;
   DP_ERROR_CODE bDiagRetValue;
   uint8_t bLoop;

   bLoop = 0;
   bRetValue = DP_PRM_RETRY_ERROR;
   while( bLoop++ < MAX_DIAG_RETRY )
   {
      bDiagRetValue = DpDiag_Alarm( USER_TYPE_PRM_OK, 0, (ALARM_STATUS_PDU_PTR) VPC3_NULL_PTR, VPC3_FALSE );
      if( ( bDiagRetValue == DP_OK ) || ( bDiagRetValue == DP_DIAG_SAME_DIAG ) )
      {
         bRetValue = ePrmError;
         break;
      }
   }

   return bRetValue;
}

/*--------------------------------------------------------------------------*/
/* function: DpDiag_SetPrmNotOk                                             */
/*--------------------------------------------------------------------------*/
/**
 * @brief Set User specific diagnostic, if SetPrm is not OK.
 * In this demo, the last user specific diagnostic will be cleared!
 *
 * @return VPC3_TRUE - diagnostic is set
 * @return VPC3_FALSE - diagnostic is not resettet yet, try again
 */
static DP_ERROR_CODE DpDiag_SetPrmNotOk( DP_ERROR_CODE ePrmError )
{
   DP_ERROR_CODE bRetValue;
   uint8_t bLoop;

   bLoop = 0;
   bRetValue = DP_PRM_RETRY_ERROR;
   while( bLoop++ < MAX_DIAG_RETRY )
   {
      if( DpDiag_Alarm( USER_TYPE_PRM_NOK, 0, (ALARM_STATUS_PDU_PTR) VPC3_NULL_PTR, VPC3_FALSE ) == DP_OK )
      {
         bRetValue = ePrmError;
         break;
      }
   }

   return bRetValue;
}

/*---------------------------------------------------------------------------*/
/* function: DpPrm_ChkNewPrmData                                             */
/*---------------------------------------------------------------------------*/
/**
 * @brief Checking parameter data.
 * The user has to program the function for checking the received parameter data.
 *
 * @param[in] pbPrmData - address of parameter data
 * @param[in] bPrmLength - length of parameter data
 *
 * @return DP_OK - The transferred parameterization is OK.
 * @return DP_NOK - The transferred parameterization isn't OK.
 */
static DP_ERROR_CODE DpPrm_ChkNewPrmData( MEM_UNSIGNED8_PTR pbPrmData, uint8_t bPrmLength )
{
   MEM_STRUC_PRM_PTR psToPrmData;
   DP_ERROR_CODE     eRetValue;
   uint8_t           dpState;

   DpPrm_Init();

   eRetValue = DP_OK;

   if( bPrmLength == PRM_LEN_DPV1 )
   {
      psToPrmData = ( MEM_STRUC_PRM_PTR )pbPrmData;
      eRetValue = DpPrm_ChkDpv1StatusBytes( psToPrmData->bDpv1Status1, psToPrmData->bDpv1Status2, psToPrmData->bDpv1Status3 );
   }
   else
   {
      eRetValue = DP_PRM_LEN_ERROR;
   }

   dpState = VPC3_GET_DP_STATE();
   if( ( dpState == DATA_EX ) && ( eRetValue == DP_OK ) )
   {
      //don't send diagnostic here
   }
   else
   {
      eRetValue = ( eRetValue == DP_OK ) ? DpDiag_SetPrmOk( eRetValue ) : DpDiag_SetPrmNotOk( eRetValue );
   }

   return eRetValue;
}

/*--------------------------------------------------------------------------*/
/* function: DpDiag_SetCfgOk                                                */
/*--------------------------------------------------------------------------*/
/**
 * @brief Reset static diagnostic bit.
 *
 * @return VPC3_TRUE - the static diagnostic is resettet
 * @return VPC3_FALSE - the static diagnostic is not resettet yet, try again
 */
static E_DP_CFG_ERROR DpDiag_SetCfgOk( E_DP_CFG_ERROR eCfgError )
{
   E_DP_CFG_ERROR eRetValue;
   uint8_t bLoop;

   bLoop = 0;
   eRetValue = DP_CFG_FAULT;
   while( bLoop++ < MAX_DIAG_RETRY )
   {
      if( DpDiag_Alarm( USER_TYPE_CFG_OK, 0x22, (ALARM_STATUS_PDU_PTR) VPC3_NULL_PTR, VPC3_FALSE ) == DP_OK )
      {
         eRetValue = eCfgError;
         break;
      }
   }

   return eRetValue;
}

/*---------------------------------------------------------------------------*/
/* function: DpCfg_ChkNewCfgData                                             */
/*---------------------------------------------------------------------------*/
/**
 * @brief Checking configuration data.
 * The function VPC3_Isr() or VPC3_Poll() calls this function if the VPC3+
 * has received a Check_Cfg message and has made the data available in the Cfg buffer.
 *
 * The user has to program the function for checking the received configuration data.
 *
 * @param[in] pbCfgData - address of check configuration data
 * @param[in] bCfgLength - length of configuration data
 *
 * @return see E_DP_CFG_ERROR @see E_DP_CFG_ERROR
 */
static E_DP_CFG_ERROR DpCfg_ChkNewCfgData( MEM_UNSIGNED8_PTR pbCfgData, uint8_t bCfgLength )
{
   E_DP_CFG_ERROR eRetValue;
   uint8_t        i;

   eRetValue = DP_CFG_OK;

   if( bCfgLength == sDpAppl.sCfgData.bLength )
   {
      for( i = 0; i < bCfgLength; i++ )
      {
         if( sDpAppl.sCfgData.abData[ i ] != *pbCfgData )
         {
            eRetValue = DP_CFG_FAULT;
         }//if( sDpAppl.sCfgData.abData[ i ] != *pbCfgData )

         pbCfgData++;
      }//for( i = 0; i < bCfgLength; i++ )
   }//if( bCfgLength == sDpAppl.sCfgData.bLength )
   else
   {
      eRetValue = DP_CFG_FAULT;
   }//else of if( bCfgLength == sDpAppl.sCfgData.bLength )

   if( ( eRetValue == DP_CFG_OK ) || ( eRetValue == DP_CFG_UPDATE ) )
   {
      eRetValue = DpDiag_SetCfgOk( eRetValue );
      if( eRetValue != DP_CFG_FAULT )
      {
         VPC3_SetDpState( eDpStateCfgOkStatDiag );
      }
   }//if( ( eRetValue == DP_CFG_OK ) || ( eRetValue == DP_CFG_UPDATE ) )

   DpDiag_AlarmInit();

   return eRetValue;
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_SetIoDataLength                                            */
/*---------------------------------------------------------------------------*/
/*!
  \brief Set length of VPC3+ input buffer and output buffer.

*/
static void VPC3_SetIoDataLength( void )
{
    /* length of buffers OK, set real buffers */
    Vpc3Write( bVpc3RwLenDoutBuf, pDpSystem->bOutputDataLength );
    Vpc3Write( bVpc3RwLenDinBuf, pDpSystem->bInputDataLength );
}

/*--------------------------------------------------------------------------*/
/* function: DpAppl_SetApplEvent                                            */
/*--------------------------------------------------------------------------*/
/*!
  \brief Set PROFIBUS event.

  \param[in] eDpApplEv PROFIBUS event @see eDpApplEv_Flags
*/
static void DpAppl_SetApplEvent( eDpApplEv_Flags eDpApplEv )
{
    sDpAppl.eDpApplEvent |= eDpApplEv;
}

/*---------------------------------------------------------------------------*/
/* function: DpAppl_IsrNewWdDpTimeout                                        */
/*---------------------------------------------------------------------------*/
/*!
   \brief The function VPC3_Isr() or VPC3_Poll() calls this function if the
   watchdog timer expired in the WD mode DP_Control.
   The communication between master and slave is time controlled, every time you're
   disconnecting the PROFIBUS master or you're disconnecting the PROFIBUS cable you'll
   get this event.
*/
static void DpAppl_IsrNewWdDpTimeout( void )
{
    //not used in our application
}

/*---------------------------------------------------------------------------*/
/* function: DpAppl_IsrGoLeaveDataExchange                                   */
/*---------------------------------------------------------------------------*/
/*!
   \brief The function VPC3_Isr() or VPC3_Poll() calls this function if the
   DP-Statemachine has entered the DataEx-mode or has exited it.
   With the function VPC3_GET_DP_STATE() you can find out the state of VPC3+.
   \param[in] bDpState - state of PROFIBUS connection (WAIT_PRM,WAIT_CFG,DATA_EX)
*/
static void DpAppl_IsrGoLeaveDataExchange( uint8_t bDpState )
{
   if( bDpState != DATA_EX )
   {
      VPC3_ClrDpState( eDpStateApplReady | eDpStateRun );
      VPC3_SetDpState( eDpStateInit );
   }//if( bDpState != DATA_EX )
}

/*---------------------------------------------------------------------------*/
/* function: DpAppl_IsrDxOut                                                 */
/*---------------------------------------------------------------------------*/
/*!
   \brief The function VPC3_Isr() or VPC3_Poll() calls this function if the VPC3+
   has received a DataExchange message and has made the new output data
   available in the N-buffer. In the case of Power_On or Leave_Master, the
   VPC3+ clears the content of the buffer, and generates this event also.
*/
static void DpAppl_IsrDxOut( void )
{
    DpAppl_SetApplEvent( eDpApplEv_IoOut );
}

/*---------------------------------------------------------------------------*/
/* function: DpAppl_IsrNewGlobalControlCommand                               */
/*---------------------------------------------------------------------------*/
/*!
   \brief The function VPC3_Isr() or VPC3_Poll() calls this function if the VPC3+
   has received a Global_Control message. The GC_Command_Byte can be read out
   with the macro VPC3_GET_GC_COMMAND().
   \param[in] bGcCommand - global control command @see VPC3_GET_GC_COMMAND()
*/
static void DpAppl_IsrNewGlobalControlCommand( uint8_t bGcCommand )
{
   //not used in our application
   //bGcCommand = bGcCommand;   //avoid compiler warning
}

/*---------------------------------------------------------------------------*/
/* function: DpAppl_IsrNewSetSlaveAddress                                    */
/*---------------------------------------------------------------------------*/
/*!
   \brief The function VPC3_Isr() or VPC3_Poll() calls this function if the VPC3+
   has received a Set_Slave_Address message and made the data available in the SSA
   buffer.
  \param[in] psSsa - pointer to set slave address structure
*/
static void DpAppl_IsrNewSetSlaveAddress( MEM_STRUC_SSA_BLOCK_PTR psSsa )
{
    //not used in our application
    //psSsa = psSsa; //avoid compiler warning
/*
   //store the new address and the bit bNoAddressChanged for the next startup
   print_string("\r\nNewAddr: ");
   print_hexbyte(psSsa->bTsAddr);
   print_hexbyte(psSsa->bNoAddressChanged);
   print_hexbyte(psSsa->bIdentHigh);
   print_hexbyte(psSsa->bIdentLow);
*/
}

/*---------------------------------------------------------------------------*/
/* function: DpAppl_IsrBaudrateDetect                                        */
/*---------------------------------------------------------------------------*/
/*!
   \brief The function VPC3_Isr() or VPC3_Poll() calls this function if the VPC3+
   has exited the Baud_Search mode and has found a baudrate.
   With the macro VPC3_GET_BAUDRATE() you can detect the baudrate.
*/
static void DpAppl_IsrBaudrateDetect( void )
{
   //not used in our application
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_GetDinBufPtr                                               */
/*---------------------------------------------------------------------------*/
/*!
  \brief Fetch buffer pointer of the next input buffer.

  \retval !0 - pointer to current input buffer.
  \retval 0 - no buffer available
*/
static VPC3_UNSIGNED8_PTR VPC3_GetDinBufPtr( void )
{
   VPC3_UNSIGNED8_PTR pToInputBuffer;     /* pointer to input buffer ( VPC3 -> DP-Master ) */

   switch( VPC3_GET_DIN_BUFFER_SM() )  /* locate user Din Buffer */
   {
      case 0x10:
      {
         pToInputBuffer = pDpSystem->pDinBuffer1;
         break;
      } /* case 0x10: */

      case 0x20:
      {
         pToInputBuffer = pDpSystem->pDinBuffer2;
         break;
      } /* case 0x20: */

      case 0x30:
      {
         pToInputBuffer = pDpSystem->pDinBuffer3;
         break;
      } /* case 0x30: */

      default:
      {
         pToInputBuffer = VPC3_NULL_PTR;
         break;
      } /* default: */
   } /* switch( VPC3_GET_DIN_BUFFER_SM() ) */

   return pToInputBuffer;
}

/*--------------------------------------------------------------------------*/
/* function: VPC3_InputDataUpdate                                           */
/*--------------------------------------------------------------------------*/
/*!
  \brief Copy input data to VPC3+ and perform buffer exchange.

  \param[in] pToInputData - pointer to local input data
*/
static void VPC3_InputDataUpdate( MEM_UNSIGNED8_PTR pToInputData )
{
    VPC3_UNSIGNED8_PTR   pToInputBuffer;
    volatile uint8_t     bResult;

    /* write DIN data to VPC3 */
    pToInputBuffer = VPC3_GetDinBufPtr();
    if( pToInputBuffer != VPC3_NULL_PTR )
    {
        Vpc3_CopyToVpc3( pToInputBuffer, pToInputData, (uint16_t)pDpSystem->bInputDataLength );

        /* the user makes a new Din-Buffer available in the state N */
        bResult = VPC3_INPUT_UPDATE();
    }
}

/*---------------------------------------------------------------------------*/
/* function: VPC3_GetDoutBufPtr                                              */
/*---------------------------------------------------------------------------*/
/*!
  \brief Fetch buffer pointer of the current output buffer.

  \param[out] pbState

  \retval !0 - pointer to current output buffer.
  \retval 0 - no buffer available
*/
static VPC3_UNSIGNED8_PTR VPC3_GetDoutBufPtr( MEM_UNSIGNED8_PTR pbState )
{
   VPC3_UNSIGNED8_PTR pToOutputBuffer;             /* pointer to output buffer ( DP-Master -> VPC3+ ) */

   *pbState = VPC3_GET_NEXT_DOUT_BUFFER_CMD();

   switch ( VPC3_GET_DOUT_BUFFER_SM() )          /* locate user Dout Buffer */
   {
      case 0x10:
      {
         pToOutputBuffer = pDpSystem->pDoutBuffer1;
         break;
      } /* case 0x10: */

      case 0x20:
      {
         pToOutputBuffer = pDpSystem->pDoutBuffer2;
         break;
      } /* case 0x20: */

      case 0x30:
      {
         pToOutputBuffer = pDpSystem->pDoutBuffer3;
         break;
      } /* case 0x30: */

      default:
      {
         pToOutputBuffer = VPC3_NULL_PTR;
         break;
      } /* default: */
   } /* switch( VPC3_GET_DOUT_BUFFER_SM() ) */

   return pToOutputBuffer;
}

/*--------------------------------------------------------------------------*/
/* function: DpAppl_ReadInputData                                           */
/*--------------------------------------------------------------------------*/
/*!
  \brief Handling of the PROFIBUS input data ( slave --> master ).
*/
static void DpAppl_ReadInputData( void )
{
    BOOL result;

    /** @todo Read cyclically the input module. */
    //LOG_WRITELN("ReadInputData");

    result = InsertSlaveInputDataIntoBuffer(&sDpAppl.abDpInputData[0]);

    VPC3_InputDataUpdate( &sDpAppl.abDpInputData[0] );
}

/*--------------------------------------------------------------------------*/
/* function: DpAppl_TestApplEvent                                           */
/*--------------------------------------------------------------------------*/
/*!
  \brief Check if the internal PROFIBUS event is set and clear the event.

  \param[in] eDpApplEv PROFIBUS event @see eDpApplEv_Flags

  \retval VPC3_TRUE Event was set
  \retval VPC3_FALSE Event was not set.
*/
static uint8_t DpAppl_TestApplEvent( eDpApplEv_Flags eDpApplEv )
{
   if( sDpAppl.eDpApplEvent & eDpApplEv )
   {
      sDpAppl.eDpApplEvent &= ~eDpApplEv;
      return VPC3_TRUE;
   }

   return VPC3_FALSE;
}

/*--------------------------------------------------------------------------*/
/* function: DpAppl_CheckEvIoOut                                            */
/*--------------------------------------------------------------------------*/
/*!
  \brief Handling of the PROFIBUS output data ( master to slave ).

  The VPC3+ has received a DataExchange message and has made the new output data
  available in the N-buffer. In the case of Power_On or Leave_Master, the
  VPC3+ clears the content of the buffer, and generates this event also.
*/
static void DpAppl_CheckEvIoOut( void )
{
   VPC3_UNSIGNED8_PTR  pToOutputBuffer;   /**< Pointer to output buffer. */
   uint8_t             bOutputState;      /**< State of output data. */

   if ( DpAppl_TestApplEvent( eDpApplEv_IoOut ) )
   {
      pToOutputBuffer = VPC3_GetDoutBufPtr( &bOutputState );
      if ( pToOutputBuffer )
      {
         Vpc3_CopyFromVpc3( &sDpAppl.abDpOutputData[0], pToOutputBuffer, pDpSystem->bOutputDataLength );

         /** @todo Write the output data to the output modules. */
      }
   }
}

/*--------------------------------------------------------------------------*/
/* function: DpAppl_ApplicationReady                                        */
/*--------------------------------------------------------------------------*/
/*!
  \brief ApplicatioReady

  This function is called after the PROFIBUS configuration data has been acknowledged
  positive by the user. The slave is now in DataExchange but the static diagnostic bit is set.
  The user can do here own additional initialization and should read here the input data. The
  slave delete now the static diagnostic bit and the master will send DataExchange telegrams.
*/
static void DpAppl_ApplicationReady( void )
{
   #if DPV1_IM_SUPP
      DpIm_ClearImIndex( 0x03 );
   #endif//#if DPV1_IM_SUPP

   /** @todo Make here your own initialization. */

   //read input data
   DpAppl_ReadInputData();

   //reset Diag.Stat
   if ( DpDiag_ResetStatDiag() )
   {
      VPC3_ClrDpState( eDpStateCfgOkStatDiag );
      VPC3_SetDpState( eDpStateApplReady );
   }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void ProfibusSlave_Init(void)
{
    taskState = PROFIBUS_TS_SetResetVPC3;
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void ProfibusSlave_Task(void)
{
    static VPC3_ADR       wAddress;
    static DP_ERROR_CODE  bError;
    static uint16_t       wTemp;
    static uint16_t       j;
    static uint16_t       i;
    static uint32_t       timer;
    static MEM_UNSIGNED8_PTR pLocalMemory;
    static uint8_t        bLength;
    static uint8_t        bResult;

    switch (taskState)
    {
    case PROFIBUS_TS_SetResetVPC3:
        {
            //LOG_WRITELN("SetResetVPC3");

            // init application data
            memset( &sDpAppl, 0, sizeof( sDpAppl ) );

            // initialize VPC3
            pVpc3 = &sVpc3OnlyForInit;
            pDpSystem = &sDpSystemChannel1;

            memset( pVpc3, 0, sizeof( VPC3_STRUC ) );

            // initialize global system structure
            memset( pDpSystem, 0, sizeof( VPC3_SYSTEM_STRUC ));
            pDpSystem->eDpState = eDpStateInit;

            DpPrm_Init();
            DpCfg_Init();
            DpDiag_Init();

            Vpc3_SetResetVPC3Channel1();

            timer = SysTick_GetSysMilliSeconds();
            taskState = PROFIBUS_TS_SetResetVPC3Delay;
        }
        break;

    case PROFIBUS_TS_SetResetVPC3Delay:
        {
            if (SysTick_IsSysMilliSecondTimeout(timer, 10))
            {
                Vpc3_ClrResetVPC3Channel1();

                timer = SysTick_GetSysMilliSeconds();
                taskState = PROFIBUS_TS_ClrResetVPC3Delay;
            }
        }
        break;

    case PROFIBUS_TS_ClrResetVPC3Delay:
        {
            if (SysTick_IsSysMilliSecondTimeout(timer, 10))
            {
                taskState = PROFIBUS_TS_MemoryTest;
            }
        }
        break;

    /*
        Check VPC3 memory.
        Checks the memory of VPC3+. The starting address is 16hex and
        the end address depends on DP_VPC3_4KB_MODE.

        errors:
        DP_VPC3_ERROR - Memory Error
    */
    case PROFIBUS_TS_MemoryTest:
        {
            /* neccessary, if 4Kbyte mode enabled */
            VPC3_SET_MODE_REG_2( INIT_VPC3_MODE_REG_2 );

            // test and clear vpc3 ram
            j = 0;
            i = 0x16;
            wAddress = bVpc3RwTsAddr;

            taskState = PROFIBUS_TS_MemoryTestWrite;
        }
        break;

    case PROFIBUS_TS_MemoryTestWrite:
        {
            if (i < ASIC_RAM_LENGTH)
            {
                Vpc3Write( wAddress++, (uint8_t)( j >> 8 ) );
                Vpc3Write( wAddress++, (uint8_t)( j      ) );

                i += 2;
                j++;
            }
            else
            {
                j = 0;
                i = 0x16;
                wAddress = bVpc3RwTsAddr;

                taskState = PROFIBUS_TS_MemoryTestRead;
            }
        }
        break;

    case PROFIBUS_TS_MemoryTestRead:
        {
            if (i < ASIC_RAM_LENGTH)
            {
                wTemp = (((uint16_t)Vpc3Read( wAddress++ )) << 8 );
                wTemp +=  (uint16_t)Vpc3Read( wAddress++ );

                if ( wTemp != j )
                {
                    //LOG_WRITELN("MemoryTestReadErr");
                    sVpc3Error.bErrorCode = DP_VPC3_ERROR;
                    taskState = PROFIBUS_TS_SetResetVPC3;
                    break;
                }

                i += 2;
                j++;
            }
            else
            {
                //LOG_WRITELN("MemoryTestReadOk");
                taskState = PROFIBUS_TS_InitVPC3;
            }
        }
        break;

    /*
        Initializes VPC3+.

        errors:
        DP_NOT_OFFLINE_ERROR - Error VPC3 is not in OFFLINE state
        DP_ADDRESS_ERROR - Error, DP Slave address
        DP_CALCULATE_IO_ERROR - Error with configuration bytes
        DP_DOUT_LEN_ERROR - Error with Dout length
        DP_DIN_LEN_ERROR - Error with Din length
        DP_DIAG_LEN_ERROR - Error with diagnostics length
        DP_PRM_LEN_ERROR - Error with parameter assignment data length
        DP_SSA_LEN_ERROR - Error with address data length
        DP_CFG_LEN_ERROR - Error with configuration data length
        DP_LESS_MEM_ERROR - Error Overall, too much memory used
        DP_LESS_MEM_FDL_ERROR - Error Overall, too much memory used
    */
    case PROFIBUS_TS_InitVPC3:
        {
            // check VPC3 is in OFFLINE
            if (!VPC3_GET_OFF_PASS())
            {
                /* neccessary, if 4Kbyte mode enabled */
                VPC3_SET_MODE_REG_2( INIT_VPC3_MODE_REG_2 );

                /* clear VPC3 */
                i = 0;
                wAddress = bVpc3RwTsAddr;

                taskState = PROFIBUS_TS_InitVPC3Clear;
            }
            else
            {
                //LOG_WRITELN("InitVPC3Err");
                sVpc3Error.bErrorCode = DP_NOT_OFFLINE_ERROR;
                taskState = PROFIBUS_TS_SetResetVPC3;
            }
        }
        break;

    case PROFIBUS_TS_InitVPC3Clear:
        {
            if (i < (ASIC_RAM_LENGTH-0x16))
            {
                Vpc3Write( wAddress++, 0 );
                i++;
            }
            else
            {
                #if DP_INTERRUPT_MASK_8BIT == 0
                    pDpSystem->wPollInterruptMask = SM_INTERRUPT_MASK;
                #endif /* #if DP_INTERRUPT_MASK_8BIT == 0 */

                // set constant values
                bError = VPC3_SetConstants( Setting_GetProfibusSlaveAddress() /*DP_ADDR*/, IDENT_NR );

                if ( DP_OK == bError )
                {
                    // calculate length of input and output data using cfg-data
                    psCFG psCfgData = (psCFG)&sDpAppl.sCfgData;
                    bError = VPC3_CalculateInpOutpLength( &psCfgData->abData[0], psCfgData->bLength );

                    if ( DP_OK == bError )
                    {
                        // initialize buffer structure
                        bError = VPC3_InitBufferStructure();

                        if ( DP_OK == bError )
                        {
                            // initialize subscriber
                            #if DP_SUBSCRIBER
                                bError = VPC3_InitSubscriber();
                            #endif /* #if DP_SUBSCRIBER */

                            // initialize fdl_interface
                            #if DP_FDL
                                sFdl_Init sFdlInit;

                                if( DP_OK == bError )
                                {
                                    memset( &sFdlInit, 0, sizeof( sFdl_Init ) );
                                    #if DP_MSAC_C1
                                        sFdlInit.eC1SapSupported |= C1_SAP_51;
                                        #if DP_ALARM_OVER_SAP50
                                            sFdlInit.eC1SapSupported |= C1_SAP_50;
                                        #endif /* #if DP_ALARM_OVER_SAP50 */
                                    #endif /* #if DP_MSAC_C1 */

                                    #if DP_MSAC_C2
                                        sFdlInit.bC2Enable = VPC3_TRUE;
                                        sFdlInit.bC2_NumberOfSaps = C2_NUM_SAPS;
                                    #endif /* #if DP_MSAC_C2 */

                                    bError = FDL_InitAcyclicServices( bSlaveAddress, sFdlInit );
                                } /* if( DP_OK == bError ) */
                            #endif /* #if DP_FDL */

                            if ( DP_OK == bError )
                            {
                                i = 0;
                                wAddress = 0x16;
                                pLocalMemory = (MEM_UNSIGNED8_PTR)&pVpc3->bTsAddr;

                                taskState = PROFIBUS_TS_InitVPC3Copy;
                            }
                            else
                            {
                                //LOG_WRITELN("InitVPC3ClrErr1");
                                sVpc3Error.bErrorCode = bError;
                                taskState = PROFIBUS_TS_SetResetVPC3;
                            }
                        }
                        else
                        {
                            //LOG_WRITELN("InitVPC3ClrErr2");
                            sVpc3Error.bErrorCode = bError;
                            taskState = PROFIBUS_TS_SetResetVPC3;
                        }
                    }
                    else
                    {
                        //LOG_WRITELN("InitVPC3ClrErr3");
                        sVpc3Error.bErrorCode = bError;
                        taskState = PROFIBUS_TS_SetResetVPC3;
                    }
                }
                else
                {
                    //LOG_WRITELN("InitVPC3ClrErr4");
                    sVpc3Error.bErrorCode = bError;
                    taskState = PROFIBUS_TS_SetResetVPC3;
                }
            }
        }
        break;

    case PROFIBUS_TS_InitVPC3Copy:
        {
            if (i < 0x2A)
            {
                Vpc3Write( wAddress++, *pLocalMemory );
                i++;
                pLocalMemory++;
            }
            else
            {
                // set real configuration data
                psCFG psCfgData = (psCFG)&sDpAppl.sCfgData;
                VPC3_SET_READ_CFG_LEN( psCfgData->bLength );      /* set configuration length */

                i = 0;
                wAddress = (VPC3_ADR)VPC3_GET_READ_CFG_BUF_PTR();
                pLocalMemory = (MEM_UNSIGNED8_PTR)&psCfgData->abData[0];

                taskState = PROFIBUS_TS_InitVPC3CopyCfg;
            }
        }
        break;

    case PROFIBUS_TS_InitVPC3CopyCfg:
        {
            psCFG psCfgData = (psCFG)&sDpAppl.sCfgData;
            if (i < psCfgData->bLength)
            {
                Vpc3Write( wAddress++, *pLocalMemory );
                i++;
                pLocalMemory++;
            }
            else
            {
                Vpc3_EnableInterruptVPC3Channel1();

                //todo: before startup the VPC3+, make here your own initializations

                VPC3_Start();

                taskState = PROFIBUS_TS_Main;
                //LOG_WRITELN("VPC3 Start");
            }
        }
        break;

    /*
        Call this task state cyclically so that the PROFIBUS DP slave services can be processed.
    */
    case PROFIBUS_TS_Main:
        {
            // trigger watchdogs
            // toggle user watchdog
            VPC3_RESET_USER_WD();   // toggle user watchdog
            taskState = PROFIBUS_TS_Poll;
        }
        break;

    /*
        Poll PROFIBUS events.
    */
    case PROFIBUS_TS_Poll:
        {
            pDpSystem->wPollInterruptEvent = MakeWord( Vpc3Read( bVpc3RwIntReqReg_H ), Vpc3Read( bVpc3RwIntReqReg_L ) );
            pDpSystem->wPollInterruptEvent &= pDpSystem->wPollInterruptMask;

            if ( pDpSystem->wPollInterruptEvent > 0 )
            {
                #if( DP_TIMESTAMP == 0 )
                    // IND_MAC_RESET
                    if ( VPC3_POLL_IND_MAC_RESET() )
                    {
                        VPC3_CON_IND_MAC_RESET();
                    }
                #endif /* #if( DP_TIMESTAMP == 0 ) */

                // IND_DIAG_BUF_CHANGED
                if ( VPC3_POLL_IND_DIAG_BUFFER_CHANGED() )
                {
                    DpDiag_IsrDiagBufferChanged();
                    VPC3_POLL_CON_IND_DIAG_BUFFER_CHANGED();
                }

                // IND_NEW_PRM_DATA
                if ( VPC3_POLL_IND_NEW_PRM_DATA() )
                {
                    #if DP_INTERRUPT_MASK_8BIT == 0
                        VPC3_POLL_CON_IND_NEW_PRM_DATA();
                    #endif /* #if DP_INTERRUPT_MASK_8BIT == 0 */

                    bResult = VPC3_PRM_FINISHED;
                    taskState = PROFIBUS_TS_PollPrm;
                }
                else
                {
                    taskState = PROFIBUS_TS_PollCfg;
                }
            }
            else
            {
                taskState = PROFIBUS_TS_State;
            }
        }
        break;

    case PROFIBUS_TS_PollPrm:
        {
            i = 0;
            bLength = VPC3_GET_PRM_LEN();
            wAddress = (VPC3_ADR)VPC3_GET_PRM_BUF_PTR();
            pLocalMemory = (MEM_UNSIGNED8_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0];

            taskState = PROFIBUS_TS_PollPrmCopy;
        }
        break;

    case PROFIBUS_TS_PollPrmCopy:
        {
            if (i < bLength)
            {
                *pLocalMemory = Vpc3Read( wAddress++ );
                i++;
                pLocalMemory++;
            }
            else
            {
                if ( DpPrm_ChkNewPrmData( (MEM_UNSIGNED8_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0], bLength ) == DP_OK )
                {
                    //LOG_WRITELN("SetPrmDataOk");
                    #if REDUNDANCY
                        #if DP_MSAC_C1
                            if( VPC3_GET_PRM_LEN() != PRM_CMD_LENGTH )
                            {
                                MSAC_C1_CheckIndNewPrmData( (MEM_STRUC_PRM_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0], bPrmLength );
                            } /* if( VPC3_GET_PRM_LEN() != PRM_CMD_LENGTH ) */
                        #endif /* #if DP_MSAC_C1 */
                    #else
                        #if DP_MSAC_C1
                            MSAC_C1_CheckIndNewPrmData( (MEM_STRUC_PRM_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0], bPrmLength );
                        #endif /* #if DP_MSAC_C1 */
                    #endif /* #if REDUNDANCY */

                    bResult = VPC3_SET_PRM_DATA_OK();
                }
                else
                {
                    //LOG_WRITELN("SetPrmDataNotOk");
                    bResult = VPC3_SET_PRM_DATA_NOT_OK();
                }

                if ( bResult == VPC3_PRM_CONFLICT )
                    taskState = PROFIBUS_TS_PollPrm;
                else
                    taskState = PROFIBUS_TS_PollCfg;
            }
        }
        break;

    case PROFIBUS_TS_PollCfg:
        {
            // check config data , application specific!
            if ( VPC3_POLL_IND_NEW_CFG_DATA() )
            {
                #if DP_INTERRUPT_MASK_8BIT == 0
                    VPC3_POLL_CON_IND_NEW_CFG_DATA();
                #endif /* #if DP_INTERRUPT_MASK_8BIT == 0 */

                bResult = VPC3_CFG_FINISHED;
                taskState = PROFIBUS_TS_PollCfgLoop;
            }
            else
            {
                taskState = PROFIBUS_TS_PollTimeout;
            }
        }
        break;

    case PROFIBUS_TS_PollCfgLoop:
        {
            i = 0;
            bLength = VPC3_GET_CFG_LEN();
            wAddress = (VPC3_ADR)VPC3_GET_CFG_BUF_PTR();
            pLocalMemory = (MEM_UNSIGNED8_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0];

            taskState = PROFIBUS_TS_PollCfgLoopCopy;
        }
        break;

    case PROFIBUS_TS_PollCfgLoopCopy:
        {
            if (i < bLength)
            {
                *pLocalMemory = Vpc3Read( wAddress++ );
                i++;
                pLocalMemory++;
            }
            else
            {
               switch ( DpCfg_ChkNewCfgData( (MEM_UNSIGNED8_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0], bLength ) )
               {
                  case DP_CFG_OK:
                  {
                     #if DP_MSAC_C1
                        MSAC_C1_DoCfgOk();
                     #endif /* #if DP_MSAC_C1 */

                     bResult = VPC3_SET_CFG_DATA_OK();
                     break;
                  } /* case DP_CFG_OK: */

                  case DP_CFG_FAULT:
                  {
                     #if DP_MSAC_C1
                        MSAC_C1_DoCfgNotOk();
                     #endif /* #if DP_MSAC_C1 */

                     bResult = VPC3_SET_CFG_DATA_NOT_OK();
                     break;
                  } /* case DP_CFG_FAULT: */

                  case DP_CFG_UPDATE:
                  {
                     /* Calculate the length of the input and output using the configuration bytes */
                     if( DP_OK != VPC3_CalculateInpOutpLength( (MEM_UNSIGNED8_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0], bLength ) )
                     {
                        #if DP_MSAC_C1
                           MSAC_C1_DoCfgNotOk();
                        #endif /* #if DP_MSAC_C1 */

                        bResult = VPC3_SET_CFG_DATA_NOT_OK();
                     } /* if( DP_OK != VPC3_CalculateInpOutpLength( (MEM_UNSIGNED8_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0], bCfgLength ) ) */
                     else
                     {
                        /* set IO-Length */
                        VPC3_SetIoDataLength();

                        #if DP_MSAC_C1
                           MSAC_C1_DoCfgOk();
                        #endif /* #if DP_MSAC_C1 */

                        VPC3_SET_READ_CFG_LEN( bLength );
                        VPC3_UPDATE_CFG_BUFFER();

                        bResult = VPC3_SET_CFG_DATA_OK();
                     } /* else of if( DP_OK != VPC3_CalculateInpOutpLength( (MEM_UNSIGNED8_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0], bCfgLength ) ) */
                     break;
                  } /* case DP_CFG_UPDATE: */

                  default:
                  {
                     break;
                  } /* default: */
               }

               if ( bResult == VPC3_CFG_CONFLICT )
               {
                    taskState = PROFIBUS_TS_PollCfgLoop;
               }
               else
               {
                    taskState = PROFIBUS_TS_PollTimeout;
               }
            }
        }
        break;

    case PROFIBUS_TS_PollTimeout:
        {
            // IND_WD_DP_TIMEOUT
            if ( VPC3_POLL_IND_WD_DP_MODE_TIMEOUT() )
            {
                DpAppl_IsrNewWdDpTimeout();
                VPC3_CON_IND_WD_DP_MODE_TIMEOUT();
            }

            taskState = PROFIBUS_TS_PollGoLeaveDataExchange;
        }
        break;

    case PROFIBUS_TS_PollGoLeaveDataExchange:
        {
            // IND_GO_LEAVE_DATA_EX
            if ( VPC3_POLL_IND_GO_LEAVE_DATA_EX() )
            {
                #if DP_MSAC_C1
                    MSAC_C1_LeaveDx();
                #endif /* #if DP_MSAC_C1 */

                DpAppl_IsrGoLeaveDataExchange( VPC3_GET_DP_STATE() );
                VPC3_CON_IND_GO_LEAVE_DATA_EX();
            }

            taskState = PROFIBUS_TS_PollDxOut;
        }
        break;

    case PROFIBUS_TS_PollDxOut:
        {
            // IND_DX_OUT
            if ( VPC3_POLL_IND_DX_OUT() )
            {
                #if DP_MSAC_C1
                    MSAC_C1_CheckIndDxOut();
                #endif /* #if DP_MSAC_C1 */

                DpAppl_IsrDxOut();
                VPC3_CON_IND_DX_OUT();
            }

            taskState = PROFIBUS_TS_PollNewGcCommand;
        }
        break;

    case PROFIBUS_TS_PollNewGcCommand:
        {
            // IND_NEW_GC_COMMAND
            if ( VPC3_POLL_IND_NEW_GC_COMMAND() )
            {
                DpAppl_IsrNewGlobalControlCommand( VPC3_GET_GC_COMMAND() );
                VPC3_CON_IND_NEW_GC_COMMAND();
            }

            taskState = PROFIBUS_TS_PollNewSsaData;
        }
        break;

    case PROFIBUS_TS_PollNewSsaData:
        {
            // IND_NEW_SSA_DATA
            if ( VPC3_POLL_IND_NEW_SSA_DATA() )
            {
                Vpc3_CopyFromVpc3( (MEM_UNSIGNED8_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0], VPC3_GET_SSA_BUF_PTR(), 4 );
                DpAppl_IsrNewSetSlaveAddress( (MEM_STRUC_SSA_BLOCK_PTR)&pDpSystem->abPrmCfgSsaHelpBuffer[0] );

                bResult = VPC3_FREE_SSA_BUF();

                VPC3_CON_IND_NEW_SSA_DATA();
            }

            taskState = PROFIBUS_TS_PollBaudrateDetect;
        }
        break;

    case PROFIBUS_TS_PollBaudrateDetect:
        {
            // IND_BAUDRATE_DETECT
            if ( VPC3_POLL_IND_BAUDRATE_DETECT() )
            {
                #if DP_MSAC_C2
                    MSAC_C2_SetTimeoutIsr();
                #endif /* #if DP_MSAC_C2 */

                DpAppl_IsrBaudrateDetect();
                VPC3_CON_IND_BAUDRATE_DETECT();
            }

            taskState = PROFIBUS_TS_PollDxbOut;
        }
        break;

    case PROFIBUS_TS_PollDxbOut:
        {
            #if DP_SUBSCRIBER
                // IND_DXB_OUT
                if ( VPC3_POLL_IND_DXB_OUT() )
                {
                    DpAppl_IsrDxbOut();
                    VPC3_CON_IND_DXB_OUT();
                }
            #endif // DP_SUBSCRIBER

            taskState = PROFIBUS_TS_PollDxbLinkError;
        }
        break;

    case PROFIBUS_TS_PollDxbLinkError:
        {
            #if DP_SUBSCRIBER
                // IND_DXB_LINK_ERROR
                if( VPC3_POLL_IND_DXB_LINK_ERROR() )
                {
                    DpAppl_IsrDxbLinkError();
                    VPC3_CON_IND_DXB_LINK_ERROR();
                }
            #endif // DP_SUBSCRIBER

            taskState = PROFIBUS_TS_PollPollEnd;
        }
        break;

    case PROFIBUS_TS_PollPollEnd:
        {
            #if DP_FDL
                // IND_POLL_END
                if( VPC3_POLL_IND_POLL_END_IND() )
                {
                    VPC3_CON_IND_POLL_END_IND();
                    FDL_PollEndIsr();
                }
            #endif // DP_FDL

            taskState = PROFIBUS_TS_PollIndication;
        }
        break;

    case PROFIBUS_TS_PollIndication:
        {
            #if DP_FDL
                // IND_FDL_IND
                if( VPC3_POLL_IND_FDL_IND() )
                {
                    VPC3_CON_IND_FDL_IND();
                    FDL_IndicationIsr();
                }
            #endif // DP_FDL

            taskState = PROFIBUS_TS_PollIntAck;
        }
        break;

    case PROFIBUS_TS_PollIntAck:
        {
            #if DP_INTERRUPT_MASK_8BIT == 0
                Vpc3Write( bVpc3WoIntAck_L, (uint8_t)(pDpSystem->wPollInterruptEvent & 0xFF) );
                Vpc3Write( bVpc3WoIntAck_H, (uint8_t)(pDpSystem->wPollInterruptEvent >> 8) );

                pDpSystem->wPollInterruptEvent = 0;
            #endif // DP_INTERRUPT_MASK_8BIT

            taskState = PROFIBUS_TS_State;
        }
        break;

    case PROFIBUS_TS_State:
        {
            // internal state machine
            if ( VPC3_GetDpState( eDpStateInit ) )
            {
                // clear data
                memset( &sDpAppl.abDpOutputData , 0, DOUT_BUFSIZE );
                memset( &sDpAppl.abDpInputData,   0, DIN_BUFSIZE );

                VPC3_ClrDpState( eDpStateInit );
                VPC3_SetDpState( eDpStateRun );
            }

            taskState = PROFIBUS_TS_DpState;
        }
        break;

    case PROFIBUS_TS_DpState:
        {
            // VPC3+ DP-state
            uint8_t bStatusRegHigh = VPC3_GET_STATUS_H();
            uint8_t bStatusRegLow = VPC3_GET_STATUS_L();

            if ( (( bStatusRegLow & VPC3_PASS_IDLE ) == 0x00 ) || (( bStatusRegHigh & AT_MASK ) != AT_VPC3S ) )
            {
                //LOG_WRITELN("DpStateErr");
                sVpc3Error.bErrorCode = bStatusRegLow;
                sVpc3Error.bCnId = bStatusRegHigh;

                taskState = PROFIBUS_TS_SetResetVPC3;
                break;
            }

            uint8_t bDpState = ( bStatusRegLow & MASK_DP_STATE );

            switch ( bDpState )
            {
            case WAIT_PRM:
                {
                    //LOG_WRITELN("WAIT PRM");
                }
                break;

            case WAIT_CFG:
                {
                    //LOG_WRITELN("WAIT CFG");
                }
                break;

            case DATA_EX:
                {
                    //LOG_WRITELN("DATA EX");
                    if (    ( VPC3_GetDpState( eDpStateApplReady ) )
                         && ( VPC3_GetDpState( eDpStateRun )  )
                       )
                    {
                        // profibus input ( slave to master )
                        DpAppl_ReadInputData();

                        /*masterData[0] = sDpAppl.abDpOutputData[0];
                        masterData[1] = sDpAppl.abDpOutputData[1];*/
                    }
                }
                break;

            case DP_ERROR:
            default:
                {
                    //LOG_WRITELN("DpStateDefErr");
                    sVpc3Error.bErrorCode = VPC3_GET_DP_STATE();
                    taskState = PROFIBUS_TS_SetResetVPC3;
                }
                break;
            }

            if (taskState != PROFIBUS_TS_SetResetVPC3)
                taskState = PROFIBUS_TS_CheckEvIoOut;
        }
        break; // PROFIBUS_TS_DpState

    case PROFIBUS_TS_CheckEvIoOut:
        {
            // profibus output ( master to slave )
            DpAppl_CheckEvIoOut();

            taskState = PROFIBUS_TS_ApplicationReady;
        }
        break;

    case PROFIBUS_TS_ApplicationReady:
        {
            // handle here profibus interrupt events
            if ( VPC3_GetDpState( eDpStateCfgOkStatDiag ) )
            {
                DpAppl_ApplicationReady();
            }

            taskState = PROFIBUS_TS_Main;
            //LOG_WRITELN("ApplicationReady");
        }
        break;

    // case PROFIBUS_TS_Nothing:
    default:
        break;
    }
}

#endif // PROFIBUS_HW_MODULE_TYPE
