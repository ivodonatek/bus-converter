//******************************************************************************
//  Sprava aplikaci
//*****************************************************************************/

#include "sys/sys_config.h"
#include "device/vpc3.h"
#include "app.h"
#include "half_duplex_line.h"
#include "modbus_master.h"
#include "modbus_reader.h"
#include "modbus_client_magx2.h"
#include "xbus_modbus.h"
#include "mbus_slave.h"
#include "profibus_slave.h"

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void App_Init(void)
{
    HalfDuplexLine_Init();
    ModbusMaster_Init();
    ModbusReader_Init();
    ModbusClientMagX2_Init();
    XbusModbus_Init();
    XbusModbus_Open();
#ifdef MBUS_HW_MODULE_TYPE
    MbusSlave_Init();
    MbusSlave_Open();
#endif // MBUS_HW_MODULE_TYPE
#ifdef PROFIBUS_HW_MODULE_TYPE
    Vpc3_Init();
    ProfibusSlave_Init();
#endif // PROFIBUS_HW_MODULE_TYPE
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void App_Task(void)
{
    HalfDuplexLine_Task();
    ModbusMaster_Task();
    ModbusReader_Task();
    ModbusClientMagX2_Task();
    XbusModbus_Task();
#ifdef MBUS_HW_MODULE_TYPE
    MbusSlave_Task();
#endif // MBUS_HW_MODULE_TYPE
#ifdef PROFIBUS_HW_MODULE_TYPE
    ProfibusSlave_Task();
#endif // PROFIBUS_HW_MODULE_TYPE
}
