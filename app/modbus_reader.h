//******************************************************************************
//  Vycteni skupiny registru.
//*****************************************************************************/

#ifndef MODBUS_READER_H_INCLUDED
#define MODBUS_READER_H_INCLUDED

#include "sys/types.h"

// datove typy
typedef enum
{
    MODBUS_READER_DATA_UINT16,
    MODBUS_READER_DATA_UINT32,

} TModbusReaderDataType;

// zaznam/predpis pro cteni registru
typedef struct ModbusReaderRecord
{
    // adresa registru
    uint16_t regAddr;

    // pocet registru
    uint8_t regCount;

    // datovy typ
    TModbusReaderDataType dataType;

    // datove uloziste (zajistit dostatek pameti pro dany dataType)
    void* dataStorage;

} TModbusReaderRecord;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void ModbusReader_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void ModbusReader_Task(void);

//------------------------------------------------------------------------------
//  Otevreni, vraci TRUE po uspechu.
//------------------------------------------------------------------------------
BOOL ModbusReader_Open(void);

//------------------------------------------------------------------------------
//  Zavreni.
//------------------------------------------------------------------------------
void ModbusReader_Close(void);

//------------------------------------------------------------------------------
//  Zacne vycitat skupinu registru.
//  Volat ModbusReader_Pending pro zjisteni stavu trvani transakce,
//  vysledek pak pomoci ModbusReader_GetResult.
//------------------------------------------------------------------------------
void ModbusReader_Read(TModbusReaderRecord* records, uint8_t recordCount);

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL ModbusReader_Pending(void);

//------------------------------------------------------------------------------
//  Vraci vysledek posledni transakce (TRUE == OK).
//------------------------------------------------------------------------------
BOOL ModbusReader_GetResult(void);

#endif // MODBUS_READER_H_INCLUDED
