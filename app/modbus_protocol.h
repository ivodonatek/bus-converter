//******************************************************************************
//  Modbus protokol
//*****************************************************************************/

#ifndef MODBUS_PROTOCOL_H_INCLUDED
#define MODBUS_PROTOCOL_H_INCLUDED

#include "sys/types.h"

/*
 * Constants which defines the format of a modbus frame. The example is
 * shown for a Modbus RTU/ASCII frame. Note that the Modbus PDU is not
 * dependent on the underlying transport.
 *
 * <------------------------ MODBUS SERIAL LINE PDU (1) ------------------->
 *              <----------- MODBUS PDU (1') ---------------->
 *  +-----------+---------------+----------------------------+-------------+
 *  | Address   | Function Code | Data                       | CRC/LRC     |
 *  +-----------+---------------+----------------------------+-------------+
 *  |           |               |                                   |
 * (2)        (3/2')           (3')                                (4)
 *
 * (1)  ... MODBUS_SER_PDU_SIZE_MAX = 256
 * (2)  ... MODBUS_SER_PDU_ADDR_OFF = 0
 * (3)  ... MODBUS_SER_PDU_PDU_OFF  = 1
 * (4)  ... MODBUS_SER_PDU_SIZE_CRC = 2
 *
 * (1') ... MODBUS_PDU_SIZE_MAX     = 253
 * (2') ... MODBUS_PDU_FUNC_OFF     = 0
 * (3') ... MODBUS_PDU_DATA_OFF     = 1
 */

#define MODBUS_SER_PDU_SIZE_MIN 4   /* Minimum size of a serial line PDU. */
#define MODBUS_SER_PDU_SIZE_MAX 256 /* Maximum size of a serial line PDU. */
#define MODBUS_SER_PDU_ADDR_OFF 0   /* Offset of address in serial line PDU. */
#define MODBUS_SER_PDU_FUNC_OFF 1   /* Offset of function code in serial line PDU. */
#define MODBUS_SER_PDU_DATA_OFF 2   /* Offset for response data in serial line PDU. */
#define MODBUS_SER_PDU_READ_REG_OFF  3  /* Offset of read register values in serial line PDU. */

#define MODBUS_PDU_SIZE_MIN     1   /* Function Code */
#define MODBUS_PDU_SIZE_MAX     253 /* Maximum size of a PDU. */
#define MODBUS_PDU_FUNC_OFF     0   /* Offset of function code in PDU. */
#define MODBUS_PDU_DATA_OFF     1   /* Offset for response data in PDU. */

#define MODBUS_ADDRESS_BROADCAST    ( 0 )   /* Modbus broadcast address. */
#define MODBUS_ADDRESS_MIN          ( 1 )   /* Smallest possible slave address. */
#define MODBUS_ADDRESS_MAX          ( 247 ) /* Biggest possible slave address. */
#define MODBUS_ADDRESS_UNKNOWN      ( 0xFF ) /* undefined address - for internal purposes. */

#define MODBUS_FUNC_NONE                          (  0 )
#define MODBUS_FUNC_READ_COILS                    (  1 )
#define MODBUS_FUNC_READ_DISCRETE_INPUTS          (  2 )
#define MODBUS_FUNC_WRITE_SINGLE_COIL             (  5 )
#define MODBUS_FUNC_WRITE_MULTIPLE_COILS          ( 15 )
#define MODBUS_FUNC_READ_HOLDING_REGISTERS        (  3 )
#define MODBUS_FUNC_READ_INPUT_REGISTER           (  4 )
#define MODBUS_FUNC_WRITE_REGISTER                (  6 )
#define MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS      ( 16 )
#define MODBUS_FUNC_READWRITE_MULTIPLE_REGISTERS  ( 23 )
#define MODBUS_FUNC_DIAG_READ_EXCEPTION           (  7 )
#define MODBUS_FUNC_DIAG_DIAGNOSTIC               (  8 )
#define MODBUS_FUNC_DIAG_GET_COM_EVENT_CNT        ( 11 )
#define MODBUS_FUNC_DIAG_GET_COM_EVENT_LOG        ( 12 )
#define MODBUS_FUNC_OTHER_REPORT_SLAVEID          ( 17 )
#define MODBUS_FUNC_ERROR                         ( 128 )

#define MODBUS_READ_HOLDING_REGISTERS_COUNT_MAX   ( 125 )

// Modbus adresa
typedef uint8_t TModbusAddress;

// Modbus funkcni kod
typedef uint8_t TModbusCode;

//------------------------------------------------------------------------------
//  Kontrola povolene adresy
//------------------------------------------------------------------------------
BOOL Modbus_IsAddressValid(TModbusAddress addr);

//------------------------------------------------------------------------------
//  Kontrola povolene slave adresy
//------------------------------------------------------------------------------
BOOL Modbus_IsSlaveAddressValid(TModbusAddress addr);

//------------------------------------------------------------------------------
//  Vrati adresu v ramci.
//------------------------------------------------------------------------------
TModbusAddress Modbus_GetFrameAddress(uint8_t* frame);

//------------------------------------------------------------------------------
//  Vrati funkcni kod v ramci.
//------------------------------------------------------------------------------
TModbusCode Modbus_GetFrameCode(uint8_t* frame);

//------------------------------------------------------------------------------
//  Zkontroluje Modbus ramec a vrati TRUE pokud je vporadku.
//------------------------------------------------------------------------------
BOOL Modbus_IsValidFrame(uint8_t* frame, uint16_t frameLgt);

//------------------------------------------------------------------------------
//  Zkontroluje Modbus ramec odpovedi na pozadavek Read Holding Registers
//  a vrati TRUE pokud je vporadku.
//------------------------------------------------------------------------------
BOOL Modbus_IsValidResponseFrame_ReadHoldingRegisters(uint8_t* frame, uint16_t frameLgt,
    TModbusAddress addr, uint8_t regCount);

//------------------------------------------------------------------------------
//  Vrati klidovy interval mezi ramci [ms].
//  bits ... start,data,parity,stop
//------------------------------------------------------------------------------
uint16_t Modbus_GetInterFrameDelay(uint32_t baudRate, uint8_t bits);

//------------------------------------------------------------------------------
//  Sestavi ramec Read Holding Registers.
//  Vrati pocet bytu ramce (>0) nebo 0 po chybe.
//------------------------------------------------------------------------------
uint16_t Modbus_GetFrame_ReadHoldingRegisters(TModbusAddress slaveAddr,
    uint16_t regAddr, uint8_t regCount, uint8_t* frame, uint16_t frameBufLgt);

//------------------------------------------------------------------------------
//  Parsing typu uint16_t.
//------------------------------------------------------------------------------
uint16_t Modbus_ParseUint16(uint8_t* data);

//------------------------------------------------------------------------------
//  Parsing i-teho registru typu uint16_t.
//------------------------------------------------------------------------------
uint16_t Modbus_ParseRegUint16(uint8_t* data, uint8_t i);

//------------------------------------------------------------------------------
//  Parsing typu uint32_t.
//------------------------------------------------------------------------------
uint32_t Modbus_ParseUint32(uint8_t* data);

//------------------------------------------------------------------------------
//  Parsing i-teho registru typu uint32_t.
//------------------------------------------------------------------------------
uint32_t Modbus_ParseRegUint32(uint8_t* data, uint8_t i);

#endif // MODBUS_PROTOCOL_H_INCLUDED
