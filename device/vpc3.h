//******************************************************************************
//  Ovladac VPC3
//*****************************************************************************/

#ifndef VPC3_H_INCLUDED
#define VPC3_H_INCLUDED

#include "sys/types.h"

// VPC3+C/S interface mode
/*
   VPC3_SERIAL_MODE

   Activate / Deactivate VPC3+S serial mode.

   Use following values:
   - 1 - Activate VPC3+S serial mode.
   - 0 - Deactivate VPC3+S serial mode - VPC3+C, VPC3+S, MPI12x works in parallel mode.
*/
#define VPC3_SERIAL_MODE 1

#define VPC3_SPI 1                  /*!< Used VPC3+S adaption: SPI - serial mode */
#define VPC3_I2C 0                  /*!< Used VPC3+S adaption: IIC - serial mode */
#define VPC3_PORT_PINS 0            /*!< Used VPC3+C, VPC3+S, MPI12x adaption: manufacturer specific mode (adaption via port pins) */

#define BUSINTERFACE_8_BIT  1       // Fix to 1: with serial mode the data handling to VPC3+S is always 8 bit
#define BUSINTERFACE_16_BIT 0       // Fix to 0
#define BUSINTERFACE_32_BIT 0       // Fix to 0

#define VPC3_EXTENSION              // Fix, do not edit!!!

#define DP_INTERRUPT_MASK_8BIT 0    // Special mode for reading/acknowledging profibus indications in dp_isr.c

/*
    MOTOROLA_MODE

    Activate / Deactivate VPC3+ motorola mode.

    Use following values:
      - 0 - Activate INTEL mode - the VPC3+ configuration pin MOT/XINT is set to parallel interface INTEL format.
      - 1 - Activate MOTOROLA mode - the VPC3+ configuration pin MOT/XINT is set to parallel interface MOTOROLA format.
*/
#define MOTOROLA_MODE 0

#define VPC3_ADR                uint16_t   // memory address in VPC3+
#define VPC3_PTR                *
#define VPC3_UNSIGNED8_PTR      uint8_t *  /*!< Pointer of byte to VPC3+ */
#define VPC3_NULL_PTR           (void VPC3_PTR)0
#define VPC3_TRUE               TRUE
#define VPC3_FALSE              FALSE
#define TVpc3Byte               uint8_t

#define MEM_UNSIGNED8_PTR       uint8_t *  /*!< Pointer of byte to local memory */
#define MEM_PTR                 *          /*!< Pointer attribut of local memory */

// Velikost buferu pro SPI data
#define VPC3_SPI_BUFFER_SIZE    4

/*-----------------------------------------------------------------------------------------------------------*/
/* VPC3+ configuration                                                                                       */
/*-----------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* profibus services                                                        */
/*--------------------------------------------------------------------------*/
/* todo: setup required services */

/*-----------------------------------------------------------------------*/
/* general slave parameter                                               */
/*-----------------------------------------------------------------------*/
/*!
   \def DP_ADDR
   \brief Set PROFIBUS slave address.

   Use following values:
   - 1 to 126
*/
#define DP_ADDR                     ((uint8_t)0x07)     // Slave address

/*!
   \def IDENT_NR
   \brief Set PROFIBUS ident number.

   Request your own number (www.profibus.com).
*/
//#define IDENT_NR                    ((uint16_t)0x4711)   // PROFIBUS Ident Number
#define IDENT_NR                    ((uint16_t)0x095F)   // PROFIBUS Ident Number###

/*!
   \def USER_WD
   \brief Set PROFIBUS user watchdog.
   The UserWatchdog is only active in DataExchange. The UserWatchdog
   isn't timebased, it's a counter of DataExchange-telegrams.
*/
#define USER_WD                     ((uint16_t)0x01FF)   // User Watchdog

/*-----------------------------------------------------------------------*/
/* define buffer length                                                  */
/*-----------------------------------------------------------------------*/
/*!
   \def DIN_BUFSIZE
   \brief Set length of the DIn buffer (data from slave to master).

   Use following values:
   - 0 to 244
*/
//#define DIN_BUFSIZE                 ((uint8_t)0x02)     // Length of the DIn Buffer (Data Slave to Master)                0..244
#define DIN_BUFSIZE                 ((uint8_t)32)     // Length of the DIn Buffer (Data Slave to Master)                0..244

/*!
   \def DOUT_BUFSIZE
   \brief Set length of the DOut buffer (data from master to slave).

   Use following values:
   - 0 to 244
*/
//#define DOUT_BUFSIZE                ((uint8_t)0x02)     // Length of the DOut Buffer     (Data Master to Slave)           0..244
#define DOUT_BUFSIZE                ((uint8_t)0x00)     // Length of the DOut Buffer     (Data Master to Slave)           0..244

/*!
   \def PRM_BUFSIZE
   \brief Set length of the paramater buffer.

   The first seven byte are mandatory.

   Use following values:
   - 7 to 244
*/
#define PRM_BUFSIZE                 ((uint8_t)0x0A)     // Length of the Parameter Buffer                                 7..244

/*!
   \def DIAG_BUFSIZE
   \brief Set length of the diagnosis buffer.

   The first 6 bytes are mandatory.

   Use following values:
   - 6 to 244
*/
#define DIAG_BUFSIZE                ((uint8_t)0x06)     // Length of the Diagnosis Buffer                                 6..244

/*!
   \def CFG_BUFSIZE
   \brief Set length of the configuration buffer.

   Use following values:
   - 1 to 244
*/
//#define CFG_BUFSIZE                 ((uint8_t)0x01)     // Length of the Configuration Buffer                             1..244
#define CFG_BUFSIZE                 ((uint8_t)0x02)     // Length of the Configuration Buffer                             1..244

/*!
   \def SSA_BUFSIZE
   \brief Set length of the set slave address buffer.

   Use following values:
   - 0 - deactivates the service set slave address
   - 4 to 244 - service is activated
*/
#define SSA_BUFSIZE                 ((uint8_t)0x00)     // Length of the Input Data in the Set_Slave_Address-Buffer 0 and 4..244
                                                      // 0: SetSlaveAddress will be deactivated!

/*---------------------------------------------------------------------------*/
/* set hardware modes                                                        */
/*---------------------------------------------------------------------------*/
/*!
   \def DP_VPC3_4KB_MODE
   \brief Activate / Deactivate VPC3+ 4kByte-mode.

   Use following values:
   - 0 - VPC3+ works in 2kByte mode.
   - 1 - VPC3+ works in 4kByte mode.
*/
#define DP_VPC3_4KB_MODE 0

/*-----------------------------------------------------------------------*/
/* ModeRegister0 (7..0) ( page 15 )                                      */
/*-----------------------------------------------------------------------*/
//
//  bit  7       6      5        4         3       2       1          0
//  --------------------------------------------------------------------------
//  | Freeze | Sync | Early | Int_Pol | MinTSDR |  WD  | Dis_Stop | Dis_Start |
//  |  supp. | supp.|  RDY  |         |         | BASE | Control  | Control   |
//  --------------------------------------------------------------------------
//       1       1      0        0         0       0        0          0        = 0xC0 // Default
//
/*!
   \def INIT_VPC3_MODE_REG_L
   \brief Set Moderegister 0 (Bit 0.. Bit 7).


   <b>Bit 0:</b> Dis_Start_Control - Disable Startbit Control
      - 0 - Monitoring the following start bit is enabled.
      - 1 - Monitoring the following start bit is switched off

   \attention A Set-Param telegram overwrites this memory cell in the DP mode.(Refer to the user specific data.)

   <b>Bit 1:</b> Dis_Stop_Control - Disable Stopbit Control
      - 0 - Stop bit monitoring is enabled.
      - 1 - Stop bit monitoring is switched off

   \attention A Set-Param telegram overwrites this memory cell in the DP mode.(Refer to the user specific data.)

   <b>Bit 2:</b> WD_Base: Watchdog Time Base
      - 0 - Watchdog time base is 10 ms (default state)
      - 1 - Watchdog time base is  1 ms

   <b>Bit 3:</b> MinTSDR: Default setting for the MinTSDR after reset for DP operation or combi operation.
      - 0 - Pure DP operation (default configuration!)

   <b>Bit 4:</b> INT_Pol: Interrupt Polarity
      - 0 - The interrupt output is low-active.
      - 1 - The interrupt output is high-active.

   <b>Bit 5:</b> Early_Rdy: Early Ready
      - 0 - Normal Ready: Ready is generated when data is valid (read) or when data has been accepted (write).
      - 1 - Ready is generated one clock pulse earlier.

   <b>Bit 6:</b> Sync_Supported: Sync_Mode support
      - 0 - Sync_Mode is not supported.
      - 1 - Sync_Mode is supported.

   <b>Bit 7:</b> Freeze_Supported - Freeze_Mode support
      - 0 - Freeze_Mode is not supported.
      - 1 - Freeze_Mode is supported
*/
#define INIT_VPC3_MODE_REG_L        ((uint8_t)0xC0)

/*-----------------------------------------------------------------------*/
/* ModeRegister0 (15..8) ( page 15 )                                     */
/*-----------------------------------------------------------------------*/
//
//  bit15      14       13           12           11           10         9         8
//  ------------------------------------------------------------------------------------
//  | Err | PrmCmd | Spec_Clear | Spec_Prm |  SetExtPrm  | User_Time | EOI_Time |  DP  |
//  | CNT |  Supp  |    Mode    | Buf_Mode |     Supp    |    Base   |   Base   | Mode |
//  ------------------------------------------------------------------------------------
//     1       0        1            0              0           1         0         1   = 0xA5 // Default
//
/*!
   \def INIT_VPC3_MODE_REG_H
   \brief Set Moderegister 0 (Bit 8.. Bit 15).


   <b>Bit 8:</b> DP_Mode: DP_Mode enable
      - 0 - Monitoring the following start bit is enabled.
      - 1 - Monitoring the following start bit is switched off

   <b>Bit 9:</b> EOI_Time_Base: End-of-Interrupt Timebase
      - 0 - The interrupt inactive time is at least 1 �sec long.
      - 1 - The interrupt inactive time is at least 1 ms long

   <b>Bit 19:</b> User_Time_Base: Timebase of the cyclical User_Time_Clock-Interrupt
      - 0 - The User_Time_Clock-Interrupt occurs every 1 ms.
      - 1 - The User_Time_Clock-Interrupt occurs every 10 ms. (mandatory DPV1)

   <b>Bit 11:</b> Set_Ext_Prm_Supported: Set_Ext_Prm telegram support
      - 0 - SAP 53 is deactivated
      - 1 - SAP 53 is activated

   <b>Bit 12:</b> Spec_Prm_Buf_Mode: Special Parameter Buffer Mode
      - 0 - No special parameter buffer.
      - 1 - Special parameter buffer mode. Parameterization data will be stored directly in the special parameter buffer.

	\attention Spec_Prm_Buf_Mode: Is no longer supported by the software, 4kByte memory is enough to handle Set-Prm-telegram via auxiliary buffer.

   <b>Bit 13:</b> Spec_Clear_Mode: Special Clear Mode (Fail Safe Mode)
      - 0 - No special clear mode.
      - 1 - Special clear mode. VPC3+ will accept data telegrams with data unit = 0

   <b>Bit 14:</b> PrmCmd_Supported: PrmCmd support for redundancy
      - 0 - PrmCmd is not supported.
      - 1 - PrmCmd is supported

   <b>Bit 15:</b> Err_CNT: Error Counter supported
      - 0 - Error counter is not supported.
      - 1 - Error counter is supported
*/
#define INIT_VPC3_MODE_REG_H        ((uint8_t)0x25)


/*-----------------------------------------------------------------------*/
/* ModeRegister2 (7..0) ( page 19 )   (only VPC3+B, VPC3+C)              */
/*-----------------------------------------------------------------------*/
//
//  bit  7          6          5        4       3          2          1            0
//  --------------------------------------------------------------------------------------
//  |  4KB  |   No_Check   | SYNC_  | SYNC_ | DX_Int_ | DX_Int_ |  No_Check_  | NEW_GC_  |
//  |  Mode | Prm_Reserved |  Pol   |  ENA  |  Port   |  Mode   | GC_RESERVED | Int_Mode |
//  --------------------------------------------------------------------------------------
//       1          0          0        0       0          1          0            1        = 0x05 or 0x85
//
/*!
   \def INIT_VPC3_MODE_REG_2
   \brief Set Moderegister 2 (Bit 0.. Bit 7).

   <b>Bit 0:</b> GC_Int_Mode: Controls generation of GC Interrupt
      - 0 - GC Interrupt is only generated, if changed GC telegram is received
      - 1 - GC Interrupt is generated after every GC telegram (default)

   <b>Bit 1:</b> No_Check_GC_Reserved: Disables checking of the reserved GC bits
      - 0 - Reserved bits of GC-telegram are checked (default).
      - 1 - Reserved bits of GC-telegram are not checked.

   <b>Bit 2:</b> DX_Int_Mode: Mode of Dataexchange Interrupt
      - 0 - DX Interrupt only generated, if DOUT length not 0 (default).
      - 1 - DX Interrupt generated after every DX-telegram

   <b>Bit 3:</b> DX_Int_Port: Port mode for Dataexchange Interrupt
      - 0 - DX Interrupt not assigned to port DATA_EXCH (default).
      - 1 - DX Interrupt (synchronized to GC-SYNC) assigned to port DATA_EXCH.

   <b>Bit 4:</b> SYNC_Ena: Enable generation of SYNC pulse (for Isochron Mode only)
      - 0 - SYNC pulse generation is disabled (default).
      - 1 - SYNC pulse generation is enabled.

   <b>Bit 5:</b> SYNC_Pol: Polarity of SYNC pulse (for Isochron Mode only)
      - 0 - negative polarity of SYNC pulse (default)
      - 1 - positive polarity of SYNC pulse

   <b>Bit 6:</b> No_Check_Prm_Reserved: Disables checking of the reserved Prm bits
      - 0 - Reserved bits of Prm-telegram are checked (default).
      - 1 - Reserved bits of Prm-telegram are not checked.

   <b>Bit 7:</b> 4kB_Mode: Size of internal RAM
      - 0 - 2kB RAM (default).
      - 1 - 4kB RAM
*/
#if DP_VPC3_4KB_MODE
   #define INIT_VPC3_MODE_REG_2    ((uint8_t)0x85)
#else
   #define INIT_VPC3_MODE_REG_2    ((uint8_t)0x05)
#endif//#if DP_VPC3_4KB_MODE

/*-----------------------------------------------------------------------*/
/* ModeRegister3 (7..0) ( page 19 )   (VPC3+C)                           */
/*-----------------------------------------------------------------------*/
//
//  bit  7          6          5          4          3         2       1         0
//  -----------------------------------------------------------------------------------
//  | RESERVED | RESERVED | RESERVED | RESERVED |   PLL   | En-Chk | DX-OUT | GC_Int  |
//  |          |          |          |          |   SUPP  |  SSAP  |  Mode  | Mode_Ex |
//  -----------------------------------------------------------------------------------
//       0          0          0          0          0         0       0         0        = 0x00
//
/*!
   \def INIT_VPC3_MODE_REG_3
   \brief Set Moderegister 2 (Bit 0.. Bit 3).

   <b>Bit 0:</b> GC_Int_Mode_Ext: extend GC_Int_Mode, works only if GC_Int_Mode=1
      - 0 - GC Interrupt is only generated, if changed GC telegram is received
      - 1 - GC Interrupt is only generated, if GC telegram with changed Control_Command is received

   <b>Bit 1:</b> DX_Int_Mode_2: Mode of DX_out interrupt
      - 0 - DX_Out interrupt is generated after each Data_Exch telegram
      - 1 - DX_Out interrupt is only generated, if received data is not equal to current data in DX_Out buffer of user

   <b>Bit 2:</b> En_Chk_SSAP: Evaluation of Source Address Extension
      - 0 - VPC3+ accept any value of S_SAP
      - 1 - VPC3+ only process the received telegram if the S_SAP match to the default values presented by the IEC 61158

   <b>Bit 3:</b> PLL_Supported: Enables PLL for clock save
      - 0 - PLL is disabled
      - 1 - PLL is enabled; For use of PLL, SYNC_Ena must be set.

   <b>Bit 4:</b> Reserved

   <b>Bit 5:</b> Reserved

   <b>Bit 6:</b> Reserved

   <b>Bit 7:</b> Reserved
*/
#define INIT_VPC3_MODE_REG_3    ((uint8_t)0x00)

/*---------------------------------------------------------------------------*/
/* set interrupt indications                                                 */
/*---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------*/
/* Interrupt Mask Register (7..0) ( page 23 )                            */
/*-----------------------------------------------------------------------*/
//  bit7       6          5           4             3             2          1         0
//  ----------------------------------------------------------------------------------------
//  | DXB | New_Ext  | DXB_LINK | User_Timer |    WD_DP     | Baud_Rate | Go/Leave | Clock  |
//  | OUT | PRM_Data |  ERROR   |   Clock    | Mode_Timeout |   detect  |  DataEx  | Sync   |
//  ----------------------------------------------------------------------------------------
//     0       0          0           0             0             0          0         0      = 0x00 // Default
//
/*!
   \def INIT_VPC3_IND_L
   \brief Set interrupt mask register (Bit 0.. Bit 7).

   <b>Bit 0:</b> Clock_Sync: The VPC3+C/S has received a Clock_Value telegram or an error occurs.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 0:</b> MAC_RESET: After processing the current request, the VPC3+C/S has entered the
                            Offline state (by setting the Go_Offline bit). Only valid if Clock sync is disabled.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 1:</b> Go/Leave_DATA_EX: The DP_SM has entered or exited the DATA_EX state
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 2:</b> Baudrate_Detect: The VPC3+ has left the Baud_Search state and found a baud rate.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 3:</b> WD_DP_Control_Timeout: The watchdog timer has run out in the DP_Control WD state
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 4:</b> User_Timer_Clock: The time base for the User_Timer_Clocks has run out ( 1 /10ms).
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 5:</b> DXB_Link_Error: The Watchdog cycle is elapsed and at least one Publisher-Subscriber connection breaks down.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 6:</b> New_Ext_Prm_Data: The VPC 3+ has received a Set_Ext_Param telegram and made the data available in the Prm buffer.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 7:</b> DXB_Out: VPC 3+ has received a DXB telegram and made the new output data available in the N buffer.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.
*/
#define INIT_VPC3_IND_L  0x00

/*-----------------------------------------------------------------------*/
/* Interrupt Mask Register (15..8) ( page 23 )                           */
/*-----------------------------------------------------------------------*/
//  bit  15         14       13        12            11       10       9         8
//  -----------------------------------------------------------------------------------
//  | REQ_PDU | POLL_END | DX_OUT | Diag_Buffer | New_PRM | NewCfg | NewSSA |  NewGC  |
//  |   Ind   |    Ind   |        |   Changed   |   Data  |  Data  |  Data  | Command |
//  -----------------------------------------------------------------------------------
//       0          0        0          0            0       0        0         0      = 0x00 // Default
//
/*!
   \def INIT_VPC3_IND_H
   \brief Set interrupt mask register (Bit 8.. Bit 15).

   <b>Bit 8:</b> New_GC_Command: The VPC3+ has received a Global_Control telegram and this byte is stored in the R_GC_Command RAM cell.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 9:</b> New_SSA_Date: The VPC3+ has received a Set_Slave_Address telegram and made the data available in the SSA buffer.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 10:</b> New_Cfg_Data: The VPC3+ has received a Check_Cfg telegram and made the data available in the Cfg buffer.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 11:</b> New_Prm_Data: The VPC3+ has received a Set_Param telegram and made the data available in the Prm buffer.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 12:</b> Diag_Buffer_Changed: Due to the request made by New_Diag_Cmd, VPC3+ exchanged the diagnostics buffer and again made the old buffer available to the user.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 13:</b> DX_Out: The VPC3+ has received a DX-Out telegram and made the data available in the DX-Out buffer.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 14:</b> Poll_End_Ind: The VPC 3+ has send the response to an acyclic service.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.

   <b>Bit 15:</b> FDL_Ind: The VPC 3+ has received an acyclic service request and made the data available in an indication buffer.
      - 0 - The VPC3+ doesn't generates an interrupt.
      - 1 - The VPC3+ generates an interrupt.
*/
#define INIT_VPC3_IND_H  0x00

/*-----------------------------------------------------------------------*/
/* Interrupt Mask Register (15..0) ( page 23 )                           */
/*-----------------------------------------------------------------------*/
//  bit  15         14       13         12           11       10       9         8       7       6          5           4             3             2          1         0
//  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//  | REQ_PDU | POLL_END | DX_OUT | Diag_Buffer | New_PRM | NewCfg | NewSSA |  NewGC  | DXB | New_Ext  | DXB_LINK | User_Timer |    WD_DP     | Baud_Rate | Go/Leave | Clock  |
//  |   Ind   |    Ind   |        |   Changed   |   Data  |  Data  |  Data  | Command | OUT | PRM_Data |  ERROR   |   Clock    | Mode_Timeout |   detect  |  DataEx  | Sync   |
//  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//       0          0        1          1            1        1        0        1        0       0          0           0             0             0          1         0      = 0x3D02 // Default
//
#define SM_INTERRUPT_MASK ((uint16_t)0x3D02)

/*---------------------------------------------------------------------------*/
/* end of user defines                                                       */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* !!!!! do not edit this calculating part !!!!!                             */
/*---------------------------------------------------------------------------*/
#define DP_MSAC_C1 0
#define DP_MSAC_C2 0
#define DP_ALARM   0
#define DPV1_IM_SUPP 0
#define DP_SUBSCRIBER 0
#define REDUNDANCY 0
#define RS232_REDUNDANCY 0
#define DP_TIMESTAMP 0
#define DP_ISOCHRONOUS_MODE 0

#define MAX(A, B)   ((A) > (B) ? (A) : (B))     /*!< Determine maximum.  */
#define HELP_BUFSIZE                ((uint16_t)MAX( PRM_BUFSIZE, CFG_BUFSIZE ))

// == Segmentation VPC3+ ===============================================================
#if DP_VPC3_4KB_MODE
   // 16 Byte segmentation
   #define SEG_OFFSET               ((uint8_t)0x0F)
   #define SEG_MULDIV               ((uint8_t)0x04)
   #define SEG_ADDBYTE              ((uint16_t)0xFFF0)
   #define SEG_ADDWORD              ((uint16_t)0xFFF0)
#else
   // 8 Byte segmentation
   #define SEG_OFFSET               ((uint8_t)0x07)
   #define SEG_MULDIV               ((uint8_t)0x03)
   #define SEG_ADDBYTE              ((uint16_t)0xFFF8)
   #define SEG_ADDWORD              ((uint16_t)0xFFF8)
#endif//#if DP_VPC3_4KB_MODE

// == DPV1/DPV2 ========================================================================
#define DP_C1_USE_RD_WR_SAP         0x01
#define DP_C1_USE_ALARM_SAP         0x04

// == MSAC1 ============================================================================
#if DP_ALARM
   #define DP_MSAC_C1               1
#endif//#if DP_ALARM

#if DP_MSAC_C1

   #define DP_C1_LEN                C1_LEN

   // == Alarm ========================================================================
   #if DP_ALARM
      #if DP_ALARM_OVER_SAP50
         #define DP_C1_SAPS         ( DP_C1_USE_RD_WR_SAP | DP_C1_USE_ALARM_SAP )
      #else
         #define DP_C1_SAPS         ( DP_C1_USE_RD_WR_SAP )
      #endif//#if DP_ALARM_OVER_SAP50
   #else
      #define DP_C1_SAPS            ( DP_C1_USE_RD_WR_SAP )
   #endif//#if DP_ALARM
#else
   #define DP_C1_SAPS               0x00
   #define DP_C1_LEN                ((uint8_t)0x00)

   #define DP_ALARM                 0
#endif//#if DP_MSAC_C1

// == MSAC2 ============================================================================
#if DP_MSAC_C2
   #define DP_C2_NUM_SAPS           C2_NUM_SAPS
   #define DP_C2_LEN                C2_LEN
#else
   #define DP_C2_NUM_SAPS           ((uint8_t)0x00)
   #define DP_C2_LEN                ((uint8_t)0x00)
#endif//#if DP_MSAC_C2

// == Subscriber =======================================================================
#if DP_SUBSCRIBER
   #define DP_MAX_LINK_SUPPORTED    MAX_LINK_SUPPORTED
   #define DP_MAX_DATA_PER_LINK     MAX_DATA_PER_LINK
#else
   #define DP_MAX_LINK_SUPPORTED    0
   #define DP_MAX_DATA_PER_LINK     0
#endif//#if DP_SUBSCRIBER

// == Calculating FDL list =============================================================
#if ( DP_MSAC_C1 || DP_MSAC_C2 )
   #define DP_FDL                   1
#else
   #define DP_FDL                   0
#endif//#if ( DP_MSAC_C1 || DP_MSAC_C2 )

#if (DP_C1_SAPS & DP_C1_USE_RD_WR_SAP)
   #define DP_TEMP_X1               1
#else
   #define DP_TEMP_X1               0
#endif//#if (DP_C1_SAPS & DP_C1_USE_RD_WR_SAP)

#if (DP_C1_SAPS & DP_C1_USE_ALARM_SAP )
   #define DP_TEMP_X2               1
   #define SAP_50
#else
   #define DP_TEMP_X2               0
#endif//#if (DP_C1_SAPS & DP_C1_USE_ALARM_SAP )

#if DP_MSAC_C1
   #define DP_TEMP_X (DP_TEMP_X1 + DP_TEMP_X2)
#else
   #define DP_TEMP_X                0
#endif//#if DP_MSAC_C1

#if DP_MSAC_C2
   #define DP_MAX_SAPS              (DP_TEMP_X + DP_C2_NUM_SAPS + 1 )
#else
   #define DP_MAX_SAPS              DP_TEMP_X
#endif//#if DP_MSAC_C2

#define DP_ORG_LENGTH               0x40 // organizational parameter

#if DP_ISOCHRONOUS_MODE
   #define DP_ISO_LENGTH            0x40
   #define DP_TS_LENGTH             0x00
#else
   #define DP_ISO_LENGTH            0x00

#if DP_TIMESTAMP
      #define DP_TS_LENGTH          0x20
#else
      #define DP_TS_LENGTH          0x10
#endif//#if DP_TIMESTAMP
#endif//#if DP_ISOCHRONOUS_MODE

#if DP_FDL
   #define FDL_SAP_MAX DP_MAX_SAPS
   /* length of vpc3-sap-list */
   #define SAP_LENGTH               (((FDL_SAP_MAX*7)+SEG_OFFSET) & SEG_ADDBYTE)
#else
   #define SAP_LENGTH               0x10
#endif//#if DP_FDL

#if DP_VPC3_4KB_MODE
   #define ASIC_RAM_LENGTH          0x1000
   #define ASIC_USER_RAM            (ASIC_RAM_LENGTH - DP_ORG_LENGTH - SAP_LENGTH - DP_ISO_LENGTH - DP_TS_LENGTH)
#else
   #define ASIC_RAM_LENGTH          0x800
   #define ASIC_USER_RAM            (ASIC_RAM_LENGTH - DP_ORG_LENGTH - SAP_LENGTH - DP_ISO_LENGTH - DP_TS_LENGTH)
#endif//#if DP_VPC3_4KB_MODE

#define ISR_ENABLE_VPC3_INT_CLOCK_SYNC              0
#define ISR_ENABLE_VPC3_INT_GO_LEAVE_DATA_EX        0
#define ISR_ENABLE_VPC3_INT_BAUDRATE_DETECT         0
#define ISR_ENABLE_VPC3_INT_WD_DP_TIMEOUT           0
#define ISR_ENABLE_VPC3_INT_USER_TIMER_CLOCK        0
#define ISR_ENABLE_VPC3_INT_DXB_LINK_ERROR          0
#define ISR_ENABLE_VPC3_INT_NEW_EXT_PRM_DATA        0
#define ISR_ENABLE_VPC3_INT_DXB_OUT                 0
#define ISR_ENABLE_VPC3_INT_NEW_GC_COMMAND          0
#define ISR_ENABLE_VPC3_INT_NEW_SSA_DATA            0
#define ISR_ENABLE_VPC3_INT_NEW_CFG_DATA            0
#define ISR_ENABLE_VPC3_INT_NEW_PRM_DATA            0
#define ISR_ENABLE_VPC3_INT_DIAG_BUF_CHANGED        0
#define ISR_ENABLE_VPC3_INT_DX_OUT                  0
#define ISR_ENABLE_VPC3_INT_RESERVED                0
#define ISR_ENABLE_VPC3_INT_SERVICE_ERROR           0
#define ISR_ENABLE_VPC3_INT_POLL_END_IND            0
#define ISR_ENABLE_VPC3_INT_FDL_IND                 0


#if( INIT_VPC3_IND_L & 0x01 )
   #undef  ISR_ENABLE_VPC3_INT_CLOCK_SYNC
   #define ISR_ENABLE_VPC3_INT_CLOCK_SYNC           1
#endif
#if( INIT_VPC3_IND_L & 0x02 )
   #undef  ISR_ENABLE_VPC3_INT_GO_LEAVE_DATA_EX
   #define ISR_ENABLE_VPC3_INT_GO_LEAVE_DATA_EX     1
#endif
#if( INIT_VPC3_IND_L & 0x04 )
   #undef  ISR_ENABLE_VPC3_INT_BAUDRATE_DETECT
   #define ISR_ENABLE_VPC3_INT_BAUDRATE_DETECT      1
#endif
#if( INIT_VPC3_IND_L & 0x08 )
   #undef  ISR_ENABLE_VPC3_INT_WD_DP_TIMEOUT
   #define ISR_ENABLE_VPC3_INT_WD_DP_TIMEOUT        1
#endif
#if( INIT_VPC3_IND_L & 0x10 )
   #undef  ISR_ENABLE_VPC3_INT_USER_TIMER_CLOCK
   #define ISR_ENABLE_VPC3_INT_USER_TIMER_CLOCK     1
#endif
#if( INIT_VPC3_IND_L & 0x20 )
   #undef  ISR_ENABLE_VPC3_INT_DXB_LINK_ERROR
   #define ISR_ENABLE_VPC3_INT_DXB_LINK_ERROR       1
#endif
#if( INIT_VPC3_IND_L & 0x40 )
   #undef  ISR_ENABLE_VPC3_INT_NEW_EXT_PRM_DATA
   #define ISR_ENABLE_VPC3_INT_NEW_EXT_PRM_DATA     1
#endif
#if( INIT_VPC3_IND_L & 0x80 )
   #undef  ISR_ENABLE_VPC3_INT_DXB_OUT
   #define ISR_ENABLE_VPC3_INT_DXB_OUT              1
#endif


#if( INIT_VPC3_IND_H & 0x01 )
   #undef  ISR_ENABLE_VPC3_INT_NEW_GC_COMMAND
   #define ISR_ENABLE_VPC3_INT_NEW_GC_COMMAND       1
#endif
#if( INIT_VPC3_IND_H & 0x02 )
   #undef  ISR_ENABLE_VPC3_INT_NEW_SSA_DATA
   #define ISR_ENABLE_VPC3_INT_NEW_SSA_DATA         1
#endif
#if( INIT_VPC3_IND_H & 0x04 )
   #undef  ISR_ENABLE_VPC3_INT_NEW_CFG_DATA
   #define ISR_ENABLE_VPC3_INT_NEW_CFG_DATA         1
#endif
#if( INIT_VPC3_IND_H & 0x08 )
   #undef  ISR_ENABLE_VPC3_INT_NEW_PRM_DATA
   #define ISR_ENABLE_VPC3_INT_NEW_PRM_DATA         1
#endif
#if( INIT_VPC3_IND_H & 0x10 )
   #undef  ISR_ENABLE_VPC3_INT_DIAG_BUF_CHANGED
   #define ISR_ENABLE_VPC3_INT_DIAG_BUF_CHANGED     1
#endif
#if( INIT_VPC3_IND_H & 0x20 )
   #undef  ISR_ENABLE_VPC3_INT_DX_OUT
   #define ISR_ENABLE_VPC3_INT_DX_OUT               1
#endif
#if( INIT_VPC3_IND_H & 0x40 )
   #undef  ISR_ENABLE_VPC3_INT_POLL_END_IND
   #define ISR_ENABLE_VPC3_INT_POLL_END_IND         1
#endif
#if( INIT_VPC3_IND_H & 0x80 )
   #undef  ISR_ENABLE_VPC3_INT_FDL_IND
   #define ISR_ENABLE_VPC3_INT_FDL_IND              1
#endif

/*-----------------------------------------------------------------------------------------------------------*/
/* VPC3+ / PROFIBUS definitions                                                                              */
/*-----------------------------------------------------------------------------------------------------------*/
/* contents:

    1.0 VPC3+
    1.1     - register structure of VPC3+
    1.2     - literals for interrupt register
    1.3     - literals for status register
    1.4     - literals for mode register 0
    1.5     - literals for mode register 1
    1.6     - literals for mode register 2
    1.7     - basic macros for VPC3+

    2.0 - prm data
    2.1     - structure of prm data
    2.2     - literals for DPV1 Status 1
    2.3     - literals for DPV1 Status 2
    2.4     - literals for DPV1 Status 3
    2.5     - general defines for prm data
    2.6     - literals for structured prm data
    2.7     - returncodes for prm data
    2.8     - macros for prm data

    3.0 - cfg data
    3.1     - standard format of cfg byte
    3.2     - special format of cfg byte
    3.3     - literals for cfg byte
    3.4     - returncodes for cfg data
    3.5     - literals for chk config data
    3.6     - macros for cfg data
    3.7     - structure for real cfg data

    4.0 - input / output
    4.1     - structure for calculated input-/output-length
    4.2     - states for output buffer
    4.3     - macros for input buffer

    5.0 - set slave address
    5.1     - structure
    5.2     - macros

    6.0 - global control telegram
    6.1     - defines for GLOBAL CONTROL
    6.2     - macros for global control

    7.0 - diagnostic telegram
    7.1     - defines
    7.2     - structures
    7.3     - macros

    8.0 - subscriber
    8.1     - structures
    8.2     - macros

    9.0 - isochron mode, hw-pll
    9.1     - defines
    9.2     - structures

   10.0 - clocksynchronization

   11.0 - timestamp

   12.0 - redundancy

   13.0 - fdl layer
   13.1     - SAPs (service access points) for MSAC1
   13.2     - SAPs (service access points) for MSAC2
   13.3     - structure of a FDL-indication-response-buffer-head
   13.4     - structure of a FDL-sap-entry
   13.5     - structure of a sap-control-block
   13.6     - structure of a indication- or response-buffer
   13.7     - structure of the immediate-response-pdu of the FDL-RM-SAP
   13.8     - global structure of FDL state machine
   13.9     - function-codes
   13.A     - returncodes of FDL-services
   13.B     - helpful macros

   14.0 - DPV1
   14.1     - return codes for DPV1 services
   14.2     - errorcodes of DPV1 for ERROR_DECODE
   14.3     - errorcodes of DPV1 for ERROR_CODE_1
   14.4     - coding of abort
   14.5     - Function Codes for DPV1
   14.6     - general defines for DPV1
   14.7     - structures for DPV1
   14.8     - defines for ALARM
   14.9     - structure of ALARM

   15.0 - global system structure

   16.0 - ERROR defines
*/


/*-----------------------------------------------------------------------------------------------------------*/
/* 1.0 vpc3+                                                                                                 */
/*-----------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/* 1.1 register structure of vpc3+                                           */
/*---------------------------------------------------------------------------*/

   #define bVpc3RwIntReqReg_L       (VPC3_ADR)( 0x00 )   /**<(000H) Interrupt request register low */
   #define bVpc3RwIntReqReg_H       (VPC3_ADR)( 0x01 )   /**<(001H) Interrupt request register high */

   #define bVpc3RoIntReg_L          (VPC3_ADR)( 0x02 )   /**<(002H) Interrupt register low */
   #define bVpc3RoIntReg_H          (VPC3_ADR)( 0x03 )   /**<(003H) Interrupt register high */
   #define bVpc3RoStatus_L          (VPC3_ADR)( 0x04 )   /**<(004H) status register low */
   #define bVpc3RoStatus_H          (VPC3_ADR)( 0x05 )   /**<(005H) status register high */

   #define bVpc3WoIntAck_L          (VPC3_ADR)( 0x02 )   /**<(002H) Interrupt acknowledge low */
   #define bVpc3WoIntAck_H          (VPC3_ADR)( 0x03 )   /**<(003H) Interrupt acknowledge high */
   #define bVpc3WoIntMask_L         (VPC3_ADR)( 0x04 )   /**<(004H) Interrupt mask register low */
   #define bVpc3WoIntMask_H         (VPC3_ADR)( 0x05 )   /**<(005H) Interrupt mask register high */

   #define bVpc3RwModeReg0_L        (VPC3_ADR)( 0x06 )   /**<(006H) mode register0 low */
   #define bVpc3RwModeReg0_H        (VPC3_ADR)( 0x07 )   /**<(007H) mode register0 high */

   #define bVpc3RoDinBufferSm          (VPC3_ADR)( 0x08 )   /**<(008H) buffer assignment of the DP_DIN_BUFFER state machine */
   #define bVpc3RoNewDinBufferCmd      (VPC3_ADR)( 0x09 )   /**<(009H) the user makes a new DP_DIN_BUFFER available in the N state */
   #define bVpc3RoDoutBufferSm         (VPC3_ADR)( 0x0A )   /**<(00AH) buffer assignment of the DP_DOUT_BUFFER state machine */
   #define bVpc3RoNextDoutBufferCmd    (VPC3_ADR)( 0x0B )   /**<(00BH) the user fetches the last DP_DOUT_BUFFER from the N state */
   #define bVpc3RoDiagBufferSm         (VPC3_ADR)( 0x0C )   /**<(00CH) buffer assignment for DP_DIAG_BUFFER state machine */
   #define bVpc3RoNewDiagBufferCmd     (VPC3_ADR)( 0x0D )   /**<(00DH) the user makes a new DP_DIAG_BUFFER available to the VPC3+ */
   #define bVpc3RoUserPrmDataOkay      (VPC3_ADR)( 0x0E )   /**<(00EH) positive acknowledge for received user parameter data */
   #define bVpc3RoUserPrmDataNotOkay   (VPC3_ADR)( 0x0F )   /**<(00FH) negative acknowledge for received user parameter data */
   #define bVpc3RoUserCfgDataOkay      (VPC3_ADR)( 0x10 )   /**<(010H) positive acknowledge for received config data */
   #define bVpc3RoUserCfgDataNotOkay   (VPC3_ADR)( 0x11 )   /**<(011H) negative acknowledge for received config data */
   #define bVpc3RoDxbOutBufferSm       (VPC3_ADR)( 0x12 )   /**<(012H) buffer assignment of the DXB_OUT_BUFFER state machine */
   #define bVpc3RoNextDxbOutBufferCmd  (VPC3_ADR)( 0x13 )   /**<(013H) the user fetches the last DXB_OUT_BUFFER */
   #define bVpc3RoSsaBufferFreeCmd     (VPC3_ADR)( 0x14 )   /**<(014H) the user has fetched data from ssa buffer and enables buffer again */
   #define bVpc3RoModeReg1             (VPC3_ADR)( 0x15 )   /**<(015H) current value of mode_reg1 */

   #define bVpc3WoModeReg1_S           (VPC3_ADR)( 0x08 )   /**<(008H) set b0..b7 in mode_reg1 */
   #define bVpc3WoModeReg1_R           (VPC3_ADR)( 0x09 )   /**<(009H) reset b0..b7 in mode_reg1 */
   #define bVpc3WoWdBaudControlVal     (VPC3_ADR)( 0x0A )   /**<(00AH) root value for baud rate monitoring */
   #define bVpc3WoMinTsdrVal           (VPC3_ADR)( 0x0B )   /**<(00BH) MinTsdr time */
   #define bVpc3WoModeReg2             (VPC3_ADR)( 0x0C )   /**<(00CH) set b0..b7 in mode_reg2 */
   #define bVpc3WoSyncPwReg            (VPC3_ADR)( 0x0D )   /**<(00DH) sync pulse width register */
   #define bVpc3WoControlCommandReg    (VPC3_ADR)( 0x0E )   /**<(00EH) Iso-Mode: control command value */
   #define bVpc3WoGroupSelectReg       (VPC3_ADR)( 0x0F )   /**<(00FH) Iso-Mode: group select value */
   #define bVpc3WoControlCommandMask   (VPC3_ADR)( 0x10 )   /**<(010H) Iso-Mode: control command mask register */
   #define bVpc3WoGroupSelectMask      (VPC3_ADR)( 0x11 )   /**<(011H) Iso-Mode: group select mask register */

   #define bVpc3WoModeReg3             (VPC3_ADR)( 0x12 )   /**<(012H) set b0..b7 in mode_reg3 */

   #define bVpc3RwTsAddr               (VPC3_ADR)( 0x16 )   /**<(016H) setup VPC3+ station address */
   #define bVpc3RwFdlSapListPtr        (VPC3_ADR)( 0x17 )   /**<(017H) pointer fdl_sap_list */
   #define bVpc3RwUserWdValue_L        (VPC3_ADR)( 0x18 )   /**<(018H) user watchdog value b0..b7 */
   #define bVpc3RwUserWdValue_H        (VPC3_ADR)( 0x19 )   /**<(019H) user watchdog value b8..b15 */
   #define bVpc3RwLenDoutBuf           (VPC3_ADR)( 0x1A )   /**<(01AH) length of dout buffers */
   #define bVpc3RwDoutBufPtr1          (VPC3_ADR)( 0x1B )   /**<(01BH) segment base address of dout_buffer [0] */
   #define bVpc3RwDoutBufPtr2          (VPC3_ADR)( 0x1C )   /**<(01CH) segment base address of dout_buffer [1] */
   #define bVpc3RwDoutBufPtr3          (VPC3_ADR)( 0x1D )   /**<(01DH) segment base address of dout_buffer [2] */
   #define bVpc3RwLenDinBuf            (VPC3_ADR)( 0x1E )   /**<(01EH) length of din buffers */
   #define bVpc3RwDinBufPtr1           (VPC3_ADR)( 0x1F )   /**<(01FH) segment base address of din_buffer [0] */
   #define bVpc3RwDinBufPtr2           (VPC3_ADR)( 0x20 )   /**<(020H) segment base address of din_buffer [1] */
   #define bVpc3RwDinBufPtr3           (VPC3_ADR)( 0x21 )   /**<(021H) segment base address of din_buffer [2] */
   #define bVpc3RwLenDxbOutBuf         (VPC3_ADR)( 0x22 )   /**<(022H) length of dxb buffers */
   #define bVpc3RwDxbOutBufPtr1        (VPC3_ADR)( 0x23 )   /**<(023H) segment base address of dxbout_buffer1 */
   #define bVpc3RwLenDiagBuf1          (VPC3_ADR)( 0x24 )   /**<(024H) length of diag buffers [0] */
   #define bVpc3RwLenDiagBuf2          (VPC3_ADR)( 0x25 )   /**<(025H) length of diag buffers [1] */
   #define bVpc3RwDiagBufPtr1          (VPC3_ADR)( 0x26 )   /**<(026H) segment base address of diag_buffer [0] */
   #define bVpc3RwDiagBufPtr2          (VPC3_ADR)( 0x27 )   /**<(027H) segment base address of diag_buffer [1] */
   #define bVpc3RwLenCtrlBuf1          (VPC3_ADR)( 0x28 )   /**<(028H) length of aux buffer 1 */
   #define bVpc3RwLenCtrlBuf2          (VPC3_ADR)( 0x29 )   /**<(029H) length of aux buffer 2 */
   #define bVpc3RwAuxBufSel            (VPC3_ADR)( 0x2A )   /**<(02AH) assignment for aux buffers 1/2 */
   #define bVpc3RwAuxBufPtr1           (VPC3_ADR)( 0x2B )   /**<(02BH) segment base address of aux buffer 1 */
   #define bVpc3RwAuxBufPtr2           (VPC3_ADR)( 0x2C )   /**<(02CH) segment base address of aux buffer 2 */
   #define bVpc3RwLenSsaBuf            (VPC3_ADR)( 0x2D )   /**<(02DH) length of SET_SLAVE_ADDRESS buffer */
   #define bVpc3RwSsaBufPtr            (VPC3_ADR)( 0x2E )   /**<(02EH) segment base address of SET_SLAVE_ADDRESS buffer */
   #define bVpc3RwLenPrmData           (VPC3_ADR)( 0x2F )   /**<(02FH) max. length of input data in SET_PRM buffer */
   #define bVpc3RwPrmBufPtr            (VPC3_ADR)( 0x30 )   /**<(030H) segment base address of SET_PRM buffer */
   #define bVpc3RwLenCfgData           (VPC3_ADR)( 0x31 )   /**<(031H) length of input data in the CHECK_CONFIG buffer */
   #define bVpc3RwCfgBufPtr            (VPC3_ADR)( 0x32 )   /**<(032H) segment base address of CHECK_CONFIG buffer */
   #define bVpc3RwLenReadCfgData       (VPC3_ADR)( 0x33 )   /**<(033H) length of input data in the GET_CONFIG buffer */
   #define bVpc3RwReadCfgBufPtr        (VPC3_ADR)( 0x34 )   /**<(034H) segment base address of GET_CONFIG buffer */
   #define bVpc3RwLenDxbLinkBuf        (VPC3_ADR)( 0x35 )   /**<(035H) length of dxb link table buffer */
   #define bVpc3RwDxbLinkBufPtr        (VPC3_ADR)( 0x36 )   /**<(036H) segment base address of dxb link table buffer */
   #define bVpc3RwLenDxbStatusBuf      (VPC3_ADR)( 0x37 )   /**<(037H) length of dxb link status buffer */
   #define bVpc3RwDxbStatusBufPtr      (VPC3_ADR)( 0x38 )   /**<(038H) segment base address of dxb link status buffer */
   #define bVpc3RwRealNoAddChange      (VPC3_ADR)( 0x39 )   /**<(039H) address changes */
   #define bVpc3RwIdentLow             (VPC3_ADR)( 0x3A )   /**<(03AH) IDENT_LOW */
   #define bVpc3RwIdentHigH            (VPC3_ADR)( 0x3B )   /**<(03BH) IDENT_HIGH */
   #define bVpc3RwGcCommand            (VPC3_ADR)( 0x3C )   /**<(03CH) last global control command */
   #define bVpc3RwLenSpecPrmBuf        (VPC3_ADR)( 0x3D )   /**<(03DH) length of SPEC_PRM buffer */
   #define bVpc3RwDxbOutBufPtr2        (VPC3_ADR)( 0x3E )   /**<(03EH) segment base address of dxbout_buffer2 */
   #define bVpc3RwDxbOutBufPtr3        (VPC3_ADR)( 0x3F )   /**<(03FH) segment base address of dxbout_buffer3 */

   #define bVpc3RwStartSapCtrlList     (VPC3_ADR)( 0x40 )   /**<(040H) SAP CONTROL BLOCK LIST */

   typedef struct
   {
         TVpc3Byte bIntReqReg_L;                      /**<(000H) Interrupt request register low */
         TVpc3Byte bIntReqReg_H;                      /**<(001H) Interrupt request register high */

         union
         {
             struct
             {                                        /* [read] */
                 TVpc3Byte bIntReg_L;                 /**<(002H) Interrupt register low */
                 TVpc3Byte bIntReg_H;                 /**<(003H) Interrupt register high */
                 TVpc3Byte bStatus_L;                 /**<(004H) status register low */
                 TVpc3Byte bStatus_H;                 /**<(005H) status register high */
             } sRead;

             struct
             {                                        /* [write] */
                 TVpc3Byte bIntAck_L;                 /**<(002H) Interrupt acknowledge low */
                 TVpc3Byte bIntAck_H;                 /**<(003H) Interrupt acknowledge high */
                 TVpc3Byte bIntMask_L;                /**<(004H) Interrupt mask register low */
                 TVpc3Byte bIntMask_H;                /**<(005H) Interrupt mask register high */
             } sWrite;
         } sReg;

         TVpc3Byte bModeReg0_L;                       /**<(006H) mode register0 low */
         TVpc3Byte bModeReg0_H;                       /**<(007H) mode register0 high */

         union
         {
             struct
             {                                            /* [read] */
                 TVpc3Byte bDinBufferSm;                   /**<(008H) buffer assignment of the DP_DIN_BUFFER state machine */
                 TVpc3Byte bNewDinBufferCmd;               /**<(009H) the user makes a new DP_DIN_BUFFER available in the N state */
                 TVpc3Byte bDoutBufferSm;                  /**<(00AH) buffer assignment of the DP_DOUT_BUFFER state machine */
                 TVpc3Byte bNextDoutBufferCmd;             /**<(00BH) the user fetches the last DP_DOUT_BUFFER from the N state */
                 TVpc3Byte bDiagBufferSm;                  /**<(00CH) buffer assignment for DP_DIAG_BUFFER state machine */
                 TVpc3Byte bNewDiagBufferCmd;              /**<(00DH) the user makes a new DP_DIAG_BUFFER available to the VPC3+ */
                 TVpc3Byte bUserPrmDataOkay;               /**<(00EH) positive acknowledge for received user parameter data */
                 TVpc3Byte bUserPrmDataNotOkay;            /**<(00FH) negative acknowledge for received user parameter data */
                 TVpc3Byte bUserCfgDataOkay;               /**<(010H) positive acknowledge for received config data */
                 TVpc3Byte bUserCfgDataNotOkay;            /**<(011H) negative acknowledge for received config data */
                 TVpc3Byte bDxbOutBufferSm;                /**<(012H) buffer assignment of the DXB_OUT_BUFFER state machine */
                 TVpc3Byte bNextDxbOutBufferCmd;           /**<(013H) the user fetches the last DXB_OUT_BUFFER */
                 TVpc3Byte bSsaBufferFreeCmd;              /**<(014H) the user has fetched data from ssa buffer and enables buffer again */
                 TVpc3Byte bModeReg1;                      /**<(015H) current value of mode_reg1 */
             } sRead;

             struct
             {                                            /* [write] */
                 TVpc3Byte bModeReg1_S;                    /**<(008H) set b0..b7 in mode_reg1 */
                 TVpc3Byte bModeReg1_R;                    /**<(009H) reset b0..b7 in mode_reg1 */
                 TVpc3Byte bWdBaudControlVal;              /**<(00AH) root value for baud rate monitoring */
                 TVpc3Byte bMinTsdrVal;                    /**<(00BH) MinTsdr time */
                 TVpc3Byte bModeReg2;                      /**<(00CH) set b0..b7 in mode_reg2 */
                 TVpc3Byte bSyncPwReg;                     /**<(00DH) sync pulse width register */
                 TVpc3Byte bControlCommandReg;             /**<(00EH) Iso-Mode: control command value */
                 TVpc3Byte bGroupSelectReg;                /**<(00FH) Iso-Mode: group select value */
                 TVpc3Byte bControlCommandMask;            /**<(010H) Iso-Mode: control command mask register */
                 TVpc3Byte bGroupSelectMask;               /**<(011H) Iso-Mode: group select mask register */
                 TVpc3Byte bModeReg3;                      /**<(012H) set b0..b7 in mode_reg3 */
                 TVpc3Byte breserved_13;                   /**<(013H)  */
                 TVpc3Byte breserved_14;                   /**<(014H)  */
                 TVpc3Byte breserved_15;                   /**<(015H)  */
             } sWrite;
         } sCtrlPrm;

         TVpc3Byte bTsAddr;                              /**<(016H) setup VPC3+ station address */
         TVpc3Byte bFdlSapListPtr;                       /**<(017H) pointer fdl_sap_list */
         TVpc3Byte abUserWdValue[2];                     /**<(018H) user watchdog value b0..b7 */
                                                      /**<(019H) user watchdog value b8..b15 */
         TVpc3Byte bLenDoutBuf;                          /**<(01AH) length of dout buffers */
         TVpc3Byte abDoutBufPtr[3];                      /**<(01BH) segment base address of dout_buffer [0] */
                                                      /**<(01CH) segment base address of dout_buffer [1] */
                                                      /**<(01DH) segment base address of dout_buffer [2] */
         TVpc3Byte bLenDinBuf;                           /**<(01EH) length of din buffers */
         TVpc3Byte abDinBufPtr[3];                       /**<(01FH) segment base address of din_buffer [0] */
                                                      /**<(020H) segment base address of din_buffer [1] */
                                                      /**<(021H) segment base address of din_buffer [2] */
         TVpc3Byte bLenDxbOutBuf;                        /**<(022H) length of dxb buffers */
         TVpc3Byte bDxbOutBufPtr1;                       /**<(023H) segment base address of dxbout_buffer1 */
         TVpc3Byte abLenDiagBuf[2];                      /**<(024H) length of diag buffers [0] */
                                                      /**<(025H) length of diag buffers [1] */
         TVpc3Byte abDiagBufPtr[2];                      /**<(026H) segment base address of diag_buffer [0] */
                                                      /**<(027H) segment base address of diag_buffer [1] */
         TVpc3Byte abLenCtrlBuf[2];                      /**<(028H) length of aux buffer 1 */
                                                      /**<(029H) length of aux buffer 2 */
         TVpc3Byte bAuxBufSel;                           /**<(02AH) assignment for aux buffers 1/2 */
         TVpc3Byte abAuxBufPtr[2];                       /**<(02BH) segment base address of aux buffer 1 */
                                                      /**<(02CH) segment base address of aux buffer 2 */
         TVpc3Byte bLenSsaBuf;                           /**<(02DH) length of SET_SLAVE_ADDRESS buffer */
         TVpc3Byte bSsaBufPtr;                           /**<(02EH) segment base address of SET_SLAVE_ADDRESS buffer */
         TVpc3Byte bLenPrmData;                          /**<(02FH) max. length of input data in SET_PRM buffer */
         TVpc3Byte bPrmBufPtr;                           /**<(030H) segment base address of SET_PRM buffer */
         TVpc3Byte bLenCfgData;                          /**<(031H) length of input data in the CHECK_CONFIG buffer */
         TVpc3Byte bCfgBufPtr;                           /**<(032H) segment base address of CHECK_CONFIG buffer */
         TVpc3Byte bLenReadCfgData;                      /**<(033H) length of input data in the GET_CONFIG buffer */
         TVpc3Byte bReadCfgBufPtr;                       /**<(034H) segment base address of GET_CONFIG buffer */
         TVpc3Byte bLenDxbLinkBuf;                       /**<(035H) length of dxb link table buffer */
         TVpc3Byte bDxbLinkBufPtr;                       /**<(036H) segment base address of dxb link table buffer */
         TVpc3Byte bLenDxbStatusBuf;                     /**<(037H) length of dxb link status buffer */
         TVpc3Byte bDxbStatusBufPtr;                     /**<(038H) segment base address of dxb link status buffer */
         TVpc3Byte bRealNoAddChange;                     /**<(039H) address changes */
         TVpc3Byte bIdentLow;                            /**<(03AH) IDENT_LOW */
         TVpc3Byte bIdentHigh;                           /**<(03BH) IDENT_HIGH */
         TVpc3Byte bGcCommand;                           /**<(03CH) last global control command */
         TVpc3Byte bLenSpecPrmBuf;                       /**<(03DH) length of SPEC_PRM buffer */
         TVpc3Byte bDxbOutBufPtr2;                       /**<(03EH) segment base address of dxbout_buffer2 */
         TVpc3Byte bDxbOutBufPtr3;                       /**<(03FH) segment base address of dxbout_buffer3 */

         TVpc3Byte abSapCtrlList[SAP_LENGTH];            /**<(040H) SAP CONTROL BLOCK LIST */
   } VPC3_STRUC;

   #define VPC3_STRUC_PTR  VPC3_STRUC *

/*---------------------------------------------------------------------------*/
/* 1.2 literals for interrupt register                                       */
/*---------------------------------------------------------------------------*/
/*  low Byte */
#define VPC3_INT_MAC_RESET             ((uint8_t)0x01)      /**<VPC3 has entered offline state */
#define VPC3_INT_CLOCK_SYNC            ((uint8_t)0x01)      /**<VPC3 received clock sync telegram */
#define VPC3_INT_GO_LEAVE_DATA_EX      ((uint8_t)0x02)      /**<VPC3 has entered or left DATA_EX */
#define VPC3_INT_BAUDRATE_DETECT       ((uint8_t)0x04)      /**<VPC3 has detected Baudrate */
#define VPC3_INT_WD_DP_MODE_TIMEOUT    ((uint8_t)0x08)      /**<DP watchdog timeout */
#define VPC3_INT_USER_TIMER_CLOCK      ((uint8_t)0x10)      /**<time base for USER_TIMER_CLOCKS has run out */
#define VPC3_INT_DXB_LINK_ERROR        ((uint8_t)0x20)      /**<subscriber link error */
#define VPC3_INT_NEW_EXT_PRM_DATA      ((uint8_t)0x40)      /**<New EXT_PRM_DATA received */
#define VPC3_INT_DXB_OUT               ((uint8_t)0x80)      /**<NEW subscriber data received */

/*  high Byte */
#define VPC3_INT_NEW_GC_COMMAND        ((uint8_t)0x01)      /**<New Global control command received */
#define VPC3_INT_NEW_SSA_DATA          ((uint8_t)0x02)      /**<New SSA_DATA received */
#define VPC3_INT_NEW_CFG_DATA          ((uint8_t)0x04)      /**<New check_config received */
#define VPC3_INT_NEW_PRM_DATA          ((uint8_t)0x08)      /**<New prm_data received */
#define VPC3_INT_DIAG_BUFFER_CHANGED   ((uint8_t)0x10)      /**<diag_buffer has been changed */
#define VPC3_INT_DX_OUT                ((uint8_t)0x20)      /**<New DOut_data received */
#define VPC3_INT_POLL_END_IND          ((uint8_t)0x40)      /**<DPV1 */
#define VPC3_INT_FDL_IND               ((uint8_t)0x80)      /**<DPV1 */

/* -- interrupt events for 16bit register */
#define VPC3_INT16_MAC_RESET           ((uint16_t)0x0001)   /**<VPC3 has entered offline state */
#define VPC3_INT16_CLOCK_SYNC          ((uint16_t)0x0001)   /**<VPC3 received clock sync telegram */
#define VPC3_INT16_GO_LEAVE_DATA_EX    ((uint16_t)0x0002)   /**<VPC3 has entered or left DATA_EX */
#define VPC3_INT16_BAUDRATE_DETECT     ((uint16_t)0x0004)   /**<VPC3 has detected Baudrate */
#define VPC3_INT16_WD_DP_MODE_TIMEOUT  ((uint16_t)0x0008)   /**<DP watchdog timeout */
#define VPC3_INT16_USER_TIMER_CLOCK    ((uint16_t)0x0010)   /**<time base for USER_TIMER_CLOCKS has run out */
#define VPC3_INT16_DXB_LINK_ERROR      ((uint16_t)0x0020)   /**<subscriber link error */
#define VPC3_INT16_NEW_EXT_PRM_DATA    ((uint16_t)0x0040)   /**<New EXT_PRM_DATA received */
#define VPC3_INT16_DXB_OUT             ((uint16_t)0x0080)   /**<NEW subscriber data received */
#define VPC3_INT16_NEW_GC_COMMAND      ((uint16_t)0x0100)   /**<New Global control command received */
#define VPC3_INT16_NEW_SSA_DATA        ((uint16_t)0x0200)   /**<New SSA_DATA received */
#define VPC3_INT16_NEW_CFG_DATA        ((uint16_t)0x0400)   /**<New check_config received */
#define VPC3_INT16_NEW_PRM_DATA        ((uint16_t)0x0800)   /**<New prm_data received */
#define VPC3_INT16_DIAG_BUFFER_CHANGED ((uint16_t)0x1000)   /**<diag_buffer has been changed */
#define VPC3_INT16_DX_OUT              ((uint16_t)0x2000)   /**<New DOut_data received */
#define VPC3_INT16_POLL_END_IND        ((uint16_t)0x4000)   /**<DPV1 */
#define VPC3_INT16_FDL_IND             ((uint16_t)0x8000)   /**<DPV1 */

/* -- events for interrupt event handler -------------------------------------- */
#define VPC3_EV_CLOCK_SYNCHRONISATION  ((uint16_t)0x0001)
#define VPC3_EV_MAC_RESET              ((uint16_t)0x0001)
#define VPC3_EV_GO_LEAVE_DATA_EX       ((uint16_t)0x0002)
#define VPC3_EV_BAUDRATE_DETECT        ((uint16_t)0x0004)
#define VPC3_EV_WD_DP_TIMEOUT          ((uint16_t)0x0008)
#define VPC3_EV_USER_TIMER_CLOCK       ((uint16_t)0x0010)
#define VPC3_EV_DXB_LINK_ERROR         ((uint16_t)0x0020)
#define VPC3_EV_NEW_EXT_PRM_DATA       ((uint16_t)0x0040)
#define VPC3_EV_DXB_OUT                ((uint16_t)0x0080)
#define VPC3_EV_NEW_GC_COMMAND         ((uint16_t)0x0100)
#define VPC3_EV_NEW_SSA_DATA           ((uint16_t)0x0200)
#define VPC3_EV_NEW_CFG_DATA           ((uint16_t)0x0400)
#define VPC3_EV_NEW_PRM_DATA           ((uint16_t)0x0800)
#define VPC3_EV_DIAG_BUF_CHANGED       ((uint16_t)0x1000)
#define VPC3_EV_DX_OUT                 ((uint16_t)0x2000)
#define VPC3_EV_RESERVED               ((uint16_t)0x4000)
#define VPC3_EV_NEW_INPUT_DATA         ((uint16_t)0x8000)

/*---------------------------------------------------------------------------*/
/* 1.3 literals for status register                                          */
/*---------------------------------------------------------------------------*/
#define VPC3_PASS_IDLE                 ((uint8_t)0x01)
#define VPC3_DIAG_FLAG                 ((uint8_t)0x04)
#define MASK_DP_STATE                  ((uint8_t)0x30)
#define MASK_WD_STATE                  ((uint8_t)0xC0)
#define WAIT_PRM                       ((uint8_t)0x00)
#define WAIT_CFG                       ((uint8_t)0x10)
#define DATA_EX                        ((uint8_t)0x20)
#define DP_ERROR                       ((uint8_t)0x30)
#define BAUD_SEARCH                    ((uint8_t)0x00)
#define BAUD_CONTROL                   ((uint8_t)0x40)
#define DP_MODE                        ((uint8_t)0x80)
#define WD_ERROR                       ((uint8_t)0xC0)

#define BAUDRATE_MASK                  ((uint8_t)0x0F)
#define BAUDRATE_12MBAUD               ((uint8_t)0x00)
#define BAUDRATE_6MBAUD                ((uint8_t)0x01)
#define BAUDRATE_3MBAUD                ((uint8_t)0x02)
#define BAUDRATE_1_5MBAUD              ((uint8_t)0x03)
#define BAUDRATE_500KBAUD              ((uint8_t)0x04)
#define BAUDRATE_187_50KBAUD           ((uint8_t)0x05)
#define BAUDRATE_93_75KBAUD            ((uint8_t)0x06)
#define BAUDRATE_45_45KBAUD            ((uint8_t)0x07)
#define BAUDRATE_19_20KBAUD            ((uint8_t)0x08)
#define BAUDRATE_9_60KBAUD             ((uint8_t)0x09)
#define BAUDRATE_AFTER_RESET           ((uint8_t)0xFF)

#define AT_MASK                        ((uint8_t)0xF0)
#define AT_VPC3                        ((uint8_t)0x00)
#define AT_VPC3B                       ((uint8_t)0xB0)
#define AT_VPC3C                       ((uint8_t)0xC0)
#define AT_MPI12X                      ((uint8_t)0xD0)
#define AT_VPC3S                       ((uint8_t)0xE0)

/*---------------------------------------------------------------------------*/
/* 1.4 literals for mode register 0                                          */
/*---------------------------------------------------------------------------*/
/* low Byte */
#define VPC3_DIS_START_CTRL            ((uint8_t)0x01)
#define VPC3_DIS_STOP_CTRL             ((uint8_t)0x02)
#define VPC3_FDL_DBB                   ((uint8_t)0x04)
#define VPC3_MINTSDR                   ((uint8_t)0x08)
#define VPC3_INT_POL_HIGH              ((uint8_t)0x10)
#define VPC3_EARLY_RDY                 ((uint8_t)0x20)
#define VPC3_SYNC_SUPP                 ((uint8_t)0x40)
#define VPC3_FREEZE_SUPP               ((uint8_t)0x80)

/* high Byte */
#define VPC3_DP_MODE                   ((uint8_t)0x01)
#define VPC3_EOI_TBASE_1ms             ((uint8_t)0x02)
#define VPC3_USR_TBASE_10ms            ((uint8_t)0x04)
#define VPC3_WD_TEST                   ((uint8_t)0x08)
#define VPC3_SPEC_PRM_BUF_MODE         ((uint8_t)0x10)
#define VPC3_SPEC_CLR_MODE             ((uint8_t)0x20)

/*---------------------------------------------------------------------------*/
/* 1.5 literals for mode register 1                                          */
/*---------------------------------------------------------------------------*/
#define VPC3_START                     ((uint8_t)0x01)
#define VPC3_EOI                       ((uint8_t)0x02)
#define VPC3_GO_OFFLINE                ((uint8_t)0x04)
#define VPC3_USER_LEAVE_MASTER         ((uint8_t)0x08)
#define VPC3_EN_CHG_CFG_BUFFER         ((uint8_t)0x10)
#define VPC3_RES_USER_WD               ((uint8_t)0x20)
#define VPC3_EN_CHK_SSAP               ((uint8_t)0x80)

/*---------------------------------------------------------------------------*/
/* 1.7 basic macros for VPC3+                                                */
/*---------------------------------------------------------------------------*/

   /* Startaddress of User-Area in VPC3-Format ----------------------------------- */
   #define VPC3_DP_BUF_START                          (uint8_t)( sizeof( VPC3_STRUC ) >> SEG_MULDIV)
   /* Startaddress of FDL-List-Area in VPC3-Format ------------------------------- */
   #define VPC3_SAP_CTRL_LIST_START                   (uint8_t)(0x40 >> SEG_MULDIV)
   #define VPC3_SET_EMPTY_SAP_LIST()                  Vpc3Write( bVpc3RwStartSapCtrlList, 0xFF )
   #define VPC3_SET_SAP_LIST_END(OFFSET)              Vpc3Write( bVpc3RwStartSapCtrlList + OFFSET, 0xFF )
   /* set HW-Mode ---------------------------------------------------------------- */
   #define VPC3_SET_HW_MODE_HIGH(MODE_HIGH)           Vpc3Write( bVpc3RwModeReg0_H, MODE_HIGH )
   #define VPC3_SET_HW_MODE_LOW(MODE_LOW)             Vpc3Write( bVpc3RwModeReg0_L, MODE_LOW )
   /* Get HW-Mode ---------------------------------------------------------------- */
   #define VPC3_GET_HW_MODE_HIGH()                    Vpc3Read( bVpc3RwModeReg0_H )
   #define VPC3_GET_HW_MODE_LOW()                     Vpc3Read( bVpc3RwModeReg0_L )
   /* set interrupt indications -------------------------------------------------- */
   #define VPC3_SET_INT_REQ_REG_L(ISR_EVENT)          Vpc3Write( bVpc3RwIntReqReg_L, ISR_EVENT )
   #define VPC3_SET_INT_REQ_REG_H(ISR_EVENT)          Vpc3Write( bVpc3RwIntReqReg_H, ISR_EVENT )
   #define VPC3_SET_INDICATIONS_HIGH(IND_HIGH)        Vpc3Write( bVpc3WoIntMask_H, IND_HIGH )
   #define VPC3_SET_INDICATIONS_LOW(IND_LOW)          Vpc3Write( bVpc3WoIntMask_L, IND_LOW )
   #define VPC3_SET_INT_ACK_REG_L(ACK_EVENT)          Vpc3Write( bVpc3WoIntAck_L, ACK_EVENT )
   #define VPC3_SET_INT_ACK_REG_H(ACK_EVENT)          Vpc3Write( bVpc3WoIntAck_H, ACK_EVENT )
   /* -- Set Slave-Adresse ------------------------------------------------------- */
   #define VPC3_SET_STATION_ADDRESS(ADDRESS)          Vpc3Write( bVpc3RwTsAddr, ADDRESS )
   #define VPC3_GET_STATION_ADDRESS()                 Vpc3Read( bVpc3RwTsAddr )
   /* -- Set min_TSDR ------------------------------------------------------------ */
   #define VPC3_SET_MINTSDR(MINTSDR)                  Vpc3Write( bVpc3WoMinTsdrVal, MINTSDR )
   /* -- Set Baud Control -------------------------------------------------------- */
   #define VPC3_SET_BAUD_CNTRL(VALUE)                 Vpc3Write( bVpc3WoWdBaudControlVal, VALUE )
   /* -- Ident-Nummer ------------------------------------------------------------- */
   /* -- Set Ident-Number --------------------------------------------------------- */
   #define VPC3_SET_IDENT_NUMBER_LOW(NR)              Vpc3Write( bVpc3RwIdentLow, NR )
   #define VPC3_SET_IDENT_NUMBER_HIGH(NR)             Vpc3Write( bVpc3RwIdentHigh, NR )
   /* -- address change service -------------------------------------------------- */
   #define VPC3_SET_ADD_CHG_DISABLE()                 Vpc3Write( bVpc3RwRealNoAddChange, 0xFF )
   #define VPC3_SET_ADD_CHG_ENABLE()                  Vpc3Write( bVpc3RwRealNoAddChange, 0x00 )
   /* -- start VPC3 --------------------------------------------------------------- */
   #define VPC3_Start__()                             Vpc3Write( bVpc3WoModeReg1_S, VPC3_START )
   #define VPC3_ResetStart()                          Vpc3Write( bVpc3WoModeReg1_R, VPC3_START )
   #define VPC3_SET_RESET_ALL(VALUE)                  Vpc3Write( bVpc3WoModeReg1_R, VALUE )
   /* -- set VPC3 offline --------------------------------------------------------- */
   #define VPC3_GoOffline()                           Vpc3Write( bVpc3WoModeReg1_S, VPC3_GO_OFFLINE )
   /* -- read/write Mode register ------------------------------------------------ */
   #define VPC3_GET_MODE_REG_1()                      Vpc3Read( bVpc3RoModeReg1 )
   #define VPC3_SET_MODE_REG_2(bValue)                Vpc3Write( bVpc3WoModeReg2, bValue )
   #define VPC3_SET_MODE_REG_3(bValue)                Vpc3Write( bVpc3WoModeReg3, bValue )
   /* -- read Status register ---------------------------------------------------- */
   #define VPC3_GET_STATUS_L()                        Vpc3Read( bVpc3RoStatus_L )
   #define VPC3_GET_STATUS_H()                        Vpc3Read( bVpc3RoStatus_H )
   /* -- read State of DP-State Machine ------------------------------------------ */
   #define VPC3_GET_DP_STATE()                        ( Vpc3Read( bVpc3RoStatus_L ) & MASK_DP_STATE )
   /* -- read WD-State Machine --------------------------------------------------- */
   #define VPC3_GET_WD_STATE()                        ( Vpc3Read( bVpc3RoStatus_L ) & MASK_WD_STATE )
   /* -- read Baud-Rate ---------------------------------------------------------- */
   #define VPC3_GET_BAUDRATE()                        ( Vpc3Read( bVpc3RoStatus_H ) & BAUDRATE_MASK )
   /* -- read ASIC-Type ---------------------------------------------------------- */
   #define VPC3_GET_ASIC_TYPE()                       ( Vpc3Read( bVpc3RoStatus_H ) & AT_MASK )
   /* -- read VPC3-Offline/Passiv-Idle-State ------------------------------------- */
   #define VPC3_GET_OFF_PASS()                        ( Vpc3Read( bVpc3RoStatus_L ) & VPC3_PASS_IDLE )
   /* -- set DP StateMachine to WAIT_PRM ----------------------------------------- */
   #define VPC3_SET_USER_LEAVE_MASTER()               Vpc3Write( bVpc3WoModeReg1_S, VPC3_USER_LEAVE_MASTER )
   /* -- User-Watchdog ----------------------------------------------------------- */
   #define VPC3_SET_USER_WD_VALUE_HIGH(VALUE_HIGH)    Vpc3Write( bVpc3RwUserWdValue_H, VALUE_HIGH )
   #define VPC3_SET_USER_WD_VALUE_LOW(VALUE_LOW)      Vpc3Write( bVpc3RwUserWdValue_L, VALUE_LOW )
   /* -- Reset User-Watchdog ----------------------------------------------------- */
   #define VPC3_RESET_USER_WD()                       Vpc3Write( bVpc3WoModeReg1_S, VPC3_RES_USER_WD )
   /* -- VPC3_SET_EN_CHK_SSAP ---------------------------------------------------- */
   #define VPC3_SET_EN_CHK_SSAP()                     Vpc3Write( bVpc3WoModeReg1_S, VPC3_EN_CHK_SSAP )
   #define VPC3_RESET_EN_CHK_SSAP()                   Vpc3Write( bVpc3WoModeReg1_R, VPC3_EN_CHK_SSAP )
   /* -- Set fdl sap-list pointer ------------------------------------------------ */
   #define VPC3_SET_FDL_SAP_LIST_PTR()                Vpc3Write( bVpc3RwFdlSapListPtr, VPC3_SAP_LIST_PTR )
   #define VPC3_GET_SAP_LIST_PTR()                    (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)(0x40)) + ((VPC3_ADR)Vpc3AsicAddress ))

   #define VPC3_GET_DOUT_BUFFER_SM()                  ( Vpc3Read( bVpc3RoDoutBufferSm ) & 0x30 )
   #define VPC3_GET_DIN_BUFFER_SM()                   ( Vpc3Read( bVpc3RoDinBufferSm ) & 0x30 )
   #define VPC3_GET_NEXT_DOUT_BUFFER_CMD()            ( Vpc3Read( bVpc3RoNextDoutBufferCmd ) & ( VPC3_NEW_DOUT_BUF | VPC3_DOUT_BUF_CLEARED ) )

   #define VPC3_GET_DIAG_BUFFER_SM()                  ( Vpc3Read( bVpc3RoDiagBufferSm ) )
   #define VPC3_GET_NEW_DIAG_BUFFER_CMD()             ( Vpc3Read( bVpc3RoNewDiagBufferCmd ) )
   #define VPC3_GET_DIAG1_LENGTH()                    ( Vpc3Read( bVpc3RwLenDiagBuf1 ) )
   #define VPC3_GET_DIAG2_LENGTH()                    ( Vpc3Read( bVpc3RwLenDiagBuf2 ) )
   #define VPC3_GET_DIAG1_PTR()                       (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)( Vpc3Read( bVpc3RwDiagBufPtr1 )) << SEG_MULDIV)+((VPC3_ADR)Vpc3AsicAddress))
   #define VPC3_GET_DIAG2_PTR()                       (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)( Vpc3Read( bVpc3RwDiagBufPtr2 )) << SEG_MULDIV)+((VPC3_ADR)Vpc3AsicAddress))

   #if DP_INTERRUPT_MASK_8BIT

      /* -- read out reasons for indications ( Vpc3Isr() ) -------------------------- */
      #define VPC3_GET_IND_MAC_RESET()                (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_MAC_RESET)
      #define VPC3_GET_IND_CLOCK_SYNC()               (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_CLOCK_SYNC)
      #define VPC3_GET_IND_GO_LEAVE_DATA_EX()         (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_GO_LEAVE_DATA_EX)
      #define VPC3_GET_IND_BAUDRATE_DETECT()          (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_BAUDRATE_DETECT)
      #define VPC3_GET_IND_WD_DP_MODE_TIMEOUT()       (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_WD_DP_MODE_TIMEOUT)
      #define VPC3_GET_IND_USER_TIMER_CLOCK()         (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_USER_TIMER_CLOCK)
      #define VPC3_GET_IND_DXB_LINK_ERROR()           (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_DXB_LINK_ERROR)
      #define VPC3_GET_IND_NEW_EXT_PRM_DATA()         (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_NEW_EXT_PRM_DATA)
      #define VPC3_GET_IND_DXB_OUT()                  (Vpc3Read( bVpc3RoIntReg_L ) & VPC3_INT_DXB_OUT)
      #define VPC3_GET_IND_NEW_GC_COMMAND()           (Vpc3Read( bVpc3RoIntReg_H ) & VPC3_INT_NEW_GC_COMMAND)
      #define VPC3_GET_IND_NEW_SSA_DATA()             (Vpc3Read( bVpc3RoIntReg_H ) & VPC3_INT_NEW_SSA_DATA)
      #define VPC3_GET_IND_NEW_CFG_DATA()             (Vpc3Read( bVpc3RoIntReg_H ) & VPC3_INT_NEW_CFG_DATA)
      #define VPC3_GET_IND_NEW_PRM_DATA()             (Vpc3Read( bVpc3RoIntReg_H ) & VPC3_INT_NEW_PRM_DATA)
      #define VPC3_GET_IND_DIAG_BUFFER_CHANGED()      (Vpc3Read( bVpc3RoIntReg_H ) & VPC3_INT_DIAG_BUFFER_CHANGED)
      #define VPC3_GET_IND_DX_OUT()                   (Vpc3Read( bVpc3RoIntReg_H ) & VPC3_INT_DX_OUT)
      #define VPC3_GET_IND_POLL_END_IND()             (Vpc3Read( bVpc3RoIntReg_H ) & VPC3_INT_POLL_END_IND)
      #define VPC3_GET_IND_FDL_IND()                  (Vpc3Read( bVpc3RoIntReg_H ) & VPC3_INT_FDL_IND)

      /* -- Acknowledging the indication ------------------------------------------- */
      #define VPC3_CON_IND_MAC_RESET()                Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_MAC_RESET )
      #define VPC3_CON_IND_CLOCK_SYNC()               Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_CLOCK_SYNC )
      #define VPC3_CON_IND_GO_LEAVE_DATA_EX()         Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_GO_LEAVE_DATA_EX )
      #define VPC3_CON_IND_BAUDRATE_DETECT()          Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_BAUDRATE_DETECT )
      #define VPC3_CON_IND_WD_DP_MODE_TIMEOUT()       Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_WD_DP_MODE_TIMEOUT )
      #define VPC3_CON_IND_USER_TIMER_CLOCK()         Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_USER_TIMER_CLOCK )
      #define VPC3_CON_IND_DXB_LINK_ERROR()           Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_DXB_LINK_ERROR )
      #define VPC3_CON_IND_NEW_EXT_PRM_DATA()         Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_NEW_EXT_PRM_DATA )
      #define VPC3_CON_IND_DXB_OUT()                  Vpc3Write( bVpc3WoIntAck_L, VPC3_INT_DXB_OUT )
      #define VPC3_CON_IND_NEW_GC_COMMAND()           Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_NEW_GC_COMMAND )
      #define VPC3_CON_IND_NEW_SSA_DATA()             Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_NEW_SSA_DATA )
      #define VPC3_CON_IND_NEW_CFG_DATA()
      #define VPC3_CON_IND_NEW_PRM_DATA()
      #define VPC3_POLL_CON_IND_DIAG_BUFFER_CHANGED() Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_DIAG_BUFFER_CHANGED )
      #define VPC3_CON_IND_DIAG_BUFFER_CHANGED()      Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_DIAG_BUFFER_CHANGED )
      #define VPC3_CON_IND_DX_OUT()                   Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_DX_OUT )
      #define VPC3_CON_IND_POLL_END_IND()             Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_POLL_END_IND )
      #define VPC3_CON_IND_FDL_IND()                  Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_FDL_IND )

      /* poll read out reasons for indications -------------------------------------- */
      #define VPC3_POLL_IND_MAC_RESET()               (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_MAC_RESET )
      #define VPC3_POLL_IND_CLOCK_SYNC()              (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_CLOCK_SYNC )
      #define VPC3_POLL_IND_BAUDRATE_DETECT()         (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_BAUDRATE_DETECT )
      #define VPC3_POLL_IND_USER_TIMER_CLOCK()        (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_USER_TIMER_CLOCK )
      #define VPC3_POLL_IND_DXB_LINK_ERROR()          (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_DXB_LINK_ERROR )
      #define VPC3_POLL_IND_GO_LEAVE_DATA_EX()        (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_GO_LEAVE_DATA_EX )
      #define VPC3_POLL_IND_WD_DP_MODE_TIMEOUT()      (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_WD_DP_MODE_TIMEOUT )
      #define VPC3_POLL_IND_NEW_EXT_PRM_DATA()        (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_NEW_EXT_PRM_DATA )
      #define VPC3_POLL_IND_DXB_OUT()                 (Vpc3Read( bVpc3RwIntReqReg_L ) & VPC3_INT_DXB_OUT )
      #define VPC3_POLL_IND_NEW_GC_COMMAND()          (Vpc3Read( bVpc3RwIntReqReg_H ) & VPC3_INT_NEW_GC_COMMAND )
      #define VPC3_POLL_IND_NEW_SSA_DATA()            (Vpc3Read( bVpc3RwIntReqReg_H ) & VPC3_INT_NEW_SSA_DATA )
      #define VPC3_POLL_IND_NEW_CFG_DATA()            (Vpc3Read( bVpc3RwIntReqReg_H ) & VPC3_INT_NEW_CFG_DATA )
      #define VPC3_POLL_IND_NEW_PRM_DATA()            (Vpc3Read( bVpc3RwIntReqReg_H ) & VPC3_INT_NEW_PRM_DATA )
      #define VPC3_POLL_IND_DIAG_BUFFER_CHANGED()     (Vpc3Read( bVpc3RwIntReqReg_H ) & VPC3_INT_DIAG_BUFFER_CHANGED )
      #define VPC3_POLL_IND_DX_OUT()                  (Vpc3Read( bVpc3RwIntReqReg_H ) & VPC3_INT_DX_OUT )
      #define VPC3_POLL_IND_POLL_END_IND()            (Vpc3Read( bVpc3RwIntReqReg_H ) & VPC3_INT_POLL_END_IND )
      #define VPC3_POLL_IND_FDL_IND()                 (Vpc3Read( bVpc3RwIntReqReg_H ) & VPC3_INT_FDL_IND )

   #else

      /* -- read out reasons for indications ( Vpc3Isr() ) -------------------------- */
      #define VPC3_GET_IND_MAC_RESET()                ( pDpSystem->wInterruptEvent & VPC3_INT16_MAC_RESET )
      #define VPC3_GET_IND_CLOCK_SYNC()               ( pDpSystem->wInterruptEvent & VPC3_INT16_CLOCK_SYNC )
      #define VPC3_GET_IND_GO_LEAVE_DATA_EX()         ( pDpSystem->wInterruptEvent & VPC3_INT16_GO_LEAVE_DATA_EX )
      #define VPC3_GET_IND_BAUDRATE_DETECT()          ( pDpSystem->wInterruptEvent & VPC3_INT16_BAUDRATE_DETECT )
      #define VPC3_GET_IND_WD_DP_MODE_TIMEOUT()       ( pDpSystem->wInterruptEvent & VPC3_INT16_WD_DP_MODE_TIMEOUT )
      #define VPC3_GET_IND_USER_TIMER_CLOCK()         ( pDpSystem->wInterruptEvent & VPC3_INT16_USER_TIMER_CLOCK )
      #define VPC3_GET_IND_DXB_LINK_ERROR()           ( pDpSystem->wInterruptEvent & VPC3_INT16_DXB_LINK_ERROR )
      #define VPC3_GET_IND_NEW_EXT_PRM_DATA()         ( pDpSystem->wInterruptEvent & VPC3_INT16_NEW_EXT_PRM_DATA )
      #define VPC3_GET_IND_DXB_OUT()                  ( pDpSystem->wInterruptEvent & VPC3_INT16_DXB_OUT )
      #define VPC3_GET_IND_NEW_GC_COMMAND()           ( pDpSystem->wInterruptEvent & VPC3_INT16_NEW_GC_COMMAND )
      #define VPC3_GET_IND_NEW_SSA_DATA()             ( pDpSystem->wInterruptEvent & VPC3_INT16_NEW_SSA_DATA )
      #define VPC3_GET_IND_NEW_CFG_DATA()             ( pDpSystem->wInterruptEvent & VPC3_INT16_NEW_CFG_DATA )
      #define VPC3_GET_IND_NEW_PRM_DATA()             ( pDpSystem->wInterruptEvent & VPC3_INT16_NEW_PRM_DATA )
      #define VPC3_GET_IND_DIAG_BUFFER_CHANGED()      ( pDpSystem->wInterruptEvent & VPC3_INT16_DIAG_BUFFER_CHANGED )
      #define VPC3_GET_IND_DX_OUT()                   ( pDpSystem->wInterruptEvent & VPC3_INT16_DX_OUT )
      #define VPC3_GET_IND_POLL_END_IND()             ( pDpSystem->wInterruptEvent & VPC3_INT16_POLL_END_IND )
      #define VPC3_GET_IND_FDL_IND()                  ( pDpSystem->wInterruptEvent & VPC3_INT16_FDL_IND )

      /* -- Acknowledging the indication ------------------------------------------- */
      #define VPC3_CON_IND_MAC_RESET()
      #define VPC3_CON_IND_CLOCK_SYNC()
      #define VPC3_CON_IND_GO_LEAVE_DATA_EX()
      #define VPC3_CON_IND_BAUDRATE_DETECT()
      #define VPC3_CON_IND_WD_DP_MODE_TIMEOUT()
      #define VPC3_CON_IND_USER_TIMER_CLOCK()
      #define VPC3_CON_IND_DXB_LINK_ERROR()
      #define VPC3_CON_IND_NEW_EXT_PRM_DATA()
      #define VPC3_CON_IND_DXB_OUT()
      #define VPC3_CON_IND_NEW_GC_COMMAND()
      #define VPC3_CON_IND_NEW_SSA_DATA()
      #define VPC3_POLL_CON_IND_NEW_CFG_DATA()        ( pDpSystem->wPollInterruptEvent &= ~VPC3_INT16_NEW_CFG_DATA )
      #define VPC3_POLL_CON_IND_NEW_PRM_DATA()        ( pDpSystem->wPollInterruptEvent &= ~VPC3_INT16_NEW_PRM_DATA )
      #define VPC3_CON_IND_NEW_CFG_DATA()             ( pDpSystem->wInterruptEvent &= ~VPC3_INT16_NEW_CFG_DATA )
      #define VPC3_CON_IND_NEW_PRM_DATA()             ( pDpSystem->wInterruptEvent &= ~VPC3_INT16_NEW_PRM_DATA )

      #define VPC3_POLL_CON_IND_DIAG_BUFFER_CHANGED()                                \
               do                                                                    \
               {                                                                     \
                  Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_DIAG_BUFFER_CHANGED );        \
                  pDpSystem->wPollInterruptEvent &= ~VPC3_INT16_DIAG_BUFFER_CHANGED; \
               }while(0)

      #define VPC3_CON_IND_DIAG_BUFFER_CHANGED()                                     \
               do                                                                    \
               {                                                                     \
                  Vpc3Write( bVpc3WoIntAck_H, VPC3_INT_DIAG_BUFFER_CHANGED );        \
                  pDpSystem->wInterruptEvent &= ~VPC3_INT16_DIAG_BUFFER_CHANGED;     \
               }while(0)

      #define VPC3_CON_IND_DX_OUT()
      #define VPC3_CON_IND_POLL_END_IND()
      #define VPC3_CON_IND_FDL_IND()

      /* poll read out reasons for indications -------------------------------------- */
      #define VPC3_POLL_IND_MAC_RESET()               ( pDpSystem->wPollInterruptEvent & VPC3_INT16_MAC_RESET )
      #define VPC3_POLL_IND_CLOCK_SYNC()              ( pDpSystem->wPollInterruptEvent & VPC3_INT16_CLOCK_SYNC )
      #define VPC3_POLL_IND_BAUDRATE_DETECT()         ( pDpSystem->wPollInterruptEvent & VPC3_INT16_BAUDRATE_DETECT )
      #define VPC3_POLL_IND_USER_TIMER_CLOCK()        ( pDpSystem->wPollInterruptEvent & VPC3_INT16_USER_TIMER_CLOCK )
      #define VPC3_POLL_IND_DXB_LINK_ERROR()          ( pDpSystem->wPollInterruptEvent & VPC3_INT16_DXB_LINK_ERROR )
      #define VPC3_POLL_IND_GO_LEAVE_DATA_EX()        ( pDpSystem->wPollInterruptEvent & VPC3_INT16_GO_LEAVE_DATA_EX )
      #define VPC3_POLL_IND_WD_DP_MODE_TIMEOUT()      ( pDpSystem->wPollInterruptEvent & VPC3_INT16_WD_DP_MODE_TIMEOUT )
      #define VPC3_POLL_IND_NEW_EXT_PRM_DATA()        ( pDpSystem->wPollInterruptEvent & VPC3_INT16_NEW_EXT_PRM_DATA )
      #define VPC3_POLL_IND_DXB_OUT()                 ( pDpSystem->wPollInterruptEvent & VPC3_INT16_DXB_OUT )
      #define VPC3_POLL_IND_NEW_GC_COMMAND()          ( pDpSystem->wPollInterruptEvent & VPC3_INT16_NEW_GC_COMMAND )
      #define VPC3_POLL_IND_NEW_SSA_DATA()            ( pDpSystem->wPollInterruptEvent & VPC3_INT16_NEW_SSA_DATA )
      #define VPC3_POLL_IND_NEW_CFG_DATA()            ( pDpSystem->wPollInterruptEvent & VPC3_INT16_NEW_CFG_DATA )
      #define VPC3_POLL_IND_NEW_PRM_DATA()            ( pDpSystem->wPollInterruptEvent & VPC3_INT16_NEW_PRM_DATA )
      #define VPC3_POLL_IND_DIAG_BUFFER_CHANGED()     ( pDpSystem->wPollInterruptEvent & VPC3_INT16_DIAG_BUFFER_CHANGED )
      #define VPC3_POLL_IND_DX_OUT()                  ( pDpSystem->wPollInterruptEvent & VPC3_INT16_DX_OUT )
      #define VPC3_POLL_IND_POLL_END_IND()            ( pDpSystem->wPollInterruptEvent & VPC3_INT16_POLL_END_IND )
      #define VPC3_POLL_IND_FDL_IND()                 ( pDpSystem->wPollInterruptEvent & VPC3_INT16_FDL_IND )

   #endif /* #if DP_INTERRUPT_MASK_8BIT */

   /* -- Ending the Indication --------------------------------------------------- */
   #define VPC3_SET_EOI() Vpc3Write( bVpc3WoModeReg1_S, VPC3_EOI )

   #define DP_LOCK_IND()                            \
            do                                      \
            {                                       \
               Vpc3Write( bVpc3WoIntMask_H, 0xFF ); \
               Vpc3Write( bVpc3WoIntMask_L, 0xFF ); \
            }while(0)


   #define DP_UNLOCK_IND()                                            \
            do                                                        \
            {                                                         \
               Vpc3Write( bVpc3WoIntMask_H, pDpSystem->bIntIndHigh ); \
               Vpc3Write( bVpc3WoIntMask_L, pDpSystem->bIntIndLow );  \
            }while(0)

/*-----------------------------------------------------------------------------------------------------------*/
/* 2.0 parameter telegram                                                                                    */
/*-----------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/* 2.1 structure of prm data                                                 */
/*---------------------------------------------------------------------------*/

/*
|--------------|------------|---------------|-----------------------|------------|-----------|------------------|-------------------|
|      7       |      6     |       5       |           4           |      3     |     2     |         1        |          0        |
|--------------|------------|---------------|-----------------------|------------|-----------|------------------|-------------------|
|   Lock_Req   | Unlock_Req |   Sync_Req    |       Freeze_Req      |    WD_On   |     0     |         0        |          0        |
|--------------|------------|---------------|-----------------------|------------|-----------|------------------|-------------------|
| WD_Fact_1 (1 bis 255)                                                                                                             |
|-----------------------------------------------------------------------------------------------------------------------------------|
| WD_Fact_2 (1 bis 255)                                                                                                             |
|-----------------------------------------------------------------------------------------------------------------------------------|
| Min. Station Delay Responder (min Tsdr)                                                                                           |
|-----------------------------------------------------------------------------------------------------------------------------------|
| Ident_Number (high)                                                                                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|
| Ident_Number (low)                                                                                                                |
|-----------------------------------------------------------------------------------------------------------------------------------|
| Group_Ident                                                                                                                       |
|--------------|------------|---------------|-----------------------|------------|-----------|------------------|-------------------|
| DPV1_Enable  | Fail_Safe  | Publisher_En  |       reserved        |  reserved  |  WD_Base  | Dis_Stop_Control | Dis_Start_Control |
|--------------|------------|---------------|-----------------------|------------|-----------|------------------|-------------------|
| En_Pull_Plug | En_Process | En_Diagnostic | En_Manufacturer_Spec. | En_Status  | En_Update |     reserved     | Chk_Cfg_Mode      |
|    Alarm     |    Alarm   |    Alarm      |         Alarm         |   Alarm    |   Alarm   |                  |                   |
|--------------|------------|---------------|-----------------------|------------|-----------|------------------|-------------------|
|   PrmCmd     |  reserved  |   reserved    |       IsoM_Req        | Prm_Struct |                 Alarm_Mode                       |
|--------------|------------|---------------|-----------------------|------------|-----------|------------------|-------------------|
|                                                            User_Prm_Data                                                          |
|-----------------------------------------------------------------------------------------------------------------------------------|
*/

typedef struct
{
   uint8_t bStationState;
   uint8_t bWdFactor1;
   uint8_t bWdFactor2;
   uint8_t bMinTsdr;
   uint8_t bIdentHigh;
   uint8_t bIdentLow;
   uint8_t bGroupIdent;
   uint8_t bDpv1Status1;
   uint8_t bDpv1Status2;
   uint8_t bDpv1Status3;

   uint8_t bUserPrmData;
} STRUC_PRM;
#define VPC3_STRUC_PRM_PTR  STRUC_PRM  VPC3_PTR
#define MEM_STRUC_PRM_PTR   STRUC_PRM  MEM_PTR

/*---------------------------------------------------------------------------*/
/* 2.5 general defines for prm data                                          */
/*---------------------------------------------------------------------------*/
#define PRM_LEN_NORM                      ((uint8_t)0x07)
#define PRM_LEN_DPV1                      ((uint8_t)0x0A)

/*---------------------------------------------------------------------------*/
/* 2.7 returncodes prm data                                                  */
/*---------------------------------------------------------------------------*/
#define VPC3_PRM_FINISHED                 ((uint8_t)0x00)
#define VPC3_PRM_CONFLICT                 ((uint8_t)0x01)
#define VPC3_PRM_NOT_ALLOWED              ((uint8_t)0x03)

/*---------------------------------------------------------------------------*/
/* 2.8 macros for prm data                                                   */
/*---------------------------------------------------------------------------*/
   /* read length of prm-Data -------------------------------------------------- */
   #define VPC3_GET_PRM_LEN()             Vpc3Read( bVpc3RwLenPrmData )
   /* get pointer to prm-buffer ------------------------------------------------ */
   #define VPC3_GET_PRM_BUF_PTR()         (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)( Vpc3Read   ( bVpc3RwPrmBufPtr )) << SEG_MULDIV))
   /* acknowledge prm-data ----------------------------------------------------- */
   #define VPC3_SET_PRM_DATA_NOT_OK()     Vpc3Read    ( bVpc3RoUserPrmDataNotOkay )
   #define VPC3_SET_PRM_DATA_OK()         Vpc3Read    ( bVpc3RoUserPrmDataOkay )

   /* read length of aux buffer 1 ---------------------------------------------- */
   #define VPC3_GET_AUX_BUF1_LEN()        Vpc3Read( bVpc3RwLenCtrlBuf1 )
   /* get pointer to aux buffer 1 ---------------------------------------------- */
   #define VPC3_GET_AUX_BUF1_PTR()        (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)( Vpc3Read   ( bVpc3RwAuxBufPtr1 )) << SEG_MULDIV))

/*---------------------------------------------------------------------------*/
/* 3.3 literals for cfg-bytes                                                */
/*---------------------------------------------------------------------------*/
#define VPC3_CFG_IS_BYTE_FORMAT           ((uint8_t)0x30)
#define VPC3_CFG_BF_LENGTH                ((uint8_t)0x0f)
#define VPC3_CFG_LENGTH_IS_WORD_FORMAT    ((uint8_t)0x40)
#define VPC3_CFG_BF_INP_EXIST             ((uint8_t)0x10)
#define VPC3_CFG_BF_OUTP_EXIST            ((uint8_t)0x20)
#define VPC3_CFG_SF_OUTP_EXIST            ((uint8_t)0x80)
#define VPC3_CFG_SF_INP_EXIST             ((uint8_t)0x40)
#define VPC3_CFG_SF_LENGTH                ((uint8_t)0x3f)

/*---------------------------------------------------------------------------*/
/* 3.4 returncodes cfg data                                                  */
/*---------------------------------------------------------------------------*/
#define VPC3_CFG_FINISHED                 ((uint8_t)0x00)
#define VPC3_CFG_CONFLICT                 ((uint8_t)0x01)
#define VPC3_CFG_NOT_ALLOWED              ((uint8_t)0x03)

/*---------------------------------------------------------------------------*/
/* 3.6 macros for cfg data                                                   */
/*---------------------------------------------------------------------------*/
   /* the Config-Buffer must be exchanged for the Read_Config-Buffer */
   #define VPC3_UPDATE_CFG_BUFFER()          Vpc3Write( bVpc3WoModeReg1_S, VPC3_EN_CHG_CFG_BUFFER )
   /* read length of cfg-buffers ---------------------------------------------- */
   #define VPC3_GET_READ_CFG_LEN()           Vpc3Read( bVpc3RwLenReadCfgData )
   #define VPC3_GET_CFG_LEN()                Vpc3Read( bVpc3RwLenCfgData )
   /* set length of cfg-data -------------------------------------------------- */
   #define VPC3_SET_READ_CFG_LEN(LEN)        Vpc3Write( bVpc3RwLenReadCfgData, LEN )
   /* get pointer to cfg-buffers ---------------------------------------------- */
   #define VPC3_GET_READ_CFG_BUF_PTR()       (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)( Vpc3Read   ( bVpc3RwReadCfgBufPtr )) << SEG_MULDIV ))
   #define VPC3_GET_CFG_BUF_PTR()            (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)( Vpc3Read   ( bVpc3RwCfgBufPtr     )) << SEG_MULDIV ))
   /* acknowledge prm-data ----------------------------------------------------- */
   #define VPC3_SET_CFG_DATA_NOT_OK()        Vpc3Read( bVpc3RoUserCfgDataNotOkay )
   #define VPC3_SET_CFG_DATA_OK()            Vpc3Read( bVpc3RoUserCfgDataOkay )

/*---------------------------------------------------------------------------*/
/* 3.7 structure for real cfg data                                           */
/*---------------------------------------------------------------------------*/
/* -- cfg structure ----------------------------------------------------------- */
/*!
  \brief Structure for configuration data.
*/
typedef struct
{
   uint8_t bLength;                  /*!< length of configuration data */
   uint8_t abData[ CFG_BUFSIZE ];    /*!< array of configuration data  */
} CFG_STRUCT;
#define psCFG CFG_STRUCT MEM_PTR

/*---------------------------------------------------------------------------*/
/* 4.2 states for output buffer                                              */
/*---------------------------------------------------------------------------*/
#define VPC3_NEW_DOUT_BUF                 ((uint8_t)0x04)
#define VPC3_DOUT_BUF_CLEARED             ((uint8_t)0x08)

/*---------------------------------------------------------------------------*/
/* 4.3 macros for input buffer                                               */
/*---------------------------------------------------------------------------*/
#define VPC3_INPUT_UPDATE()         Vpc3Read( bVpc3RoNewDinBufferCmd )

/*-----------------------------------------------------------------------------------------------------------*/
/* 5.0 set slave address                                                                                     */
/*-----------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/* 5.1 structure                                                             */
/*---------------------------------------------------------------------------*/
typedef struct
{
   uint8_t bTsAddr;
   uint8_t bIdentHigh;
   uint8_t bIdentLow;
   uint8_t bNoAddressChanged;
} STRUC_SSA_BLOCK;
#define VPC3_STRUC_SSA_BLOCK_PTR  STRUC_SSA_BLOCK  VPC3_PTR
#define MEM_STRUC_SSA_BLOCK_PTR  STRUC_SSA_BLOCK  MEM_PTR

/*---------------------------------------------------------------------------*/
/* 5.2 macros                                                                */
/*---------------------------------------------------------------------------*/
/*  -- read length of set-slave-address ---------------------------------------- */
#define VPC3_GET_SSA_LEN()  Vpc3Read( bVpc3RwLenSsaBuf )
/*  -- get pointer to ssa buffer ----------------------------------------------- */
#define VPC3_GET_SSA_BUF_PTR() (VPC3_UNSIGNED8_PTR)(((VPC3_ADR)( Vpc3Read( bVpc3RwSsaBufPtr )) << SEG_MULDIV ))
/*  -- acknowledge ssa commando ------------------------------------------------ */
#define VPC3_FREE_SSA_BUF() Vpc3Read( bVpc3RoSsaBufferFreeCmd )

/*---------------------------------------------------------------------------*/
/* 6.2 macros for global control                                             */
/*---------------------------------------------------------------------------*/
/*  -- read Global-Control-Command ----------------------------------------- */
#define VPC3_GET_GC_COMMAND() Vpc3Read( bVpc3RwGcCommand )

/*-----------------------------------------------------------------------------------------------------------*/
/* 7.0 diagnostic telegram                                                                                   */
/*-----------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/* 7.1 defines                                                               */
/*---------------------------------------------------------------------------*/
#define EXT_DIAG_RESET                    ((uint8_t)0x00)
#define DIAG_RESET                        ((uint8_t)0x00)
#define EXT_DIAG_SET                      ((uint8_t)0x01)
#define STAT_DIAG_RESET                   ((uint8_t)0x00)
#define STAT_DIAG_SET                     ((uint8_t)0x02)
#define EXT_STAT_DIAG_SET                 ((uint8_t)0x03)
#define OVF_DIAG_SET                      ((uint8_t)0x04)
#define DIAG_WAIT_FOR_ALARM               ((uint8_t)0x80)

#define DIAG_REV_SIGN                     ((uint8_t)0xC0)
#define DIAG_KEN_SIGN                     ((uint8_t)0x40)
#define DIAG_CHN_SIGN                     ((uint8_t)0x80)
#define DIAG_DEV_SIGN                     ((uint8_t)0x00)

#define DIAG_TYPE_MASK                    ((uint8_t)0xC0)
#define DIAG_TYPE_DEV                     ((uint8_t)0x00)
#define DIAG_TYPE_KEN                     ((uint8_t)0x40)
#define DIAG_TYPE_CHN                     ((uint8_t)0x80)
#define DIAG_TYPE_REV                     ((uint8_t)0xC0)

#define DIAG_NORM_DIAG_SIZE               ((uint8_t)0x06)
#define DIAG_TYPE_CHN_SIZE                ((uint8_t)0x03)
#define DIAG_TYPE_REV_SIZE                ((uint8_t)0x01)

#define STATUS_DIAG_HEAD_SIZE             ((uint8_t)0x04)
#define STATUS_TYPE_SIGN                  ((uint8_t)0x80)
#define STATUS_TYPE_STATUS_MESSAGE        ((uint8_t)0x01)
#define STATUS_TYPE_MODUL_STATUS          ((uint8_t)0x02)
#define STATUS_TYPE_PRM_COMMAND_ACK       ((uint8_t)0x1E)
#define STATUS_TYPE_H_STATUS_MESSAGE      ((uint8_t)0x1F)
#define STATUS_TYPE_MANU_MIN              ((uint8_t)0xA0)
#define STATUS_TYPE_MANU_MAX              ((uint8_t)0xFE)

#define ALARM_TYPE_SIGN                   ((uint8_t)0x00)
#define ALARM_DIAG_HEAD_SIZE              ((uint8_t)0x04)

/*---------------------------------------------------------------------------*/
/* 7.3 macros                                                                */
/*---------------------------------------------------------------------------*/
/* -- Control diagnostics buffer ------------ */
#define VPC3_GET_DIAG_FLAG()  ( Vpc3Read( bVpc3RoStatus_L ) & VPC3_DIAG_FLAG )

/*---------------------------------------------------------------------------*/
/* 10.4 structure of a FDL-sap-entry                                         */
/*---------------------------------------------------------------------------*/
typedef struct
{
   uint8_t bRespSendSapNr;
   uint8_t bReqSa;
   uint8_t bAccessReqSsap;
   uint8_t bEventServSup;
   uint8_t abVpc3SegIndPtr[2];
   uint8_t bVpc3SegRespPtr;
} FDL_SAP_CNTRL;
#define FDL_SAP_CNTRL_PTR FDL_SAP_CNTRL *
#define cSizeFdlSapCntrlEntry   ((uint8_t)0x07)

/* -- structure of a FDL-indication-response-buffer-head --------------------- */
typedef struct
{
   uint8_t bControl;               /*!< control byte for buffer management */
   uint8_t bMaxIndicationLength;   /*!< maximal length of indication buffer */
   uint8_t bIndicationLength;      /*!< current indication/response length */
   uint8_t bFunctionCode;          /*!< current indication/response-function-code */
   uint8_t bDpv1Service;           /*!< current DPV1 service */
   uint8_t bSlotNr;                /*!< current slot number */
   uint8_t bIndex;                 /*!< current index number */
   uint8_t bDataLength;            /*!< current data length */
} MSG_HEADER;
#define MSG_HEADER_PTR MSG_HEADER *
#define cSizeMsgHeader   ((uint8_t)0x08)

/*----------------------------------------------------------------------------*/
/* 11.9 structure of ALARM                                                    */
/*----------------------------------------------------------------------------*/
typedef struct
{
   //DPL_STRUC_LIST_CB    dplListHead;

   uint8_t              bHeader;
   uint8_t              bAlarmType;
   uint8_t              bSlotNr;
   uint8_t              bSpecifier;
   uint8_t              bUserDiagLength;
   uint8_t              bCallback;
   MEM_UNSIGNED8_PTR    pToUserDiagData;
} ALARM_STATUS_PDU;
#define ALARM_STATUS_PDU_PTR   ALARM_STATUS_PDU  MEM_PTR

/*typedef union
{
   DPL_STRUC_LIST_CB_PTR sListPtr;
   ALARM_STATUS_PDU_PTR  psAlarm;
} ALARM_UNION_ALARM;*/

/*-----------------------------------------------------------------------------------------------------------*/
/* 12.0 global system structure                                                                              */
/*-----------------------------------------------------------------------------------------------------------*/
#define MSAC_C1_MAX_PDU_SIZE              ((uint8_t)0xF0)
#define MSAC_C1_MAX_PDU                   ((uint8_t)0xF4)   /* PDU with DPV1-Header (4Byte) + 240 User Data */
#define MSAC_C1_MIN_PDU_SIZE              ((uint8_t)0x44)   /* I&M function are mandatory */

/* defines for MSAC1 Operation Mode */
typedef enum
{
   DPV0_MODE           = ((uint8_t)0x00),
   DPV1_MODE           = ((uint8_t)0x01)
} DP_OPMODE;

/* defines for MSAC1 Start State */
typedef enum
{
   DP_SS_IDLE          = ((uint8_t)0x00),
   DP_SS_STOP          = ((uint8_t)0x01),
   DP_SS_START_AGAIN   = ((uint8_t)0x02),
   DP_SS_RUN           = ((uint8_t)0x03)
} MSAC1_START_STATE;

typedef enum
{
   C1_SAP_50     = (1 << 0),
   C1_SAP_51     = (1 << 1),

   C1_SAP_NO_SAP = 0x00,
   C1_SAP_ALL    = 0x03
} eC1Saps;

typedef struct
{
   eC1Saps eC1SapSupported;
   uint8_t bC2Enable;
   uint8_t bC2_NumberOfSaps;
} sFdl_Init;

/* -- structures -------------------------------------------------------------- */
typedef struct
{
   eC1Saps              eSapSupported;
   MSAC1_START_STATE    eStartState;
   uint8_t              bDxEntered;
   uint8_t              bFdlClosing;
   uint8_t              bActiveJob;
   uint8_t              bC1Service;
   uint8_t              bMSAC1Sactivate;

   MSG_HEADER_PTR       psMsgHeader;
   VPC3_UNSIGNED8_PTR   pToDpv1Data;

   MSG_HEADER_PTR       psMsgHeaderSAP50;
   VPC3_UNSIGNED8_PTR   pToDpv1DataSAP50;
} C1_STRUC;

#if DP_MSAC_C2
typedef struct
{
   uint8_t              bC2Enable;
   uint8_t              bC2_NumberOfSaps;
   uint16_t             wSendTimeOut;
   uint8_t              bReadRecPtr;
   uint8_t              bWriteRecPtr;
   uint8_t              bStartRecPtr;                                /* first element of receive queue */
   uint8_t              bEndRecPtr;                                  /* last  element of receive queue */
   uint8_t              bNrOfSap;

   uint8_t              bEnabled;
   uint8_t              bCloseChannelRequest;                        /* memory item for close_channel request of the user, */
                                                                     /* used to reject a double request */
   MSAC_C2_INITIATE_REQ_PDU   sInitiateReq;
   MSAC_C2_REC_QUEUE_PTR      psActRecQueue;
   MSAC_C2_REC_QUEUE          asRecQueue[MSAC_C2_MAX_INPUT_ITEMS];
   MSAC_C2_CONNECT_ITEM       asConnectionList[DP_C2_NUM_SAPS];      /* list of connection-descriptions */
   MSAC_C2_TIMER              asTimerList[ DP_C2_NUM_SAPS ];
} C2_STRUC;
#endif /* #if DP_MSAC_C2 */

#define AL_STATE_CLOSED                   ((uint8_t)0x10)
#define AL_STATE_OPEN                     ((uint8_t)0x11)

#define AL_TYPE_MAX                       ((uint8_t)0x07)   /* dpv1 draft specification - do not use cast ! */
#define AL_SEQUENCE_MAX                   ((uint8_t)0x20)   /* dpv1 draft specification - do not use cast ! */

/* defines for sequence_status */
#define AL_SEQUENCE_STATUS_SIZE           ((uint8_t)(((ALARM_TYPE_MAX * MAX_SEQ_NR)+7)/8))

#define AL_ALARM_STATUS_ACTION_SET        ((uint8_t)0x11)
#define AL_ALARM_STATUS_ACTION_CHECK      ((uint8_t)0x49)
#define AL_ALARM_STATUS_ACTION_RESET      ((uint8_t)0x57)

#if 0
typedef struct
{
   DPL_STRUC_LIST_CB dplAlarmQueue;
   DPL_STRUC_LIST_CB dplAlarmAckQueue;

   uint8_t       bState;
   uint8_t       bEnabled;       /* DPV1_STATUS 2, indicates the type of alarms */
   uint8_t       bMode;          /* DPV1_STATUS 3 */

   uint8_t       bTypeStatus;
   uint8_t       abSequenceStatus[ AL_SEQUENCE_STATUS_SIZE ];

   uint8_t       bSequence;      /* VPC3_FALSE: only one alarm of a specific ALARM_TYPE can be active at one time */
                                 /* VPC3_TRUE : several alarms (2 to 32) of the same or different ALARM_TYPE can be */
                                 /*       active at one time */
   uint8_t       bLimit;         /* contains the maximum number of alarms */
                                 /* allowed by the actual master-slave connection */
   uint8_t       bCount;         /* contains the number of alarms, which have been sent */
} AL_STRUC;
#endif // 0

/* defines for PRMCMD Function */
#define FUNC_BACKUP_REQUEST               ((uint8_t)0x01)
#define FUNC_PRIMARY_REQUEST              ((uint8_t)0x02)
#define FUNC_MASK_START_STOP_MSAC1S       ((uint8_t)0x0C)
#define FUNC_NO_ACTION                    ((uint8_t)0x00)
#define FUNC_STOP_MSAC1S                  ((uint8_t)0x04)
#define FUNC_START_MSAC1S                 ((uint8_t)0x08)
#define FUNC_RESET_MSAC1S                 ((uint8_t)0x0C)
#define FUNC_CHECK_PROPERTIES             ((uint8_t)0x10)
#define FUNC_CHECK_RESERVED_1             ((uint8_t)0x20)
#define FUNC_CHECK_MASTER_STATE_CLEAR     ((uint8_t)0x40)
#define FUNC_CHECK_RESERVED_2             ((uint8_t)0x80)

/* defines for PRMCMD Properties */
#define PROP_PRIM_REQ_MS0_MS1_USED        ((uint8_t)0x01)
#define PROP_PRIM_START_STOPMSAC1S_USED   ((uint8_t)0x02)
#define PROP_ADDRESS_CHANGED              ((uint8_t)0x04)
#define PROP_ADDRESS_OFFSET_64            ((uint8_t)0x08)
#define PROP_CHECK_RESERVED_1             ((uint8_t)0x10)
#define PROP_CHECK_RESERVED_2             ((uint8_t)0x20)
#define PROP_CHECK_RESERVED_3             ((uint8_t)0x40)
#define PROP_CHECK_RESERVED_4             ((uint8_t)0x80)

#define PRM_CMD_LENGTH                    ((uint8_t)0x12)

#define PC_PRMCMD_SUPPORTED               ((uint8_t)0x01)
#define PC_SEND_PRMCMD_ACK                ((uint8_t)0x02)

/*
typedef struct
{
   uint8_t bSlotNr;
   uint8_t bSpecifier;
   uint8_t bFunction;
   uint8_t bProperties;
   uint16_t wOutputHoldTime;
} RED_PRM_CMD;
#define RED_PRM_CMD_PTR   RED_PRM_CMD MEM_PTR
*/

/* defines for SWITCH OVER */
#define SWO_DATAEX_TO_WAITPRM             ((uint8_t)0x01)
#define SWO_WAITPRM_TO_DATAEX             ((uint8_t)0x02)
#define SWO_PLC_RUN_TO_STOP               ((uint8_t)0x03)
#define SWO_PLC_STOP_TO_RUN               ((uint8_t)0x04)
#define SWO_BAUDRATE_DETECT               ((uint8_t)0x05)

/* defines for RedState_1/RedState_2 */
#define RS_BACKUP                         ((uint8_t)0x01)
#define RS_PRIMARY                        ((uint8_t)0x02)
#define RS_HW_DEFEKT                      ((uint8_t)0x04)
#define RS_DATA_EXCHANGE                  ((uint8_t)0x08)
#define RS_MASTER_STATE_CLEAR             ((uint8_t)0x10)
#define RS_BAUDRATE_FOUND                 ((uint8_t)0x20)
#define RS_TOH_STARTED                    ((uint8_t)0x40)
#define RS_RESERVED                       ((uint8_t)0x80)

typedef struct
{
   uint8_t bHeaderByte;   /* fix to 0x08 */
   uint8_t bStatusType;   /* RedStateDiagnosis: 0x9F; PrmCommandAck: 0x9E */
   uint8_t bSlotNr;       /* fix to 0x00 */
   uint8_t bSpecifier;    /*  */
   uint8_t bFunction;     /*  */
   uint8_t bRedState_1;   /* State from the initiator */
   uint8_t bRedState_2;   /* State from the other slave device */
   uint8_t bRedState_3;   /* Application specific */
} RED_STATE_STRUC;
#define RED_STATE_STRUC_PTR   RED_STATE_STRUC MEM_PTR
#define cSizeOfRedStateDiag                  ((uint8_t)0x08)

#define RED_CHANNEL_1                        ((uint8_t)0x01)
#define RED_CHANNEL_2                        ((uint8_t)0x02)
#define RED_CHANNEL                          ((uint8_t)0x03)

#define RISM_ST_POWER_ON                     ((uint8_t)0x00)
#define RISM_ST_S_WAITING                    ((uint8_t)0x01)
#define RISM_ST_S_PRIMARY                    ((uint8_t)0x02)
#define RISM_ST_C_CONFIGURE                  ((uint8_t)0x03)
#define RISM_ST_BACKUP                       ((uint8_t)0x04)
#define RISM_ST_BP_PARTNER_ACK               ((uint8_t)0x05)
#define RISM_ST_BP_SWITCHOVER                ((uint8_t)0x06)
#define RISM_ST_BP_PRM_CMD                   ((uint8_t)0x07)
#define RISM_ST_BP_DX                        ((uint8_t)0x08)
#define RISM_ST_PRIMARY                      ((uint8_t)0x09)
#define RISM_ST_PB_PARTNER_ACK               ((uint8_t)0x0A)
#define RISM_ST_PB_SWITCHOVER                ((uint8_t)0x0B)
#define RISM_ST_NIL                          ((uint8_t)0xFF)

#define RISM_EV_RED_C_CHECK_IND              ((uint16_t)0x0001)
#define RISM_EV_RED_C_SwitchoverPosCnf       ((uint16_t)0x0002)
#define RISM_EV_RED_C_SwitchoverNegCnf       ((uint16_t)0x0004)
#define RISM_EV_RED_C_SwitchoverInd          ((uint16_t)0x0008)
#define RISM_EV_FSPMS_Prm_Cmd_Ind            ((uint16_t)0x0010)
#define RISM_EV_FSPMS_New_Output_Ind         ((uint16_t)0x0020)
#define RISM_EV_FSPMS_Stopped_Ind            ((uint16_t)0x0080)
/* #define RISM_EV_MS2_AR_ACTIVE              ((uint16_t)0x0100) */
#define RISM_EV_SwitchoverDone               ((uint16_t)0x1000)
#define RISM_EV_RED_C_WaitForSwitchoverCnf   ((uint16_t)0x2000)

typedef struct
{
   uint16_t wEvent;
   uint8_t  bStatus;
} RED_INDICATION_STRUC;
#define RED_IND_STRUC_PTR   RED_INDICATION_STRUC MEM_PTR

#define REDUNDANCY_PRIMARY_ADDR           ((uint8_t)0x01)
#define REDUNDANCY_BACKUP_ADDR            ((uint8_t)0x02)
#define REDUNDANCY_CHANGE_ADDR            ((uint8_t)0xFC)
#define REDUNDANCY_RESET_ADDR             ((uint8_t)0xFE)
#define REDUNDANCY_NIL_ADDR               ((uint8_t)0xFF)

/* -- defines for user state */
#define USER_STATE_CLEAR                  ((uint8_t)0x00)
#define USER_STATE_RUN                    ((uint8_t)0x01)

#define eDpStateInit             ((uint8_t)( 1u << 0 ))  /**< init state, all data cleared */
#define eDpStateRun              ((uint8_t)( 1u << 1 ))  /**< internal state run */
#define eDpStateApplReady        ((uint8_t)( 1u << 2 ))  /**< state application ready, set after successfull call of function DpAppl_ApplicationReady() */
#define eDpStateCfgOkStatDiag    ((uint8_t)( 1u << 3 ))  /**< VPC3+ has received valid cfg-data, set Diag.StatDiag, call the function DpAppl_ApplicationReady() */
#define eDpStateDiagActive       ((uint8_t)( 1u << 4 ))  /**< The user has send new user diagnostic to VPC3+ and waits for new diag-pointer */
#define eDpStateWritePrmOK       ((uint8_t)( 1u << 5 ))  /**< Write Parameter was OK */
#define eDpStateWaitForApplReady ((uint8_t)( 1u << 6 ))  /**< Wait for Diag.StatDiag reset! */
#define eDpStateAlarmActive      ((uint8_t)( 1u << 7 ))  /**< The master is now in Operate-mode, the alarm state machine is active */

/* -- dp system structure ----------------------------------------------------- */
typedef struct
{
   uint8_t                 eDpState;
   uint16_t                wEvent;
   #if DP_INTERRUPT_MASK_8BIT == 0
      uint16_t             wInterruptEvent;
      uint16_t             wPollInterruptEvent;
      uint16_t             wPollInterruptMask;
   #endif /* #if DP_INTERRUPT_MASK_8BIT == 0 */
   uint16_t                wOldDiag;
   uint8_t                 bDiagSeqCounter;
   uint8_t                 bOldGlobalControl;
   uint8_t                 abUserDiagnostic[ DIAG_BUFSIZE ];
   uint8_t                 abPrmCfgSsaHelpBuffer[ HELP_BUFSIZE ];
   DP_OPMODE               eDPV1;

   uint8_t                 bOutputDataLength;                  /* calculated output length (data from DP-Master to VPC3) */
   uint8_t                 bInputDataLength;                   /* calculated input length  (data from VPC3 to DP-Master) */
   uint16_t                wVpc3UsedDPV0BufferMemory;          /* consumed user_memory */
   uint16_t                wVpc3UsedDPV1BufferMemory;          /* consumed user_memory */

   uint8_t                 bIntIndHigh;                        /* interrupt indication high byte */
   uint8_t                 bIntIndLow;                         /* interrupt indication low byte */

   uint8_t                 bDinBufsize;                        /*!< Length of the 3 Din buffers (dp_cfg.h:\ref DIN_BUFSIZE).            */
   uint8_t                 bDoutBufsize;                       /*!< Length of the 3 Dout buffers (dp_cfg.h:\ref DOUT_BUFSIZE).          */
   uint8_t                 bPrmBufsize;                        /*!< Length of the Set_Param buffer (dp_cfg.h:\ref PRM_BUFSIZE).         */
   uint8_t                 bDiagBufsize;                       /*!< Length of the 2 Diag buffer (dp_cfg.h:\ref DIAG_BUFSIZE).           */
   uint8_t                 bCfgBufsize;                        /*!< Length of the 2 Config buffer (dp_cfg.h:\ref CFG_BUFSIZE).          */
   uint8_t                 bSsaBufsize;                        /*!< Length of the Set_Slave_Address-buffer (dp_cfg.h:\ref SSA_BUFSIZE). */

   uint16_t                wAsicUserRam;

   #if DP_FDL

      uint16_t             awImIndex[0x10];
      uint8_t              abCallService[0x10];

      uint16_t             awModImIndex[0x41][0x10];

      #if DP_MSAC_C1
         C1_STRUC          sC1;
      #endif /* #if DP_MSAC_C1 */

      #if DP_MSAC_C2
         C2_STRUC          sC2;
      #endif /* #if DP_MSAC_C2 */

      FDL_STRUC            sFdl;
      AL_STRUC             sAl;

   #endif /* #if DP_FDL */

   #if REDUNDANCY
      uint8_t                 bPrmCommandAck;
      uint8_t                 bOnline;
      uint8_t                 bChannel;
      uint8_t                 bHwError;
      uint8_t                 bPrmCmdReceived;

      uint8_t                 bRedState;
      uint8_t                 bRedPartnerState;
      uint8_t                 bRedPreferedPrim;
      uint16_t                wRedToPrime;
      uint8_t                 bRedClear;
      uint8_t                 bRedTredcom;
      uint8_t                 bRedPrmCmdActive;
      uint8_t                 bRedNotReadyForP;
      uint8_t                 bRedMS2_AR_Active;
      uint8_t                 bRedTohStopped;
      uint8_t                 bRedSwitchover;
      RED_INDICATION_STRUC    sRedIndication;
      STRUC_PRM_BLOCK_PRMCMD  sPrmCmd;
      RED_IND_STRUC_PTR       ptrToRedIndOtherSlave;
      RED_STATE_STRUC         sRedState;
      RED_STATE_STRUC_PTR     ptrToRedStateOtherSlave;
   #endif /* #if REDUNDANCY */

   VPC3_UNSIGNED8_PTR      pDoutBuffer1;                       /*!< microprocessor formatted pointer to Dout buffer 1.*/
   VPC3_UNSIGNED8_PTR      pDoutBuffer2;                       /*!< microprocessor formatted pointer to Dout buffer 2.*/
   VPC3_UNSIGNED8_PTR      pDoutBuffer3;                       /*!< microprocessor formatted pointer to Dout buffer 3.*/

   VPC3_UNSIGNED8_PTR      pDinBuffer1;                        /*!< microprocessor formatted pointer to Din buffer 1.*/
   VPC3_UNSIGNED8_PTR      pDinBuffer2;                        /*!< microprocessor formatted pointer to Din buffer 2.*/
   VPC3_UNSIGNED8_PTR      pDinBuffer3;                        /*!< microprocessor formatted pointer to Din buffer 3.*/

   VPC3_UNSIGNED8_PTR      pDiagBuffer1;                       /*!< microprocessor formatted pointer to Diag buffer 1.*/
   VPC3_UNSIGNED8_PTR      pDiagBuffer2;                       /*!< microprocessor formatted pointer to Diag buffer 2.*/

   #if DP_SUBSCRIBER
      PRM_DXB_LINK_TABLE   sLinkTable;                         /* LinkTable */
      VPC3_UNSIGNED8_PTR   pDxbOutBuffer1;                     /*!< microprocessor formatted pointer to Dxb buffer 1.*/
      VPC3_UNSIGNED8_PTR   pDxbOutBuffer2;                     /*!< microprocessor formatted pointer to Dxb buffer 2.*/
      VPC3_UNSIGNED8_PTR   pDxbOutBuffer3;                     /*!< microprocessor formatted pointer to Dxb buffer 3.*/
   #endif /* #if DP_SUBSCRIBER */

   VPC3_UNSIGNED8_PTR      pDiagBuffer;                        /*!< microprocessor formatted pointer to current diagnostic buffer.*/
} VPC3_SYSTEM_STRUC;
#define VPC3_SYSTEM_STRUC_PTR    VPC3_SYSTEM_STRUC *

#define VPC3_SetDpState( _eDpState ) (pDpSystem->eDpState |=  (_eDpState))
#define VPC3_ClrDpState( _eDpState ) (pDpSystem->eDpState &= ~(_eDpState))
#define VPC3_GetDpState( _eDpState ) (pDpSystem->eDpState &   (_eDpState))

#if REDUNDANCY
   typedef enum
   {
      eRedAppl_SingleSlv = 0,
      eRedAppl_RedSlvManuSpec = 1,
      eRedAppl_RedSlvProfile2212 = 2,
      eRedAppl_RedSlvManuSpecFR = 3
   }eRedApplication;

   typedef struct
   {
      eRedApplication      eRedAppl;
      uint8_t              bSwitchOverChannel1;
      uint8_t              bSwitchOverChannel2;
      uint8_t              bRedundancyPrimaryAddress;
      uint8_t              bRedundancyBackupAddress;
      uint8_t              bUpdateRedStateDiagnosis;
      uint8_t              bRedStateSeqNr;
      uint8_t              bRedTimerTick;
      uint8_t              bAddressChanged;
   } REDUNDANCY_STRUC;
#endif /* #if REDUNDANCY */

/*-----------------------------------------------------------------------------------------------------------*/
/* 16.0 ERROR defines                                                                                        */
/*-----------------------------------------------------------------------------------------------------------*/
typedef enum
{
   DP_FATAL_ERROR                      = 0x00, /* fatal error */

   DP_OK                               = 0x01, /* OK */
   DP_NOK                              = 0x02, /* OK */

   DP_NOT_OFFLINE_ERROR                = 0x10, /* VPC3 is not in OFFLINE state */
   DP_ADDRESS_ERROR                    = 0x11, /* Slave Address Error */
   DP_CALCULATE_IO_ERROR               = 0x12,
   DP_DOUT_LEN_ERROR                   = 0x13,
   DP_DIN_LEN_ERROR                    = 0x14,
   DP_DIAG_LEN_ERROR                   = 0x15,
   DP_PRM_LEN_ERROR                    = 0x16,
   DP_SSA_LEN_ERROR                    = 0x17,
   DP_CFG_LEN_ERROR                    = 0x18,
   DP_CFG_FORMAT_ERROR                 = 0x19,
   DP_LESS_MEM_ERROR                   = 0x1A,
   DP_LESS_MEM_FDL_ERROR               = 0x1B,
   DP_PRM_RETRY_ERROR                  = 0x1C,
   DP_SPEC_PRM_NOT_SUPP_ERROR          = 0x1D,

   DP_PRM_ENTRY_ERROR                  = 0x20,
   DP_PRM_SERVICE_NOT_SUPPORTED        = 0x21,
   DP_PRM_DPV1_STATUS                  = 0x22,
   DP_PRM_DPV0_NOT_SUPP                = 0x23,
   DP_PRM_DPV1_NOT_SUPP                = 0x24,
   DP_PRM_BLOCK_ERROR                  = 0x25,
   DP_PRM_BLOCK_CMD_NOT_SUPP           = 0x26,
   DP_PRM_ALARM_ERROR                  = 0x27,
   DP_PRMCMD_LEN_ERROR                 = 0x28,
   DP_PRM_SOLL_IST_ERROR               = 0x29,

   DP_PRM_USER_PRM_BLOCK_ERROR         = 0x2A,

   DP_PRM_DXB_PRM_BLOCK_LENGTH_ERROR   = 0x2B,
   DP_PRM_DXB_MAX_LINK_ERROR           = 0x2C,
   DP_PRM_DXB_ERROR                    = 0x2D,
   DP_PRM_DXB_WD_ERROR                 = 0x2E,

   DP_PRM_CS_LENGTH_ERROR              = 0x30,
   DP_PRM_CS_INTERVAL_ERROR            = 0x31,

   DP_PRM_ISO_PRM_BLOCK_ERROR          = 0x40,
   DP_PRM_DPV1_STATUS_3_ISOM_REQ_ERROR = 0x41,
   DP_PRM_ISO_T_BASE_DP_ERROR          = 0x42,
   DP_PRM_ISO_T_BASE_IO_ERROR          = 0x43,

   DP_PRM_UNKNOWN_MOD_REF              = 0x4A,

   DP_CFG_ENTRY_ERROR                  = 0x50,
   DP_CFG_UPDATE_ERROR                 = 0x51,

   DP_DIAG_BUFFER_ERROR                = 0x60,
   DP_DIAG_SEQUENCE_ERROR              = 0x61,
   DP_DIAG_OLD_DIAG_NOT_SEND_ERROR     = 0x62,
   DP_DIAG_NOT_POSSIBLE_ERROR          = 0x63,
   DP_DIAG_NO_BUFFER_ERROR             = 0x64,
   DP_DIAG_BUFFER_LENGTH_ERROR         = 0x65,
   DP_DIAG_CONTROL_BYTE_ERROR          = 0x66,
   DP_DIAG_SAME_DIAG                   = 0x67,
   DP_DIAG_ACTIVE_DIAG                 = 0x68,

   C1_DATA_LEN_ERROR                   = 0x70,

   C2_DATA_LEN_ERROR                   = 0x80,
   C2_DATA_POLL_TIMEOUT_ERROR          = 0x81,
   C2_DATA_SAP_ERROR                   = 0x82,
   C2_NO_CONN_RESOURCE                 = 0x83,
   C2_INV_LOWER_LAYER                  = 0x84,
   C2_ENABLED_ERROR                    = 0x85,
   C2_RESOURCE_ERROR                   = 0x86,
   C2_INV_CN_ID                        = 0x87,
   C2_USER_ERR                         = 0x88,
   C2_DOUBLE_REQUEST                   = 0x89,
   C2_ALREADY_DISCONNECTED             = 0x8A,

   SSC_MAX_DATA_PER_LINK               = 0x90,

   DP_EEPROM_ERROR                     = 0xF1, /* Hardware errors */
   DP_VPC3_ERROR                       = 0xF4,
   DP_SRAM_ERROR                       = 0xFF

} DP_ERROR_CODE;

/** @brief possible return values for the function DpCfg_ChkNewCfgData() */
typedef enum
{
   DP_CFG_OK = 0     /**< The transferred configuration is OK. */
   ,DP_CFG_FAULT     /**< The transferred configuration is not OK. */
   ,DP_CFG_UPDATE    /**< The transferred configuration is OK, but it's different from read configuration buffer. The user will exchange verified configuration with read configuration buffer. */
} E_DP_CFG_ERROR;

typedef enum
{
   _DP_USER    = 0x10,  /**< DpUser.c. */
   _DP_APPL    = 0x11,  /**< DpAppl.c. */
   _DP_PRM     = 0x12,  /**< DpPrm.c. */
   _DP_CFG     = 0x13,  /**< DpCfg.c. */
   _DP_DIAG    = 0x14,  /**< DpDiag.c. */
   _DP_V1      = 0x15,  /**< DpV1.c. */
   _DP_IM      = 0x16,  /**< DpIm.c. */
   _DP_IF      = 0x20,  /**< DPV0_DRV: dp_if.c. */
   _DP_ISR     = 0x30,  /**< DPV0_DRV: dp_isr.c. */
   _DP_FDL     = 0x40,  /**< DPV1_DRV: dp_fdl.c. */
   _DP_C1      = 0x50,  /**< DPV1_DRV: dp_msac1.c. */
   _DP_C2      = 0x60,  /**< DPV1_DRV: dp_msac2.c. */
   _TIME_IF    = 0x70,  /**< TIME_DRV: TimeDrv.c. */
   _RED_DRV    = 0x80   /**< RED_DRV: RedDrv.c. */
} DP_ERROR_FILE;

typedef struct
{
   uint8_t  bFunction;
   uint8_t  bErrorCode;
   uint16_t bDetail;
   uint8_t  bCnId;
} VPC3_STRUC_ERRCB;
#define VPC3_ERRCB_PTR   VPC3_STRUC_ERRCB *


// VPC3 API

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void Vpc3_Init(void);

//------------------------------------------------------------------------------
//  Zapise byte do VPC3+
//------------------------------------------------------------------------------
void Vpc3Write(VPC3_ADR wAddress, uint8_t bData);

//------------------------------------------------------------------------------
//  Vycte byte z VPC3+
//------------------------------------------------------------------------------
uint8_t Vpc3Read(VPC3_ADR wAddress);

/**
 * @brief Copy block of memory to VPC3+.
 *
 * @param[in]pToVpc3Memory Pointer to the destination array where the content is to be copied.
 * @param[in]pLocalMemory Pointer to the source of data to be copied.
 * @param[in]wLength Number of bytes to copy.
 */
void Vpc3_CopyToVpc3(VPC3_UNSIGNED8_PTR pToVpc3Memory, MEM_UNSIGNED8_PTR pLocalMemory, uint16_t wLength);

/**
 * @brief Copy block of memory from VPC3+.
 *
 * @param[in]pLocalMemory Pointer to the destination array where the content is to be copied.
 * @param[in]pToVpc3Memory Pointer to the source of data to be copied.
 * @param[in]wLength Number of bytes to copy.
 */
void Vpc3_CopyFromVpc3( MEM_UNSIGNED8_PTR pLocalMemory, VPC3_UNSIGNED8_PTR pToVpc3Memory, uint16_t wLength);

//------------------------------------------------------------------------------
//  Set VPC3+ reset.
//  The VPC3+ reset is high-active!
//------------------------------------------------------------------------------
void Vpc3_SetResetVPC3Channel1(void);

//------------------------------------------------------------------------------
//  Clear VPC3+ reset.
//  The VPC3+ reset is high-active!
//------------------------------------------------------------------------------
void Vpc3_ClrResetVPC3Channel1(void);

//------------------------------------------------------------------------------
//  Enable VPC3+ interrupt.
//------------------------------------------------------------------------------
void Vpc3_EnableInterruptVPC3Channel1(void);

//------------------------------------------------------------------------------
//  Disable VPC3+ interrupt.
//------------------------------------------------------------------------------
void Vpc3_DisableInterruptVPC3Channel1(void);

#endif // VPC3_H_INCLUDED
