//******************************************************************************
//  Ovladac VPC3
//*****************************************************************************/

#include "sys/sys_config.h"

#ifdef PROFIBUS_HW_MODULE_TYPE

#include <stdio.h>
#include "machine/api.h"
#include "vpc3.h"
#include "sys/sys.h"
#include "sys/spi.h"
#include "sys/io.h"
#include "sys/systick.h"
#include "sys/log.h"
#include "sys/global_status.h"
#include "board/board_config.h"

// Parametry SPI
#define VPC3_SPI_DATA_ORDER              SPI_DATA_ORDER_MSB
#define VPC3_SPI_CLOCK_POLARITY          SPI_CLOCK_POL_RISE_FALL
#define VPC3_SPI_CLOCK_PHASE             SPI_CLOCK_PHASE_SAMPLE_SETUP
#define VPC3_SPI_CLOCK_RATE              SPI_CLOCK_RATE_FOSC_64
#define VPC3_SPI_SLAVE_SELECT_DELAY      10
#define VPC3_SPI_DUMMY_BYTE              0x00

// SPI instrukce
#define READ_BYTE_INSTR     (0x13)
#define READ_ARRAY_INSTR    (0x03)
#define WRITE_BYTE_INSTR    (0x12)
#define WRITE_ARRAY_INSTR   (0x02)

// bufer pro SPI data
static uint8_t spiBuffer[VPC3_SPI_BUFFER_SIZE];

// naplni parametry SPI
static void FillSpiParameters(TSpiParameters* spiParams)
{
    spiParams->dataOrder = VPC3_SPI_DATA_ORDER;
    spiParams->clockPolarity = VPC3_SPI_CLOCK_POLARITY;
    spiParams->clockPhase = VPC3_SPI_CLOCK_PHASE;
    spiParams->clockRate = VPC3_SPI_CLOCK_RATE;
    spiParams->slaveSelectDelay = VPC3_SPI_SLAVE_SELECT_DELAY;
    spiParams->dummyByte = VPC3_SPI_DUMMY_BYTE;
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void Vpc3_Init(void)
{
    TSpiParameters spiParams;
    FillSpiParameters(&spiParams);
    Spi_Open(&spiParams);
}

//------------------------------------------------------------------------------
//  Zapise byte do VPC3+
//------------------------------------------------------------------------------
void Vpc3Write(VPC3_ADR wAddress, uint8_t bData)
{
    //16 bit address into 2 bytes
	uint8_t upperByte = (uint8_t)(wAddress >> 8);
	uint8_t lowerByte = (uint8_t)wAddress;

	Vpc3_DisableInterruptVPC3Channel1();

    // instrukce
	spiBuffer[0] = WRITE_BYTE_INSTR;
	/* 16 bit address of slave memory cell*/
    //1. Upper byte address
	spiBuffer[1] = upperByte;
    //2. Lower byte address
	spiBuffer[2] = lowerByte;
	/*data byte*/
	spiBuffer[3] = bData;

	Spi_DataTransfer(spiBuffer, 4);

    while (Spi_Pending())
        ;

    _delay_us(10);

	Vpc3_EnableInterruptVPC3Channel1();
}

//------------------------------------------------------------------------------
//  Vycte byte z VPC3+
//------------------------------------------------------------------------------
uint8_t Vpc3Read(VPC3_ADR wAddress)
{
    //16 bit address into 2 bytes
	uint8_t upperByte = (uint8_t)(wAddress >> 8);
	uint8_t lowerByte = (uint8_t)wAddress;

	Vpc3_DisableInterruptVPC3Channel1();

    // instrukce
	spiBuffer[0] = READ_BYTE_INSTR;
	/* 16 bit address of slave memory cell*/
    //1. Upper byte address
	spiBuffer[1] = upperByte;
    //2. Lower byte address
	spiBuffer[2] = lowerByte;
	/*dummy byte*/
	spiBuffer[3] = 0x00;

	Spi_DataTransfer(spiBuffer, 4);

    while (Spi_Pending())
        ;

    _delay_us(10);

    uint8_t readValue = spiBuffer[3];

	Vpc3_EnableInterruptVPC3Channel1();

	return readValue;
}

/**
 * @brief Copy block of memory to VPC3+.
 *
 * @param[in]pToVpc3Memory Pointer to the destination array where the content is to be copied.
 * @param[in]pLocalMemory Pointer to the source of data to be copied.
 * @param[in]wLength Number of bytes to copy.
 */
void Vpc3_CopyToVpc3(VPC3_UNSIGNED8_PTR pToVpc3Memory, MEM_UNSIGNED8_PTR pLocalMemory, uint16_t wLength)
{
	uint16_t wAddress = (VPC3_ADR)pToVpc3Memory;
	uint8_t upperByte = (uint8_t)(wAddress >> 8);
	uint8_t lowerByte = (uint8_t)wAddress;

	if (wLength == 0)
        return;

	Vpc3_DisableInterruptVPC3Channel1();

    // instrukce
	spiBuffer[0] = WRITE_ARRAY_INSTR;
	/* 16 bit address of slave memory cell*/
    //1. Upper byte address
	spiBuffer[1] = upperByte;
    //2. Lower byte address
	spiBuffer[2] = lowerByte;

	Spi_DataSend2(spiBuffer, 3, pLocalMemory, wLength);

    while (Spi_Pending())
        ;

    _delay_us(10);

    Vpc3_EnableInterruptVPC3Channel1();
}

/**
 * @brief Copy block of memory from VPC3+.
 *
 * @param[in]pLocalMemory Pointer to the destination array where the content is to be copied.
 * @param[in]pToVpc3Memory Pointer to the source of data to be copied.
 * @param[in]wLength Number of bytes to copy.
 */
void Vpc3_CopyFromVpc3( MEM_UNSIGNED8_PTR pLocalMemory, VPC3_UNSIGNED8_PTR pToVpc3Memory, uint16_t wLength)
{
    uint16_t wAddress = (VPC3_ADR)pToVpc3Memory;
	uint8_t upperByte = (uint8_t)(wAddress >> 8);
	uint8_t lowerByte = (uint8_t)wAddress;

	if (wLength == 0)
        return;

	Vpc3_DisableInterruptVPC3Channel1();

    // instrukce
	spiBuffer[0] = READ_ARRAY_INSTR;
	/* 16 bit address of slave memory cell*/
    //1. Upper byte address
	spiBuffer[1] = upperByte;
    //2. Lower byte address
	spiBuffer[2] = lowerByte;

	Spi_DataSendAndReceive(spiBuffer, 3, pLocalMemory, wLength);

    while (Spi_Pending())
        ;

    _delay_us(10);

    Vpc3_EnableInterruptVPC3Channel1();
}

//------------------------------------------------------------------------------
//  Set VPC3+ reset.
//  The VPC3+ reset is high-active!
//------------------------------------------------------------------------------
void Vpc3_SetResetVPC3Channel1(void)
{
    IO_SetOutputLevel(IO_ID_VPC3_RESET, IO_VPC3_RESET_ON_LEVEL);
}

//------------------------------------------------------------------------------
//  Clear VPC3+ reset.
//  The VPC3+ reset is high-active!
//------------------------------------------------------------------------------
void Vpc3_ClrResetVPC3Channel1(void)
{
    IO_SetOutputLevel(IO_ID_VPC3_RESET, IO_VPC3_RESET_OFF_LEVEL);
}

//------------------------------------------------------------------------------
//  Enable VPC3+ interrupt.
//------------------------------------------------------------------------------
void Vpc3_EnableInterruptVPC3Channel1(void)
{
    /** @todo Add your own code here! */
}

//------------------------------------------------------------------------------
//  Disable VPC3+ interrupt.
//------------------------------------------------------------------------------
void Vpc3_DisableInterruptVPC3Channel1(void)
{
    /** @todo Add your own code here! */
}

#endif // PROFIBUS_HW_MODULE_TYPE
