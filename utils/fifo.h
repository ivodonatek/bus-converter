//******************************************************************************
//  FIFO Buffer
//*****************************************************************************/

#ifndef FIFO_H_INCLUDED
#define FIFO_H_INCLUDED

#include "sys/types.h"

// FIFO object type
typedef struct {
     char * buf;
     int head;
     int tail;
     int size;
} fifo_t;

//------------------------------------------------------------------------------
//  This initializes the FIFO structure with the given buffer and size.
//------------------------------------------------------------------------------
void fifo_init(fifo_t * f, char * buf, int size);

//------------------------------------------------------------------------------
//  Are there any data in buffer?
//------------------------------------------------------------------------------
BOOL fifo_any_data(const fifo_t * f);

//------------------------------------------------------------------------------
//  This reads nbytes bytes from the FIFO.
//  The number of bytes read is returned.
//------------------------------------------------------------------------------
int fifo_read(fifo_t * f, void * buf, int nbytes);

//------------------------------------------------------------------------------
//  This writes up to nbytes bytes to the FIFO.
//  If the head runs in to the tail, not all bytes are written.
//  The number of bytes written is returned.
//------------------------------------------------------------------------------
int fifo_write(fifo_t * f, const void * buf, int nbytes);

#endif // FIFO_H_INCLUDED
