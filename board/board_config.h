//******************************************************************************
//  Konfigurace desky
//*****************************************************************************/

#ifndef BOARD_CONFIG_H_INCLUDED
#define BOARD_CONFIG_H_INCLUDED

#include "sys/uart.h"

// UART ID Modbus linky
#define MODBUS_UART_ID          UART_ID_0

// UART ID Mbus linky
#define MBUS_UART_ID            UART_ID_1

// ovladani resetu VPC3
#define IO_VPC3_RESET_ON_LEVEL  1       // aktivni uroven
#define IO_VPC3_RESET_OFF_LEVEL 0       // neaktivni uroven
#define IO_VPC3_RESET_PORT_REG  PORTA   // Port Data Register
#define IO_VPC3_RESET_DDR_REG   DDRA    // Port Data Direction Register
#define IO_VPC3_RESET_BIT       0       // Bit Position in Registers

#endif // BOARD_CONFIG_H_INCLUDED
