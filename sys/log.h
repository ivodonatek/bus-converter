//******************************************************************************
//  Logovani zprav
//*****************************************************************************/

#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include "sys/sys_config.h"
#include "sys/types.h"
#include "sys/uart.h"

// Velikost buferu logovani
#define LOG_BUFFER_SIZE     200

// UART ID logovaci linky
#define LOG_UART_ID          UART_ID_0

// Parametry UARTu logovaci linky
#define LOG_UART_BAUD_RATE   19200
#define LOG_UART_DATA_BITS   UART_FRAME_DATA_BITS_8
#define LOG_UART_STOP_BITS   UART_FRAME_STOP_BITS_ONE
#define LOG_UART_PARITY      UART_FRAME_PAR_NONE

// makra funkci
#ifdef LOG_ENABLED
#define LOG_INIT() Log_Init()
#define LOG_WRITE(msg) Log_Write(msg)
#define LOG_WRITELN(msg) Log_Writeln(msg)
#define LOG_TASK() Log_Task()
#else
#define LOG_INIT()
#define LOG_WRITE(msg)
#define LOG_WRITELN(msg)
#define LOG_TASK()
#endif // LOG_ENABLED

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani nize
//------------------------------------------------------------------------------
void Log_Init(void);

//------------------------------------------------------------------------------
//  Zapise zpravu do logu.
//------------------------------------------------------------------------------
void Log_Write(const char* msg);

//------------------------------------------------------------------------------
//  Zapise zpravu s odradkovanim do logu.
//------------------------------------------------------------------------------
void Log_Writeln(const char* msg);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void Log_Task(void);

#endif // LOG_H_INCLUDED
