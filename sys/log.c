//******************************************************************************
//  Logovani zprav
//*****************************************************************************/

#include "sys/sys_config.h"

#ifdef LOG_ENABLED

#include "log.h"
#include "uart.h"
#include "utils/fifo.h"

static char buffer[LOG_BUFFER_SIZE];
static fifo_t fifo;
static uint8_t uartData;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani nize
//------------------------------------------------------------------------------
void Log_Init(void)
{
    fifo_init(&fifo, buffer, LOG_BUFFER_SIZE);

    TUartParameters uartParams;
    uartParams.baudRate = LOG_UART_BAUD_RATE;
    uartParams.dataBits = LOG_UART_DATA_BITS;
    uartParams.stopBits = LOG_UART_STOP_BITS;
    uartParams.parity = LOG_UART_PARITY;

    Uart_Open(LOG_UART_ID, &uartParams);
}

//------------------------------------------------------------------------------
//  Zapise zpravu do logu.
//------------------------------------------------------------------------------
void Log_Write(const char* msg)
{
    fifo_write(&fifo, msg, strlen(msg));
}

//------------------------------------------------------------------------------
//  Zapise zpravu s odradkovanim do logu.
//------------------------------------------------------------------------------
void Log_Writeln(const char* msg)
{
    Log_Write(msg);
    Log_Write("\r\n");
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void Log_Task(void)
{
    if (!Uart_Sending(LOG_UART_ID) && fifo_any_data(&fifo))
    {
        if (fifo_read(&fifo, &uartData, 1))
            Uart_Send(LOG_UART_ID, &uartData, 1);
    }
}

#endif // LOG_ENABLED
