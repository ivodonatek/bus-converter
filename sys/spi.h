//******************************************************************************
//  SPI API
//*****************************************************************************/

#ifndef SPI_H_INCLUDED
#define SPI_H_INCLUDED

#include "sys/types.h"

// SPI Data Order
typedef enum
{
    SPI_DATA_ORDER_MSB = 0,     // the MSB of the data word is transmitted first
    SPI_DATA_ORDER_LSB,         // the LSB of the data word is transmitted first

    SPI_DATA_ORDER_Min = SPI_DATA_ORDER_MSB,
    SPI_DATA_ORDER_Max = SPI_DATA_ORDER_LSB,
    SPI_DATA_ORDER_Count = (SPI_DATA_ORDER_Max + 1)

} TSpiDataOrder;

// SPI Clock Polarity
typedef enum
{
    SPI_CLOCK_POL_RISE_FALL = 0,    // Leading Edge Rising, Trailing Edge Falling
    SPI_CLOCK_POL_FALL_RISE,        // Leading Edge Falling, Trailing Edge Rising

    SPI_CLOCK_POL_Min = SPI_CLOCK_POL_RISE_FALL,
    SPI_CLOCK_POL_Max = SPI_CLOCK_POL_FALL_RISE,
    SPI_CLOCK_POL_Count = (SPI_CLOCK_POL_Max + 1)

} TSpiClockPolarity;

// SPI Clock Phase
typedef enum
{
    SPI_CLOCK_PHASE_SAMPLE_SETUP = 0,   // Leading Edge Sample, Trailing Edge Setup
    SPI_CLOCK_PHASE_SETUP_SAMPLE,       // Leading Edge Setup, Trailing Edge Sample

    SPI_CLOCK_PHASE_Min = SPI_CLOCK_PHASE_SAMPLE_SETUP,
    SPI_CLOCK_PHASE_Max = SPI_CLOCK_PHASE_SETUP_SAMPLE,
    SPI_CLOCK_PHASE_Count = (SPI_CLOCK_PHASE_Max + 1)

} TSpiClockPhase;

// SPI Clock Rate
typedef enum
{
    SPI_CLOCK_RATE_FOSC_2 = 0,  // fosc/2
    SPI_CLOCK_RATE_FOSC_4,      // fosc/4
    SPI_CLOCK_RATE_FOSC_8,      // fosc/8
    SPI_CLOCK_RATE_FOSC_16,     // fosc/16
    SPI_CLOCK_RATE_FOSC_32,     // fosc/32
    SPI_CLOCK_RATE_FOSC_64,     // fosc/64
    SPI_CLOCK_RATE_FOSC_128,    // fosc/128

    SPI_CLOCK_RATE_FOSC_Min = SPI_CLOCK_RATE_FOSC_2,
    SPI_CLOCK_RATE_FOSC_Max = SPI_CLOCK_RATE_FOSC_128,
    SPI_CLOCK_RATE_FOSC_Count = (SPI_CLOCK_RATE_FOSC_Max + 1)

} TSpiClockRate;

// Parametry SPI
typedef struct SpiParameters
{
    // Data Order
    TSpiDataOrder dataOrder;

    // Clock Polarity
    TSpiClockPolarity clockPolarity;

    // Clock Phase
    TSpiClockPhase clockPhase;

    // Clock Rate
    TSpiClockRate clockRate;

    // Slave select delay [us]
    uint16_t slaveSelectDelay;

    // Dummy byte
    uint8_t dummyByte;

} TSpiParameters;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani nize
//------------------------------------------------------------------------------
void Spi_Init(void);

//------------------------------------------------------------------------------
//  Otevre SPI.
//------------------------------------------------------------------------------
void Spi_Open(TSpiParameters* params);

//------------------------------------------------------------------------------
//  Zavre SPI.
//------------------------------------------------------------------------------
void Spi_Close(void);

//------------------------------------------------------------------------------
//  Prenos dat na SPI (zacne vymena dat mezi masterem a slave),
//  volajici zajisti dostupnost pameti po dobu prenosu.
//  Na zacatku transakce jsou v datovem buferu vysilana data.
//  Po konci transakce budou v datovem buferu prijata data.
//------------------------------------------------------------------------------
void Spi_DataTransfer(uint8_t* data, uint16_t dataCount);

//------------------------------------------------------------------------------
//  Odeslani dat ve 2 buferech na SPI (zacne posilani dat masterem do slave),
//  volajici zajisti dostupnost pameti po dobu prenosu.
//------------------------------------------------------------------------------
void Spi_DataSend2(const uint8_t* data1, uint16_t dataCount1, const uint8_t* data2, uint16_t dataCount2);

//------------------------------------------------------------------------------
//  Odeslani a nasledny prijem dat na/z SPI (zacne posilani dat masterem do slave),
//  volajici zajisti dostupnost pameti po dobu prenosu.
//------------------------------------------------------------------------------
void Spi_DataSendAndReceive(const uint8_t* sendData, uint16_t sendDataCount, uint8_t* receiveData, uint16_t receiveDataCount);

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL Spi_Pending(void);

#endif // SPI_H_INCLUDED
