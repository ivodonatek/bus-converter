//******************************************************************************
//  Konfigurace systemu
//*****************************************************************************/

#ifndef SYS_CONFIG_H_INCLUDED
#define SYS_CONFIG_H_INCLUDED

// Typ hw modulu (pouzit jenom jeden, ostatni zakomentovat)
//#define MBUS_HW_MODULE_TYPE
#define PROFIBUS_HW_MODULE_TYPE

// Povoleni logovani zprav (pokud se nepouziva, zakomentovat)
//#define LOG_ENABLED

// Povoleni podpory globalniho stavu systemu (pokud se nepouziva, zakomentovat)
//#define GLOBAL_STATUS_ENABLED

#endif // SYS_CONFIG_H_INCLUDED
