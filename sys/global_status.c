//******************************************************************************
//  Globalni stav systemu
//*****************************************************************************/

#include "sys/sys_config.h"

#ifdef GLOBAL_STATUS_ENABLED

#include "global_status.h"

#define GLOBAL_STATUS_IDX_COUNT     10

static TGStatusCode gStatusCode;
static TGStatusCode gIdxStatusCodes[GLOBAL_STATUS_IDX_COUNT];

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani nize
//------------------------------------------------------------------------------
void GlobalStatus_Init(void)
{
    TGStatusIndex idx;

    gStatusCode = 0;

    for (idx = 0; idx < GLOBAL_STATUS_IDX_COUNT; idx++)
        gIdxStatusCodes[idx] = 0;
}

//------------------------------------------------------------------------------
//  Nastavi stav.
//------------------------------------------------------------------------------
void GlobalStatus_Set(TGStatusCode statusCode)
{
    gStatusCode = statusCode;
}

//------------------------------------------------------------------------------
//  Nastavi indexovany stav.
//------------------------------------------------------------------------------
void GlobalStatus_IdxSet(TGStatusIndex idx, TGStatusCode statusCode)
{
    if (idx < GLOBAL_STATUS_IDX_COUNT)
        gIdxStatusCodes[idx] = statusCode;
}

//------------------------------------------------------------------------------
//  Inkrementace indexovaneho stavu.
//------------------------------------------------------------------------------
void GlobalStatus_IdxInc(TGStatusIndex idx)
{
    if (idx < GLOBAL_STATUS_IDX_COUNT)
        gIdxStatusCodes[idx]++;
}

//------------------------------------------------------------------------------
//  Nastavi stav, pokud je hodnota statusCode vetsi nez aktualni hodnota stavu.
//------------------------------------------------------------------------------
void GlobalStatus_SetGreater(TGStatusCode statusCode)
{
    if (statusCode > gStatusCode)
        gStatusCode = statusCode;
}

//------------------------------------------------------------------------------
//  Vrati stav.
//------------------------------------------------------------------------------
TGStatusCode GlobalStatus_Get(void)
{
    return gStatusCode;
}

//------------------------------------------------------------------------------
//  Vrati indexovany stav.
//------------------------------------------------------------------------------
TGStatusCode GlobalStatus_IdxGet(TGStatusIndex idx)
{
    if (idx < GLOBAL_STATUS_IDX_COUNT)
        return gIdxStatusCodes[idx];
    else
        return 0;
}

#endif // GLOBAL_STATUS_ENABLED
