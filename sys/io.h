//******************************************************************************
//  Vstup/vystup
//*****************************************************************************/

#ifndef IO_H_INCLUDED
#define IO_H_INCLUDED

#include "sys/types.h"

// identifikator vstupu/vystupu
typedef enum
{
    IO_ID_VPC3_RESET = 0,       // ovladani resetu VPC3

    IO_ID_Min = IO_ID_VPC3_RESET,
    IO_ID_Max = IO_ID_VPC3_RESET,
    IO_ID_Count = (IO_ID_Max + 1),
    IO_ID_None = IO_ID_Count    // zadny vstup/vystup

} TIOId;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void IO_Init(void);

//------------------------------------------------------------------------------
//  Nastavi uroven na vystupu.
//------------------------------------------------------------------------------
void IO_SetOutputLevel(TIOId id, TBit level);

#endif // IO_H_INCLUDED
