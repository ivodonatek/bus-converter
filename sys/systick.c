//******************************************************************************
//  Systemovy tik
//*****************************************************************************/

#include "machine/api.h"
#include "systick.h"

#define CLOCK_PRESCALE 8

#define CLOCK_TICK_PER_SEC (F_CPU/CLOCK_PRESCALE)
#define CLOCK_TICK_PER_MSEC (F_CPU/CLOCK_PRESCALE/1000)
#define MINIMUM_TICK_DELAY (31/CLOCK_PRESCALE+1)

#if CLOCK_PRESCALE == 1
#define PRESCALE_BITS (BIT(CS10))
#elif CLOCK_PRESCALE == 8
#define PRESCALE_BITS (BIT(CS11))
#elif CLOCK_PRESCALE == 64
#define PRESCALE_BITS (BIT(CS10) | BIT(CS11))
#else
#error Invalid clock prescale
#endif

// CLOCK_MSEC_INTERVAL is 16.16 fractional number of clock ticks between each millisecond count
// As it accumulates in clock.nextMsec, it will very precisely decide when the milliseconds
// should be incremented next.

// The simple logic we use can't handle the case where we need to travel farther than half the timer
// (16-bit) before the next msec increment.
#if 0x7FFF < F_CPU / CLOCK_PRESCALE / 1000
#error Current clock logic requires that milliseconds be no farther than half the timer apart
#else
#define CLOCK_MSEC_INTERVAL ((uint32_t)((F_CPU * 65536ULL) / (CLOCK_PRESCALE*1000ULL)))
#endif

volatile uint32_t msecTick = 0; // pocitani milisekund od startu systemu
volatile uint32_t nextMsec = CLOCK_MSEC_INTERVAL; // 32.32 fractional TCNT value when we will next update msecCount

//------------------------------------------------------------------------------
//  Rutiny preruseni Timeru 1
//------------------------------------------------------------------------------
ISR(TIMER1_COMPA_vect)
{
	// time to increment the clock.
	// we use a loop in case interrupts were disabled for more than a millisecond
	// (but that should never happen!)
	uint16_t wake_at;
	while (1)
    {
		msecTick++;
		nextMsec += CLOCK_MSEC_INTERVAL;
		wake_at = (uint16_t)(nextMsec >> 16);

		// We increment the millisecond count up to MIN ticks (F_CPU/PRESCALE) early
		//  to give us time to update OCRA before the time arrives.
		if ((int16_t)(wake_at - TCNT1) > MINIMUM_TICK_DELAY)
			break;
	}
	OCR1A = wake_at;
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void SysTick_Init(void)
{
    TCNT1 = 0;
	OCR1A = (uint16_t)(CLOCK_MSEC_INTERVAL >> 16);
	OCR1B = 0;
	TCCR1B |= PRESCALE_BITS; // enable with prescaler setting from above
    TIMSK1 |= BIT(OCIE1A); // enable interrupt for Compare-A
}

//------------------------------------------------------------------------------
//  Vrati systemovy cas [ms]
//------------------------------------------------------------------------------
uint32_t SysTick_GetSysMilliSeconds(void)
{
    uint8_t prevSreg = SREG;
	cli();
	uint32_t ms = msecTick;
	SREG = prevSreg;
    return ms;
}

//------------------------------------------------------------------------------
//  Vraci TRUE pokud uplynula prodleva (delayMilliSeconds) od zadaneho casu
//  (userSysMilliSeconds)
//------------------------------------------------------------------------------
BOOL SysTick_IsSysMilliSecondTimeout(uint32_t userSysMilliSeconds, uint32_t delayMilliSeconds)
{
    if ((SysTick_GetSysMilliSeconds() - userSysMilliSeconds) >= delayMilliSeconds)
        return TRUE;
    else
        return FALSE;
}
