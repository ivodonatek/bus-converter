//******************************************************************************
//  Sprava systemu
//*****************************************************************************/

#ifndef SYS_H_INCLUDED
#define SYS_H_INCLUDED

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void Sys_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void Sys_Task(void);

//------------------------------------------------------------------------------
//  Uloha hlavni smycky.
//------------------------------------------------------------------------------
void Sys_MainTask(void);

#endif // SYS_H_INCLUDED
