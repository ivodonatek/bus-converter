//******************************************************************************
//  System types and definitions
//*****************************************************************************/

#ifndef SYS_TYPES_H_INCLUDED
#define SYS_TYPES_H_INCLUDED

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "machine/types.h"

#define BIT(x) (1<<(x))

#define MakeWord( Hi, Lo ) ( (uint16_t)( ( ( (uint8_t)( Hi ) ) << 8 ) | ( (uint8_t)( Lo ) ) ) )

#ifndef __time_t_defined
#define __time_t_defined
typedef _TIME_T_ time_t;
#endif // __time_t_defined

struct tm
{
  int	tm_sec;
  int	tm_min;
  int	tm_hour;
  int	tm_mday;
  int	tm_mon;
  int	tm_year;
  int	tm_wday;
  int	tm_yday;
  int	tm_isdst;
};

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE  1
#endif

// typ pro hodnoty TRUE a FALSE
typedef uint8_t BOOL;

// typ pro bitove hodnoty 0(L) a 1(H)
typedef uint8_t TBit;

#endif // SYS_TYPES_H_INCLUDED
