//******************************************************************************
//  SPI API
//*****************************************************************************/

#include "sys_config.h"

#ifdef PROFIBUS_HW_MODULE_TYPE

#include "machine/api.h"
#include "spi.h"

// definice portu a pinu
#define SPI_DDR     DDRB
#define SPI_PORT    PORTB
#define SPI_SS      (1<<PB4)
#define SPI_SCK     (1<<PB7)
#define SPI_MOSI    (1<<PB5)
#define SPI_MISO    (1<<PB6)

// Typ prenosu dat na SPI
typedef enum
{
    SPI_DATA_TRANSPORT_DataTransfer,
    SPI_DATA_TRANSPORT_DataSend2,
    SPI_DATA_TRANSPORT_DataSendAndReceive

} TSpiDataTransportType;

// SPI data bufer
typedef struct SpiDataBuffer
{
    // bufer
    uint8_t* buffer;

    // velikost buferu
    uint16_t size;

    // pocitadlo odeslanych/prijatych bytu dat v buferu
    uint16_t counter;

} TSpiDataBuffer;

// Stav SPI
typedef struct SpiState
{
    // Typ prenosu dat na SPI
    TSpiDataTransportType transportType;

    // bufer dat 1
    TSpiDataBuffer dataBuffer1;

    // bufer dat 2
    TSpiDataBuffer dataBuffer2;

    // Slave select delay [us]
    uint16_t slaveSelectDelay;

    // Dummy byte
    uint8_t dummyByte;

} TSpiState;

static TSpiState spiState;

//------------------------------------------------------------------------------
// ovladani SS pinu
// parametry:
//  activate
//      TRUE    ... aktivace slave
//      FALSE   ... deaktivace slave
//  delay
//      TRUE    ... prodleva pouzita
//      FALSE   ... bez prodlevy
//------------------------------------------------------------------------------
static void SlaveSelect(BOOL activate, BOOL delay)
{
    if (activate)
    {
        SPI_PORT &= ~SPI_SS;

        if (delay)
            _delay_us(spiState.slaveSelectDelay);
    }
    else
    {
        if (delay)
            _delay_us(spiState.slaveSelectDelay);

        SPI_PORT |= SPI_SS;
    }
}

//------------------------------------------------------------------------------
// obsluha preruseni SPI
// SPI Serial Transfer Complete
//------------------------------------------------------------------------------
ISR(SPI_STC_vect)
{
    volatile uint8_t dataReg = SPDR;
    BOOL sent;

    if (spiState.transportType == SPI_DATA_TRANSPORT_DataTransfer)
    {
        if (spiState.dataBuffer1.counter < spiState.dataBuffer1.size)
        {
            spiState.dataBuffer1.buffer[spiState.dataBuffer1.counter] = dataReg;
            spiState.dataBuffer1.counter++;

            if (spiState.dataBuffer1.counter < spiState.dataBuffer1.size)
                SPDR = spiState.dataBuffer1.buffer[spiState.dataBuffer1.counter];
            else
                SlaveSelect(FALSE, TRUE);
        }
    }
    else if (spiState.transportType == SPI_DATA_TRANSPORT_DataSend2)
    {
        sent = FALSE;
        if (spiState.dataBuffer1.counter < spiState.dataBuffer1.size)
        {
            spiState.dataBuffer1.counter++;

            if (spiState.dataBuffer1.counter < spiState.dataBuffer1.size)
                SPDR = spiState.dataBuffer1.buffer[spiState.dataBuffer1.counter];
            else
                SPDR = spiState.dataBuffer2.buffer[spiState.dataBuffer2.counter];

            sent = TRUE;
        }

        if (!sent)
        {
            if (spiState.dataBuffer2.counter < spiState.dataBuffer2.size)
            {
                spiState.dataBuffer2.counter++;

                if (spiState.dataBuffer2.counter < spiState.dataBuffer2.size)
                    SPDR = spiState.dataBuffer2.buffer[spiState.dataBuffer2.counter];
                else
                    SlaveSelect(FALSE, TRUE);
            }
        }
    }
    else if (spiState.transportType == SPI_DATA_TRANSPORT_DataSendAndReceive)
    {
        sent = FALSE;
        if (spiState.dataBuffer1.counter < spiState.dataBuffer1.size)
        {
            spiState.dataBuffer1.counter++;

            if (spiState.dataBuffer1.counter < spiState.dataBuffer1.size)
                SPDR = spiState.dataBuffer1.buffer[spiState.dataBuffer1.counter];
            else
                SPDR = spiState.dummyByte;

            sent = TRUE;
        }

        if (!sent)
        {
            if (spiState.dataBuffer2.counter < spiState.dataBuffer2.size)
            {
                spiState.dataBuffer2.buffer[spiState.dataBuffer2.counter] = dataReg;
                spiState.dataBuffer2.counter++;

                if (spiState.dataBuffer2.counter < spiState.dataBuffer2.size)
                    SPDR = spiState.dummyByte;
                else
                    SlaveSelect(FALSE, TRUE);
            }
        }
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void Spi_Init(void)
{
    Spi_Close();
}

//------------------------------------------------------------------------------
//  Otevre SPI.
//------------------------------------------------------------------------------
void Spi_Open(TSpiParameters* params)
{
    Spi_Close();

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        SPCR = 0;
        SPSR = 0;

        // nastaveni pinu
        SPI_DDR |= SPI_SCK | SPI_MOSI | SPI_SS;
        SPI_PORT |= SPI_SCK | SPI_MOSI | SPI_SS;
        SPI_DDR &= ~SPI_MISO;
        SPI_PORT |= SPI_MISO; // the pull-up resistor is activated

        uint8_t spcrSetup = (1<<SPIE) | (1<<SPE) | (1<<MSTR);
        uint8_t spsrSetup = 0;

        switch (params->dataOrder)
        {
        //case SPI_DATA_ORDER_MSB:
        default:
            break;
        case SPI_DATA_ORDER_LSB:
            spcrSetup |= (1<<DORD);
            break;
        }

        switch (params->clockPolarity)
        {
        //case SPI_CLOCK_POL_RISE_FALL:
        default:
            break;
        case SPI_CLOCK_POL_FALL_RISE:
            spcrSetup |= (1<<CPOL);
            break;
        }

        switch (params->clockPhase)
        {
        //case SPI_CLOCK_PHASE_SAMPLE_SETUP:
        default:
            break;
        case SPI_CLOCK_PHASE_SETUP_SAMPLE:
            spcrSetup |= (1<<CPHA);
            break;
        }

        switch (params->clockRate)
        {
        //case SPI_CLOCK_RATE_FOSC_4:
        default:
            break;
        case SPI_CLOCK_RATE_FOSC_2:
            spsrSetup |= (1<<SPI2X);
            break;
        case SPI_CLOCK_RATE_FOSC_8:
            spcrSetup |= (1<<SPR0);
            spsrSetup |= (1<<SPI2X);
            break;
        case SPI_CLOCK_RATE_FOSC_16:
            spcrSetup |= (1<<SPR0);
            break;
        case SPI_CLOCK_RATE_FOSC_32:
            spcrSetup |= (1<<SPR1);
            spsrSetup |= (1<<SPI2X);
            break;
        case SPI_CLOCK_RATE_FOSC_64:
            spcrSetup |= (1<<SPR1);
            break;
        case SPI_CLOCK_RATE_FOSC_128:
            spcrSetup |= (1<<SPR1) | (1<<SPR0);
            break;
        }

        SPCR = spcrSetup;
        SPSR = spsrSetup;

        spiState.slaveSelectDelay = params->slaveSelectDelay;
        spiState.dummyByte = params->dummyByte;
    }
}

//------------------------------------------------------------------------------
//  Zavre SPI.
//------------------------------------------------------------------------------
void Spi_Close(void)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        SPCR = 0;
        SPSR = 0;

        spiState.dataBuffer1.counter = 0;
        spiState.dataBuffer1.size = 0;
        spiState.dataBuffer2.counter = 0;
        spiState.dataBuffer2.size = 0;
        spiState.slaveSelectDelay = 0;
        spiState.dummyByte = 0;
    }
}

//------------------------------------------------------------------------------
//  Prenos dat na SPI (zacne vymena dat mezi masterem a slave),
//  volajici zajisti dostupnost pameti po dobu prenosu.
//  Na zacatku transakce jsou v datovem buferu vysilana data.
//  Po konci transakce budou v datovem buferu prijata data.
//------------------------------------------------------------------------------
void Spi_DataTransfer(uint8_t* data, uint16_t dataCount)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        spiState.transportType = SPI_DATA_TRANSPORT_DataTransfer;

        spiState.dataBuffer1.buffer = data;
        spiState.dataBuffer1.size = dataCount;
        spiState.dataBuffer1.counter = 0;

        if (dataCount > 0)
        {
            SlaveSelect(TRUE, TRUE);
            SPDR = data[0];
        }
    }
}

//------------------------------------------------------------------------------
//  Odeslani dat ve 2 buferech na SPI (zacne posilani dat masterem do slave),
//  volajici zajisti dostupnost pameti po dobu prenosu.
//------------------------------------------------------------------------------
void Spi_DataSend2(const uint8_t* data1, uint16_t dataCount1, const uint8_t* data2, uint16_t dataCount2)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        spiState.transportType = SPI_DATA_TRANSPORT_DataSend2;

        spiState.dataBuffer1.buffer = (uint8_t*)data1;
        spiState.dataBuffer1.size = dataCount1;
        spiState.dataBuffer1.counter = 0;

        spiState.dataBuffer2.buffer = (uint8_t*)data2;
        spiState.dataBuffer2.size = dataCount2;
        spiState.dataBuffer2.counter = 0;

        if ((dataCount1 > 0) && (dataCount2 > 0))
        {
            SlaveSelect(TRUE, TRUE);
            SPDR = data1[0];
        }
    }
}

//------------------------------------------------------------------------------
//  Odeslani a nasledny prijem dat na/z SPI (zacne posilani dat masterem do slave),
//  volajici zajisti dostupnost pameti po dobu prenosu.
//------------------------------------------------------------------------------
void Spi_DataSendAndReceive(const uint8_t* sendData, uint16_t sendDataCount, uint8_t* receiveData, uint16_t receiveDataCount)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        spiState.transportType = SPI_DATA_TRANSPORT_DataSendAndReceive;

        spiState.dataBuffer1.buffer = (uint8_t*)sendData;
        spiState.dataBuffer1.size = sendDataCount;
        spiState.dataBuffer1.counter = 0;

        spiState.dataBuffer2.buffer = (uint8_t*)receiveData;
        spiState.dataBuffer2.size = receiveDataCount;
        spiState.dataBuffer2.counter = 0;

        if ((sendDataCount > 0) && (receiveDataCount > 0))
        {
            SlaveSelect(TRUE, TRUE);
            SPDR = sendData[0];
        }
    }
}

//------------------------------------------------------------------------------
//  Vraci stav trvani transakce (TRUE == probiha).
//------------------------------------------------------------------------------
BOOL Spi_Pending(void)
{
    BOOL pending = FALSE;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        if (spiState.transportType == SPI_DATA_TRANSPORT_DataTransfer)
            pending = (spiState.dataBuffer1.counter < spiState.dataBuffer1.size) ? TRUE : FALSE;
        else if ((spiState.transportType == SPI_DATA_TRANSPORT_DataSend2) || (spiState.transportType == SPI_DATA_TRANSPORT_DataSendAndReceive))
            pending = ((spiState.dataBuffer1.counter < spiState.dataBuffer1.size) || (spiState.dataBuffer2.counter < spiState.dataBuffer2.size)) ? TRUE : FALSE;
    }

    return pending;
}

#endif // PROFIBUS_HW_MODULE_TYPE
