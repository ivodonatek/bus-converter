//******************************************************************************
//  Vstup/vystup
//*****************************************************************************/

#include "io.h"
#include "machine/api.h"
#include "board/board_config.h"

// operace vstupu/vystupu
typedef enum
{
    IO_OPER_INIT,               // inicializace
    IO_OPER_SET_OUTPUT_LEVEL,   // nastaveni vystupni urovne

} TIOOperation;

// typ pro parametry IO operace
typedef uint8_t TIOParameters;

// typ pro vysledek IO operace
typedef uint8_t TIOResult;

// prototyp ovladace vstupu/vystupu
typedef TIOResult (*TIODriver)(TIOOperation oper, TIOParameters params);

// ovladace vstupu/vystupu
// ovladac IO_ID_VPC3_RESET
static TIOResult IODriver_VPC3_RESET(TIOOperation oper, TIOParameters params)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        if (oper == IO_OPER_INIT)
        {
            IO_SetOutputLevel(IO_ID_VPC3_RESET, IO_VPC3_RESET_ON_LEVEL);
            IO_VPC3_RESET_DDR_REG |= BIT(IO_VPC3_RESET_BIT);
        }
        else if (oper == IO_OPER_SET_OUTPUT_LEVEL)
        {
            if (params)
                IO_VPC3_RESET_PORT_REG |= BIT(IO_VPC3_RESET_BIT);
            else
                IO_VPC3_RESET_PORT_REG &= ~BIT(IO_VPC3_RESET_BIT);
        }
    }

    return 0;
}

// tabulka IO ovladacu
static const TIODriver IODriver_TABLE[] = {
    IODriver_VPC3_RESET
};

// vrati ovladac IO nebo NULL
static TIODriver GetIODriver(TIOId id)
{
    if (id > IO_ID_Max)
        return NULL;

    return IODriver_TABLE[id];
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void IO_Init(void)
{
    TIODriver ioDriver;

    for (TIOId id = IO_ID_Min; id <= IO_ID_Max; id++)
    {
        ioDriver = GetIODriver(id);
        if (ioDriver != NULL)
            ioDriver(IO_OPER_INIT, 0);
    }
}

//------------------------------------------------------------------------------
//  Nastavi uroven na vystupu.
//------------------------------------------------------------------------------
void IO_SetOutputLevel(TIOId id, TBit level)
{
    TIODriver ioDriver = GetIODriver(id);
    if (ioDriver != NULL)
        ioDriver(IO_OPER_SET_OUTPUT_LEVEL, level);
}
