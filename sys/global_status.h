//******************************************************************************
//  Globalni stav systemu
//*****************************************************************************/

#ifndef GLOBAL_STATUS_H_INCLUDED
#define GLOBAL_STATUS_H_INCLUDED

#include "sys/sys_config.h"
#include "sys/types.h"

// Kod stavu
typedef uint8_t TGStatusCode;

// Index stavu
typedef uint8_t TGStatusIndex;

// makra funkci
#ifdef GLOBAL_STATUS_ENABLED
#define GLOBAL_STATUS_INIT() GlobalStatus_Init()
#define GLOBAL_STATUS_SET(statusCode) GlobalStatus_Set(statusCode)
#define GLOBAL_STATUS_IDX_SET(idx, statusCode) GlobalStatus_IdxSet(idx, statusCode)
#define GLOBAL_STATUS_IDX_INC(idx) GlobalStatus_IdxInc(idx)
#define GLOBAL_STATUS_SET_GREATER(statusCode) GlobalStatus_SetGreater(statusCode)
#define GLOBAL_STATUS_GET() GlobalStatus_Get()
#define GLOBAL_STATUS_IDX_GET(idx) GlobalStatus_IdxGet(idx)
#else
#define GLOBAL_STATUS_INIT()
#define GLOBAL_STATUS_SET(statusCode)
#define GLOBAL_STATUS_IDX_SET(idx, statusCode)
#define GLOBAL_STATUS_IDX_INC(idx)
#define GLOBAL_STATUS_SET_GREATER(statusCode)
#define GLOBAL_STATUS_GET() (0)
#define GLOBAL_STATUS_IDX_GET(idx) (0)
#endif // GLOBAL_STATUS_ENABLED

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani nize
//------------------------------------------------------------------------------
void GlobalStatus_Init(void);

//------------------------------------------------------------------------------
//  Nastavi stav.
//------------------------------------------------------------------------------
void GlobalStatus_Set(TGStatusCode statusCode);

//------------------------------------------------------------------------------
//  Nastavi indexovany stav.
//------------------------------------------------------------------------------
void GlobalStatus_IdxSet(TGStatusIndex idx, TGStatusCode statusCode);

//------------------------------------------------------------------------------
//  Inkrementace indexovaneho stavu.
//------------------------------------------------------------------------------
void GlobalStatus_IdxInc(TGStatusIndex idx);

//------------------------------------------------------------------------------
//  Nastavi stav, pokud je hodnota statusCode vetsi nez aktualni hodnota stavu.
//------------------------------------------------------------------------------
void GlobalStatus_SetGreater(TGStatusCode statusCode);

//------------------------------------------------------------------------------
//  Vrati stav.
//------------------------------------------------------------------------------
TGStatusCode GlobalStatus_Get(void);

//------------------------------------------------------------------------------
//  Vrati indexovany stav.
//------------------------------------------------------------------------------
TGStatusCode GlobalStatus_IdxGet(TGStatusIndex idx);

#endif // GLOBAL_STATUS_H_INCLUDED
