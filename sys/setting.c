//******************************************************************************
//  Nastaveni
//*****************************************************************************/

#include "sys_config.h"
#include "setting.h"
#include "device/vpc3.h"

// ID (Identification Number)
#define SETTING_IDENTIFICATION_NUMBER   0

// Modbus slave address
#define SETTING_MODBUS_SLAVE_ADDRESS    1

// Modbus UART baud rate
#define SETTING_MODBUS_UART_BAUD_RATE   19200

// Modbus UART data bits
#define SETTING_MODBUS_UART_DATA_BITS   UART_FRAME_DATA_BITS_8

// Modbus UART stop bits
#define SETTING_MODBUS_UART_STOP_BITS   UART_FRAME_STOP_BITS_ONE

// Modbus UART parity
#define SETTING_MODBUS_UART_PARITY      UART_FRAME_PAR_NONE

#ifdef MBUS_HW_MODULE_TYPE

// Mbus slave address
#define SETTING_MBUS_SLAVE_ADDRESS      1

// Mbus UART baud rate
#define SETTING_MBUS_UART_BAUD_RATE     2400

// Mbus UART data bits
#define SETTING_MBUS_UART_DATA_BITS     UART_FRAME_DATA_BITS_8

// Mbus UART stop bits
#define SETTING_MBUS_UART_STOP_BITS     UART_FRAME_STOP_BITS_ONE

// Mbus UART parity
#define SETTING_MBUS_UART_PARITY        UART_FRAME_PAR_EVEN

#endif // MBUS_HW_MODULE_TYPE

#ifdef PROFIBUS_HW_MODULE_TYPE

// PROFIBUS slave address
#define SETTING_PROFIBUS_SLAVE_ADDRESS  DP_ADDR

#endif // PROFIBUS_HW_MODULE_TYPE

//------------------------------------------------------------------------------
//  Vraci ID (Identification Number)
//------------------------------------------------------------------------------
unsigned long Setting_GetIdentificationNumber(void)
{
    return SETTING_IDENTIFICATION_NUMBER;
}

//------------------------------------------------------------------------------
//  Modbus slave address
//------------------------------------------------------------------------------
TModbusAddress Setting_GetModbusSlaveAddress(void)
{
    return SETTING_MODBUS_SLAVE_ADDRESS;
}

//------------------------------------------------------------------------------
//  Modbus UART baud rate
//------------------------------------------------------------------------------
TUartBaudRate Setting_GetModbusUartBaudRate(void)
{
    return SETTING_MODBUS_UART_BAUD_RATE;
}

//------------------------------------------------------------------------------
//  Modbus UART data bits
//------------------------------------------------------------------------------
TUartFrameDataBits Setting_GetModbusUartDataBits(void)
{
    return SETTING_MODBUS_UART_DATA_BITS;
}

//------------------------------------------------------------------------------
//  Modbus UART stop bits
//------------------------------------------------------------------------------
TUartFrameStopBits Setting_GetModbusUartStopBits(void)
{
    return SETTING_MODBUS_UART_STOP_BITS;
}

//------------------------------------------------------------------------------
//  Modbus UART parity
//------------------------------------------------------------------------------
TUartFrameParity Setting_GetModbusUartParity(void)
{
    return SETTING_MODBUS_UART_PARITY;
}

#ifdef MBUS_HW_MODULE_TYPE

//------------------------------------------------------------------------------
//  Mbus slave address
//------------------------------------------------------------------------------
TMbusAddress Setting_GetMbusSlaveAddress(void)
{
    return SETTING_MBUS_SLAVE_ADDRESS;
}

//------------------------------------------------------------------------------
//  Mbus UART baud rate
//------------------------------------------------------------------------------
TUartBaudRate Setting_GetMbusUartBaudRate(void)
{
    return SETTING_MBUS_UART_BAUD_RATE;
}

//------------------------------------------------------------------------------
//  Mbus UART data bits
//------------------------------------------------------------------------------
TUartFrameDataBits Setting_GetMbusUartDataBits(void)
{
    return SETTING_MBUS_UART_DATA_BITS;
}

//------------------------------------------------------------------------------
//  Mbus UART stop bits
//------------------------------------------------------------------------------
TUartFrameStopBits Setting_GetMbusUartStopBits(void)
{
    return SETTING_MBUS_UART_STOP_BITS;
}

//------------------------------------------------------------------------------
//  Mbus UART parity
//------------------------------------------------------------------------------
TUartFrameParity Setting_GetMbusUartParity(void)
{
    return SETTING_MBUS_UART_PARITY;
}

#endif // MBUS_HW_MODULE_TYPE

#ifdef PROFIBUS_HW_MODULE_TYPE

//------------------------------------------------------------------------------
//  PROFIBUS slave address
//------------------------------------------------------------------------------
uint8_t Setting_GetProfibusSlaveAddress(void)
{
    return SETTING_PROFIBUS_SLAVE_ADDRESS;
}

#endif // PROFIBUS_HW_MODULE_TYPE
