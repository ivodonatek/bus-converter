#ifndef SYSTICK_H_INCLUDED
#define SYSTICK_H_INCLUDED

#include "sys/types.h"

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void SysTick_Init(void);

//------------------------------------------------------------------------------
//  Vrati systemovy cas [ms]
//------------------------------------------------------------------------------
uint32_t SysTick_GetSysMilliSeconds(void);

//------------------------------------------------------------------------------
//  Vraci TRUE pokud uplynula prodleva (delayMilliSeconds) od zadaneho casu
//  (userSysMilliSeconds)
//------------------------------------------------------------------------------
BOOL SysTick_IsSysMilliSecondTimeout(uint32_t userSysMilliSeconds, uint32_t delayMilliSeconds);

#endif // SYSTICK_H_INCLUDED
