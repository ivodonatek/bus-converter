//******************************************************************************
//  UART API
//*****************************************************************************/

#ifndef UART_H_INCLUDED
#define UART_H_INCLUDED

#include "sys/types.h"

// UART identifikator
typedef enum
{
    UART_ID_0 = 0,
    UART_ID_1,

    UART_ID_Min = UART_ID_0,
    UART_ID_Max = UART_ID_1,
    UART_ID_Count = (UART_ID_Max + 1)

} TUartId;

// UART frame parity
typedef enum
{
    UART_FRAME_PAR_NONE = 0,
    UART_FRAME_PAR_EVEN,
    UART_FRAME_PAR_ODD,

    UART_FRAME_PAR_Min = UART_FRAME_PAR_NONE,
    UART_FRAME_PAR_Max = UART_FRAME_PAR_ODD,
    UART_FRAME_PAR_Count = (UART_FRAME_PAR_Max + 1)

} TUartFrameParity;

// UART frame stop bits
typedef enum
{
    UART_FRAME_STOP_BITS_ONE = 0,
    UART_FRAME_STOP_BITS_TWO,

    UART_FRAME_STOP_BITS_Min = UART_FRAME_STOP_BITS_ONE,
    UART_FRAME_STOP_BITS_Max = UART_FRAME_STOP_BITS_TWO,
    UART_FRAME_STOP_BITS_Count = (UART_FRAME_STOP_BITS_Max + 1)

} TUartFrameStopBits;

// UART frame data bits
typedef enum
{
    UART_FRAME_DATA_BITS_5 = 0,
    UART_FRAME_DATA_BITS_6,
    UART_FRAME_DATA_BITS_7,
    UART_FRAME_DATA_BITS_8,
    UART_FRAME_DATA_BITS_9,

    UART_FRAME_DATA_BITS_Min = UART_FRAME_DATA_BITS_5,
    UART_FRAME_DATA_BITS_Max = UART_FRAME_DATA_BITS_9,
    UART_FRAME_DATA_BITS_Count = (UART_FRAME_DATA_BITS_Max + 1)

} TUartFrameDataBits;

// UART baud rate
typedef uint32_t TUartBaudRate;

// Parametry UARTu
typedef struct UartParameters
{
    // baud rate
    TUartBaudRate baudRate;

    // data bits
    TUartFrameDataBits dataBits;

    // stop bits
    TUartFrameStopBits stopBits;

    // parity
    TUartFrameParity parity;

} TUartParameters;

// prototyp UART callbacku
typedef void (*TUartCallback)(TUartId id);

//------------------------------------------------------------------------------
//  Vrati dobu trvani preneseni znaku [ms].
//  bits ... start,data,parity,stop
//------------------------------------------------------------------------------
float Uart_GetFrameTime(uint32_t baudRate, uint8_t bits);

//------------------------------------------------------------------------------
//  Vrati celkovy pocet bitu v UART znaku (start,data,parity,stop).
//------------------------------------------------------------------------------
uint8_t Uart_GetFrameBits(TUartFrameDataBits dataBits, TUartFrameParity parity,
    TUartFrameStopBits stopBits);

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani nize
//------------------------------------------------------------------------------
void Uart_Init(void);

//------------------------------------------------------------------------------
//  Otevre UART.
//------------------------------------------------------------------------------
void Uart_Open(TUartId id, TUartParameters* params);

//------------------------------------------------------------------------------
//  Zavre UART.
//------------------------------------------------------------------------------
void Uart_Close(TUartId id);

//------------------------------------------------------------------------------
//  Posle data na UART (zacne se vysilat),
//  volajici zajisti dostupnost dat po dobu vysilani.
//------------------------------------------------------------------------------
void Uart_Send(TUartId id, uint8_t* data, uint16_t dataCount);

//------------------------------------------------------------------------------
//  Posle data na UART (zacne se vysilat),
//  volajici zajisti dostupnost dat po dobu vysilani.
//  Po odvysilani se vola callback.
//------------------------------------------------------------------------------
void Uart_SendWithCallback(TUartId id, uint8_t* data, uint16_t dataCount, TUartCallback callback);

//------------------------------------------------------------------------------
//  Vraci stav vysilani na UARTu (TRUE == vysila).
//------------------------------------------------------------------------------
BOOL Uart_Sending(TUartId id);

//------------------------------------------------------------------------------
//  Prijme data z UARTu (zacne prijem),
//  volajici zajisti pamet pro data po dobu prijmu.
//------------------------------------------------------------------------------
void Uart_Receive(TUartId id, uint8_t* buffer, uint16_t bufferSize);

//------------------------------------------------------------------------------
//  Vraci pocet prijatych bytu v buferu z UARTu.
//------------------------------------------------------------------------------
uint16_t Uart_ReceivedBytes(TUartId id);

//------------------------------------------------------------------------------
//  Zastavi prijem dat z UARTu.
//------------------------------------------------------------------------------
void Uart_StopReceiving(TUartId id);

#endif // UART_H_INCLUDED
