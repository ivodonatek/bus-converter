//******************************************************************************
//  UART API
//*****************************************************************************/

#include "machine/api.h"
#include "uart.h"
#include "global_status.h"

#define UART_BAUD_CALC(UART_BAUD_RATE) \
    ( ( F_CPU ) / ( ( UART_BAUD_RATE ) * 16UL ) - 1 )

// Stav UARTu
typedef struct UartState
{
    // stav vysilani
    BOOL sending;

    // Tx bufer
    uint8_t* txBuffer;

    // pocet bytu dat v Tx buferu
    uint16_t txDataCount;

    // pocet odeslanych bytu dat z Tx buferu
    uint16_t txDataSent;

    // stav prijmu
    BOOL receiving;

    // Rx bufer
    uint8_t* rxBuffer;

    // velikost Rx buferu
    uint16_t rxBufferSize;

    // pocet prijatych bytu dat v Rx buferu
    uint16_t rxBytes;

    // callback
    TUartCallback callback;

} TUartState;

static TUartState uartStates[UART_ID_Count];

// vypocet baud rate
static uint32_t Uart_BaudCalc(uint32_t br)
{
    return UART_BAUD_CALC(br);
}

// vrati odkaz na stav UARTu
static TUartState* GetUartState(TUartId id)
{
    TUartState *pUartState = (id == UART_ID_0) ? &uartStates[UART_ID_0] : &uartStates[UART_ID_1];
    return pUartState;
}

// prida byte do rx buferu (pokud se prijima a je dostatek mista)
static void AddByteToRxBuffer(TUartState *pUartState, uint8_t rxData)
{
    if (pUartState->receiving)
    {
        if (pUartState->rxBytes < pUartState->rxBufferSize)
        {
            pUartState->rxBuffer[pUartState->rxBytes] = rxData;
            pUartState->rxBytes++;
        }
    }
}

// odesle dalsi byte z tx buferu (pokud se vysila a je neco k odeslani,
// po poslednim bytu se nastavi konec vysilani)
static void SendByteFromTxBuffer(TUartState *pUartState, TUartId id)
{
    if (pUartState->sending)
    {
        pUartState->txDataSent++;
        if (pUartState->txDataSent < pUartState->txDataCount)
        {
            uint8_t txData = pUartState->txBuffer[pUartState->txDataSent];

            if (id == UART_ID_0)
                UDR0 = txData;
            else
                UDR1 = txData;
        }
        else
        {
            pUartState->sending = FALSE;
            if (pUartState->callback != NULL)
                pUartState->callback(id);
        }
    }
}

// obsluhy preruseni USART
// USART0 Rx Complete
ISR(USART0_RX_vect)
{
    uint8_t rxData = UDR0;
    TUartState *pUartState = &uartStates[UART_ID_0];
    AddByteToRxBuffer(pUartState, rxData);
}

// USART1 Rx Complete
ISR(USART1_RX_vect)
{
    uint8_t rxData = UDR1;
    TUartState *pUartState = &uartStates[UART_ID_1];
    AddByteToRxBuffer(pUartState, rxData);
}

// USART0 Tx Complete
ISR(USART0_TX_vect)
{
    TUartState *pUartState = &uartStates[UART_ID_0];
    SendByteFromTxBuffer(pUartState, UART_ID_0);
}

// USART1 Tx Complete
ISR(USART1_TX_vect)
{
    TUartState *pUartState = &uartStates[UART_ID_1];
    SendByteFromTxBuffer(pUartState, UART_ID_1);
}

//------------------------------------------------------------------------------
//  Vrati dobu trvani preneseni znaku [ms].
//  bits ... start,data,parity,stop
//------------------------------------------------------------------------------
float Uart_GetFrameTime(uint32_t baudRate, uint8_t bits)
{
    if (baudRate > 0)
    {
        float delay = bits * 1000.0 / baudRate;
        return delay;
    }
    else
    {
        return 0;
    }
}

//------------------------------------------------------------------------------
//  Vrati celkovy pocet bitu v UART znaku (start,data,parity,stop).
//------------------------------------------------------------------------------
uint8_t Uart_GetFrameBits(TUartFrameDataBits dataBits, TUartFrameParity parity,
    TUartFrameStopBits stopBits)
{
    uint8_t bits = 1;

    bits += dataBits + 5;

    if (parity != UART_FRAME_PAR_NONE)
        bits += 1;

    if (stopBits == UART_FRAME_STOP_BITS_ONE)
        bits += 1;
    else if (stopBits == UART_FRAME_STOP_BITS_TWO)
        bits += 2;

    return bits;
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void Uart_Init(void)
{
    for (TUartId id = UART_ID_Min; id <= UART_ID_Max; id++)
    {
        Uart_Close(id);
    }
}

//------------------------------------------------------------------------------
//  Otevre UART.
//------------------------------------------------------------------------------
void Uart_Open(TUartId id, TUartParameters* params)
{
    Uart_Close(id);

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        uint32_t baudSetup = Uart_BaudCalc(params->baudRate);

        uint8_t ucsrbSetup = (1<<RXEN0) | (1<<TXEN0) | (1<<RXCIE0) | (1<<TXCIE0);

        if (params->dataBits == UART_FRAME_DATA_BITS_9)
            ucsrbSetup |= (1<<UCSZ02);

        uint8_t ucsrcSetup = 0;

        switch (params->dataBits)
        {
        //case UART_FRAME_DATA_BITS_5:
        default:
            break;
        case UART_FRAME_DATA_BITS_6:
            ucsrcSetup |= (1<<UCSZ00);
            break;
        case UART_FRAME_DATA_BITS_7:
            ucsrcSetup |= (1<<UCSZ01);
            break;
        case UART_FRAME_DATA_BITS_8:
        case UART_FRAME_DATA_BITS_9:
            ucsrcSetup |= (1<<UCSZ01) | (1<<UCSZ00);
            break;
        }

        if (params->stopBits == UART_FRAME_STOP_BITS_TWO)
            ucsrcSetup |= (1<<USBS0);

        switch (params->parity)
        {
        //case UART_FRAME_PAR_NONE:
        default:
            break;
        case UART_FRAME_PAR_EVEN:
            ucsrcSetup |= (1<<UPM01);
            break;
        case UART_FRAME_PAR_ODD:
            ucsrcSetup |= (1<<UPM01) | (1<<UPM00);
            break;
        }

        if (id == UART_ID_0)
        {
            UBRR0H = baudSetup >> 8;
            UBRR0L = baudSetup;
            UCSR0B = ucsrbSetup;
            UCSR0C = ucsrcSetup;
        }
        else
        {
            UBRR1H = baudSetup >> 8;
            UBRR1L = baudSetup;
            UCSR1B = ucsrbSetup;
            UCSR1C = ucsrcSetup;
        }
    }
}

//------------------------------------------------------------------------------
//  Zavre UART.
//------------------------------------------------------------------------------
void Uart_Close(TUartId id)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        TUartState *pUartState = GetUartState(id);
        pUartState->sending = pUartState->receiving = FALSE;
        pUartState->callback = NULL;

        if (id == UART_ID_0)
        {
            UCSR0B = 0;
        }
        else
        {
            UCSR1B = 0;
        }
    }
}

//------------------------------------------------------------------------------
//  Posle data na UART (zacne se vysilat),
//  volajici zajisti dostupnost dat po dobu vysilani.
//------------------------------------------------------------------------------
void Uart_Send(TUartId id, uint8_t* data, uint16_t dataCount)
{
    Uart_SendWithCallback(id, data, dataCount, NULL);
}

//------------------------------------------------------------------------------
//  Posle data na UART (zacne se vysilat),
//  volajici zajisti dostupnost dat po dobu vysilani.
//  Po odvysilani se vola callback.
//------------------------------------------------------------------------------
void Uart_SendWithCallback(TUartId id, uint8_t* data, uint16_t dataCount, TUartCallback callback)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        TUartState *pUartState = GetUartState(id);

        pUartState->sending = (dataCount != 0) ? TRUE : FALSE;
        pUartState->txBuffer = data;
        pUartState->txDataCount = dataCount;
        pUartState->txDataSent = 0;
        pUartState->callback = callback;

        if (pUartState->sending)
        {
            if (id == UART_ID_0)
                UDR0 = data[0];
            else
                UDR1 = data[0];
        }
    }
}

//------------------------------------------------------------------------------
//  Vraci stav vysilani na UARTu (TRUE == vysila).
//------------------------------------------------------------------------------
BOOL Uart_Sending(TUartId id)
{
    BOOL sending;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        TUartState *pUartState = GetUartState(id);
        sending = pUartState->sending;
    }

    return sending;
}

//------------------------------------------------------------------------------
//  Prijme data z UARTu (zacne prijem),
//  volajici zajisti pamet pro data po dobu prijmu.
//------------------------------------------------------------------------------
void Uart_Receive(TUartId id, uint8_t* buffer, uint16_t bufferSize)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        TUartState *pUartState = GetUartState(id);

        pUartState->receiving = TRUE;
        pUartState->rxBuffer = buffer;
        pUartState->rxBufferSize = bufferSize;
        pUartState->rxBytes = 0;
    }
}

//------------------------------------------------------------------------------
//  Vraci pocet prijatych bytu v buferu z UARTu.
//------------------------------------------------------------------------------
uint16_t Uart_ReceivedBytes(TUartId id)
{
    uint16_t receivedBytes;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        TUartState *pUartState = GetUartState(id);
        receivedBytes = pUartState->rxBytes;
    }

    return receivedBytes;
}

//------------------------------------------------------------------------------
//  Zastavi prijem dat z UARTu.
//------------------------------------------------------------------------------
void Uart_StopReceiving(TUartId id)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        TUartState *pUartState = GetUartState(id);
        pUartState->receiving = FALSE;
    }
}
