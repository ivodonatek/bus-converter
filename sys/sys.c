//******************************************************************************
//  Sprava systemu
//*****************************************************************************/

#include "sys_config.h"
#include "machine/api.h"
#include "app/app.h"
#include "sys.h"
#include "systick.h"
#include "io.h"
#include "uart.h"
#include "spi.h"
#include "log.h"
#include "global_status.h"

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//------------------------------------------------------------------------------
void Sys_Init(void)
{
    SysTick_Init();
    IO_Init();
    Uart_Init();
#ifdef PROFIBUS_HW_MODULE_TYPE
    Spi_Init();
#endif // PROFIBUS_HW_MODULE_TYPE
    LOG_INIT();
    GLOBAL_STATUS_INIT();
    sei();
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void Sys_Task(void)
{
    LOG_TASK();
}

//------------------------------------------------------------------------------
//  Uloha hlavni smycky.
//------------------------------------------------------------------------------
void Sys_MainTask(void)
{
    Sys_Task();
    App_Task();
}
