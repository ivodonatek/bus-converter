//******************************************************************************
//  Machine API
//*****************************************************************************/

#ifndef MACHINE_API_H_INCLUDED
#define MACHINE_API_H_INCLUDED

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <util/atomic.h>

#endif // MACHINE_API_H_INCLUDED
