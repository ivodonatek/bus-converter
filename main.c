//******************************************************************************
//  Hlavni fw modul
//*****************************************************************************/

#include <string.h>
#include <stdio.h>
#include "board/board.h"
#include "board/board_config.h"
#include "sys/sys.h"
#include "app/app.h"
#include "machine/api.h"
#include "sys/uart.h"
#include "sys/setting.h"
#include "sys/systick.h"
#include "sys/log.h"
#include "sys/global_status.h"
#include "app/half_duplex_line.h"

//------------------------------------------------------------------------------
//	Hlavni funkce
//------------------------------------------------------------------------------
int main(void)
{
    // Init desky
    Board_Init();

    // Init systemu
    Sys_Init();

    uint32_t startUpTimer = SysTick_GetSysMilliSeconds();
    while (!SysTick_IsSysMilliSecondTimeout(startUpTimer, 20000))
        ;

    LOG_WRITELN("Sys Init");

    // Init aplikaci
    App_Init();

    while (1)
    {
        Sys_MainTask();
    }
}
